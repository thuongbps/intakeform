﻿using BPS.Intake.Infrastructure;
using HvN.Lib.IoC.Autofac;
namespace BPS.Intake.Application
{
    public class Bootstrapper
    {
        private static IApplication _application;

        public static void Boot()
        {
            _application = new BpsIntakeFormApplication();
            _application.Start();
        }

        public IApplication Application { get { return _application; } }
    }
}
