﻿using HvN.Lib.IoC.Autofac;
using BPS.Intake.Infrastructure;

namespace BPS.Intake.Application
{
    public class BpsIntakeFormApplication : IApplication
    {
        public void Start()
        {
            AutofacInstaller.Install();
            var container = AutofacEngine.Instance.Container;
            new Framework.IocRegister().Register(container);
            new Resource.IocRegister().Register(container);
            ResourceProvider = AutofacEngine.Instance.Resolve<IResourceProvider>();
        }

        public IResourceProvider ResourceProvider { get; private set; }
    }
}
