﻿using System;
using HvN.Lib.Shared.Data;

namespace BPS.Intake.Domain.Dto
{   
    public abstract class BaseDto : BaseDto<int> 
    {
        public Guid? CreatedBy { get; set; }

        public Guid? UpdatedBy { get; set; }

        public Guid? DeletedBy { get; set; }

        public DateTimeOffset? CreatedDate { get; set; }

        public DateTimeOffset? UpdatedDate { get; set; }

        public DateTimeOffset? DeletedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
