﻿using System;

namespace BPS.Intake.Domain.Dto
{
    public class CommonSearchDto
    {
        public Guid? CategoryId { get; set; }

        public Guid? GroupId { get; set; }

        public string Name { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public int Type { get; set; }
    }
}
