﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Dto
{
    public class DropdownDataSourceDto : BaseDto
    {
        public int DropdownId { get; set; }
        public string OptionValue { get; set; }
    }
}
