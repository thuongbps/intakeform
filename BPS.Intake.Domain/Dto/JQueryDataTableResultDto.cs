﻿using System.Collections.Generic;

namespace BPS.Intake.Domain.Dto
{
    public class JQueryDataTableResultDto<T> where T : class
    {
        public string sEcho { get; set; }

        public int iTotalRecords { get; set; }

        public int iTotalDisplayRecords { get; set; }

        public IEnumerable<T> Result { get; set; }
    }
}
