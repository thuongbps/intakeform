﻿namespace BPS.Intake.Domain.Dto
{
    public class LoginDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; }
        public string LastName { get; set; }
        public string OnQId { get; set; }
        public string OnQUserHotels { get; set; }
        public string OnQUserType { get; set; }
    }
}
