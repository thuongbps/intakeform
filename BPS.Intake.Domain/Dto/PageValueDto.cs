﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Dto
{
   public class PageValueDto : BaseDto
    {
        public int SessionId { get; set; }
        public int ControlId { get; set; }
        public string Value { get; set; }
    }
}
