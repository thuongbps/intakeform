﻿using System.Collections.Generic;
using BPS.Intake.Infrastructure.Types;
using System.ComponentModel;

namespace BPS.Intake.Domain.Dto
{
    public class QuestionCategoryDto : BaseDto
    {
        [DisplayName("Category")]
        public string Name { get; set; }
        
    }
}
