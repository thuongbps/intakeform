﻿using System.Collections.Generic;
using BPS.Intake.Infrastructure.Types;
using System.ComponentModel.DataAnnotations;

namespace BPS.Intake.Domain.Dto
{
    public class QuestionGroupDto : BaseDto
    {
        [Required]
        public string Name { get; set; }
    }
}
