﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Dto
{
    public class RegionDto : BaseDto
    {
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public bool IsMultipleChoice { get; set; }
        public int? GroupID { get; set; }
        public int? DependentID { get; set; }
        public int? DisplayOrder { get; set; }
        public string TypeOfObject { get; set; }
        public bool IsChecked { get; set; }
        public string Value { get; set; }
        public int? DependencyLevel { get; set; }
        public string DependencyWhere { get; set; }
        public string Validation { get; set; }
        public bool IsRequire { get; set; }
        public int? MaxLength { get; set; }
        public string TextMode { get; set; }
        public List<DropdownDataSourceDto> DropdownDataSources { get; set; }
    }
}
