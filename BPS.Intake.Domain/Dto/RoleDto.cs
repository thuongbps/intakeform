﻿using System.Collections.Generic;
using BPS.Intake.Infrastructure.Types;

namespace BPS.Intake.Domain.Dto
{
    public class RoleDto : BaseUserDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public RoleType RoleType { get; set; }

        public virtual IList<UserDto> Users { get; set; }
    }
}
