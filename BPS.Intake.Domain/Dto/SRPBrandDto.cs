﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Dto
{
    public class SRPBrandDto : BaseDto
    {
        public virtual int BrandId { get; set; }
        public virtual int SRPId { get; set; }
        public virtual string Value { get; set; }
    }
}
