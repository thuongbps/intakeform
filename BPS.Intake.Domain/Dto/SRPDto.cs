﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Dto
{
    public class SRPDto : BaseDto
    {
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public int SRPStatusId { get; set; }
        public string UserName { get; set; }
        public List<SRPRegionDto> SRPRegions { get; set; }
        public List<SRPBrandDto> SRPBrands { get; set; }
    }
}
