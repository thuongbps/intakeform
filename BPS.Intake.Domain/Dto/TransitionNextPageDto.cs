﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Dto
{
    public class TransitionNextPageDto : BaseDto
    {
        public int CurrentPageId { get; set; }
        public int ConditionGroupId { get; set; }
        public int ControlId { get; set; }
        public string WithValue { get; set; }
        public int NextPageId { get; set; }
    }
}
