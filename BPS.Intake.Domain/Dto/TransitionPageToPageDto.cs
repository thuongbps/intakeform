﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Dto
{
    public class TransitionPageToPageDto : BaseDto
    {
        public int SessionId { get; set; }
        public int FromPageId { get; set; }
        public int ToPageId { get; set; }
    }
}
