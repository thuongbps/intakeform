﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace BPS.Intake.Domain.Dto
{
    public class UserDto : BaseUserDto, IUser<Guid>
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OnQUserType { get; set; }
        public string OnQUserHotels { get; set; }
        public IList<RoleDto> Roles { get; set; }
    }
}
