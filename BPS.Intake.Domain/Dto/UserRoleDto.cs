﻿using System;

namespace BPS.Intake.Domain.Dto
{
    public class UserRoleDto:BaseDto
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public UserDto User { get; set; }
        public RoleDto Role { get; set; }
    }
}
