﻿namespace BPS.Intake.Domain.Saving
{
    public class SaveResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }
    }
}
