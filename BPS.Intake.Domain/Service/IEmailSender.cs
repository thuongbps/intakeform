﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Service
{
    public interface IEmailSender
    {
        Task<bool> Send(List<string> emailto, string subject, string bodyContent);
    }
}
