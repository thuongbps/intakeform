﻿using BPS.Intake.Domain.Dto;
using HvN.Lib.CrossCutting.Seedwork.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Service
{
    public interface ISRPService : IApplicationServiceBase
    {
        Task<JQueryDataTableResultDto<SRPDto>> GetSRPs(JQueryDataTableParamDto searchParams);
        Task<SRPDto> GetSRP(int id);
        Task<List<RegionDto>> GetRegion(int? parentID);
        Task<List<RegionDto>> GetRegionByParentID(int sessionId,int parentID);
        Task<List<PageValueDto>> GetPageValue(int? sessionID,List<RegionDto> region);
        Task<List<TransitionNextPageDto>> GetCurrentPage(List<RegionDto> region);
        Task<List<PageValueDto>> SavePageValue(int? sessionID, int? controlId, string value);
        Task<List<TransitionPageToPageDto>> SaveTransitionPageToPage(int sessionID, int fromPageID, int toPageID);
        Task<bool> HaveNextPage(int id);
        Task<bool> HavePreviousPage(int sessionID, int pageID);
    }
}
