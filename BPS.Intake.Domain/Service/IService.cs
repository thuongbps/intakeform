﻿
namespace BPS.Intake.Domain.Service
{
    public interface IService
    {
        IQuestionCategoryService CategoryService { get; set; }
        IUserService UserService { get; set; }
    }
}
