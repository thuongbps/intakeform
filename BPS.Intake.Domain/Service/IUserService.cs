﻿using System;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Services;
using BPS.Intake.Domain.Dto;

namespace BPS.Intake.Domain.Service
{
    public interface IUserService : IApplicationServiceBase
    {
        Task<Guid> CreateAsync(UserDto user);
        Task<UserDto> FindByUserName(string userName);
        Task<UserDto> UpdateAsync(UserDto user);
        Task<UserDto> GetUserById(Guid userId);
    }
}
