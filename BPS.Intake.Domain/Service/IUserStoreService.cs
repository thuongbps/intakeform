﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BPS.Intake.Domain.Dto;

namespace BPS.Intake.Domain.Service
{
    public interface IUserStoreService
    {
        Task<IList<UserDto>> GetAllUserRolesAsync();
        Task<IList<RoleDto>> GetAllRoleAsync();
        Task<UserDto> GetUserRolesByNameAsync(string userName);
    }
}
