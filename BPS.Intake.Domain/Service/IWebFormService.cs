﻿using BPS.Intake.Domain.Dto;
using HvN.Lib.CrossCutting.Seedwork.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BPS.Intake.Domain.Service
{
    public interface IWebFormService : IApplicationServiceBase
    {
        Task<IEnumerable<string>> GetCtyhocn(string inncodes);
        void UpdateUserLoginAudit(LoginDto dto, string innCodes, string lobbyInnCodes);
    }
}
