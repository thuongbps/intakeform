﻿using System;
using System.Collections.Generic;
using System.Linq;
using HvN.OTT.Domain.Dto;
using HvN.OTT.Domain.Service;
using HvN.OTT.Framework.Repository;
using HvN.OTT.Framework.Service;
using HvN.OTT.Schema.Entity;
using Moq;
using NUnit.Framework;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using Entity = HvN.OTT.Schema.Entity;
using System.Linq.Expressions;
using AutoMapper;
using HvN.Lib.ModuleMapping.AutoMapper;

namespace HvN.OTT.Appication.Test.Question
{
    [TestFixture]
    public class QuestionResultServiceTest
    {
        private IMapper _mapper;
        private Mock<IUnitOfWork> _uow;
        private Mock<IRepository<QuestionResult>> _repositoryQuestionResult;
        private IQuestionResultService _questionResultService;
        private QuestionResultDto _questionResultTest;
        [SetUp]
        public void Setup()
        {
            Mapper.CreateMap<QuestionResult, QuestionResultDto>().ReverseMap();
            Mapper.CreateMap<Schema.Entity.UserExam, UserExamDto>().ReverseMap();
            Mapper.CreateMap<AnswerResult, AnswerResultDto>().ReverseMap();

            _mapper = new AutoMapperMapper();
            _uow = new Mock<IUnitOfWork>();
            _repositoryQuestionResult = new Mock<IRepository<QuestionResult>>();
            _questionResultService = new QuestionResultService(_mapper, _uow.Object, _repositoryQuestionResult.Object);

            _questionResultTest = new QuestionResultDto
            {
                Id = Guid.NewGuid(),
                Content = "Question Test",
                Answers = new List<AnswerResultDto>
                {
                    new AnswerResultDto
                    {
                        Id = Guid.NewGuid(),
                        Selected = true,
                        IsCorrect = true
                    },
                    new AnswerResultDto
                    {
                        Id = Guid.NewGuid(),
                        Selected = true,
                        IsCorrect = true
                    }
                },
                UserExam = new UserExamDto
                {
                    Id = Guid.NewGuid(),
                    PassRate = 20,
                    Duration = TimeSpan.FromMinutes(20),
                },
                CreatedDate = DateTimeOffset.Now,
                UpdatedDate = DateTimeOffset.Now.AddMinutes(10)
            };
        }

        [Test]
        public void GetQuestionByUserExam_Return_With_Exist_Data()
        {
            var question = _mapper.MapTo<QuestionResult>(_questionResultTest);
            var questionList = new List<QuestionResult> { question };
            _repositoryQuestionResult.Setup(c => c.Find(It.IsAny<Expression<Func<QuestionResult, bool>>>())).Returns(questionList.AsQueryable());

            var result = _questionResultService.GetQuestionByUserExam(Guid.NewGuid());

            Assert.IsNotNull(result.Result);
            _repositoryQuestionResult.Verify(c => c.Find(It.IsAny<Expression<Func<QuestionResult, bool>>>()), Times.Once);
        }


        [Test]
        public void GetQuestionByUserExam_Return_Without_Exist_Data()
        {
            var questionList = new List<QuestionResult>();
            _repositoryQuestionResult.Setup(c => c.Find(It.IsAny<Expression<Func<QuestionResult, bool>>>())).Returns(questionList.AsQueryable());

            var result = _questionResultService.GetQuestionByUserExam(Guid.NewGuid());

            Assert.AreEqual(result.Result.Count, 0);
            _repositoryQuestionResult.Verify(c => c.Find(It.IsAny<Expression<Func<QuestionResult, bool>>>()), Times.Once);
        }


        [Test]
        public void GetMarkQuestionResult_Return_Mark_Average_By_Question_Count()
        {
            var countQuestion = 2;
            var averageMark = 0.0;
            var questionResult = new List<QuestionResultDto> { _questionResultTest };
            
            var result = _questionResultService.GetMarkQuestionResult(questionResult, out averageMark, countQuestion);

            Assert.AreEqual(50, result);
        }

        [Test]
        public void GetMarkQuestionResult_Return_0_Without_Question_Result()
        {
            var countQuestion = 2;
            var averageMark = 0.0;
            var questionResult = new List<QuestionResultDto>();

            var result = _questionResultService.GetMarkQuestionResult(questionResult, out averageMark, countQuestion);

            Assert.AreEqual(0, result);
        }

        [Test]
        public void IsQuestionResultExam_Return_Pass()
        {
            var countQuestion = 4;
            var averageMark = 0.0;
            var questionResult = new List<QuestionResultDto> { _questionResultTest };

            var result = _questionResultService.IsQuestionResultExam(questionResult, out averageMark, countQuestion);

            Assert.IsTrue(result.Result);
        }

        [Test]
        public void IsQuestionResultExam_Return_Fail()
        {
            var countQuestion = 10;
            var averageMark = 0.0;
            var questionResult = new List<QuestionResultDto> { _questionResultTest };

            var result = _questionResultService.IsQuestionResultExam(questionResult, out averageMark, countQuestion);

            Assert.IsFalse(result.Result);
        }

        [Test]
        public void CountQuestionResult_Return_Question_Valid()
        {
            var question = new QuestionResultDto
            {
                CreatedDate = DateTimeOffset.Now,
                UpdatedDate = DateTimeOffset.Now.AddMinutes(30),
                UserExam = new UserExamDto
                {
                    Duration = TimeSpan.FromMinutes(20)
                }
            };
            var questionResult = new List<QuestionResultDto> {_questionResultTest, question};

            var result = _questionResultService.CountQuestionResult(questionResult);

            Assert.AreEqual(result.Count, 1);
        }
    }
}
