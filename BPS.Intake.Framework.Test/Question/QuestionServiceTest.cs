﻿using System;
using System.Collections.Generic;
using System.Linq;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Framework.Service;
using Moq;
using NUnit.Framework;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using Entity = BPS.Intake.Schema.Entity;
using System.Linq.Expressions;
using AutoMapper;
using HvN.Lib.ModuleMapping.AutoMapper;

namespace BPS.Intake.Appication.Test.Question
{
    [TestFixture]
    public class QuestionServiceTest
    {
        private IMapper _mapper;
        private Mock<IUnitOfWork> _uow;
        private Mock<IRepository<Schema.Entity.SRPQuestionGroup>> _repositoryGroup;
        private Mock<IRepository<Schema.Entity.SRPQuestion>> _repositoryQuestion;
        private IQuestionService _questionService;
        [SetUp]
        public void Setup()
        {
            Mapper.CreateMap<Schema.Entity.SRPQuestionGroup, QuestionGroupDto>().ReverseMap();
            _mapper = new AutoMapperMapper();
            _uow = new Mock<IUnitOfWork>();
            _repositoryQuestion = new Mock<IRepository<Schema.Entity.SRPQuestion>>();
            _repositoryGroup = new Mock<IRepository<Schema.Entity.SRPQuestionGroup>>();
            _questionService = new QuestionService(_repositoryGroup.Object , _repositoryQuestion.Object, _mapper, _uow.Object);
        }

        [Test]
        public void CreateQuestion_Throw_exception_When_Dto_Invalid()
        {
            //var expected = Assert.Throws<ArgumentNullException>(() =>  _questionService.CreateQuestion( null));
            //Assert.AreEqual(expected.ParamName, "manageQuestionDTO is Null");
        }

        [Test]
        public void CreateQuestion_Question_Created_When_Dto_Valid()
        {
            //var question = _mapper.MapTo<Schema.Entity.Question>(_questionTest);

            //var result = _questionService.CreateQuestion(_questionTest);

            //Assert.AreEqual(result.Result, _questionTest.Id);
            //_repositoryQuestion.Verify(c => c.Add(question), Times.Once);
            //_uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Once);
        }

        
    }
}
