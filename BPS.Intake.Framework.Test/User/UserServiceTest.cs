﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using HvN.Lib.ModuleMapping.AutoMapper;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Framework.Service;
using Entity = BPS.Intake.Schema.Entity;
using Moq;
using NUnit.Framework;

namespace BPS.Intake.Appication.Test.User
{
    [TestFixture]
    public class UserServiceTest
    {
        private IMapper _mapper;
        private Mock<IUnitOfWork> _uow;
        private Mock<IUserRepository<Schema.Entity.User>> _repositoryUser;
        private IUserService _userService;

        private UserDto _userTest;
        [SetUp]
        public void Setup()
        {
            AutoMapper.Mapper.CreateMap<Schema.Entity.User, UserDto>().ReverseMap();
            _mapper = new AutoMapperMapper();
            _uow = new Mock<IUnitOfWork>();
            _repositoryUser = new Mock<IUserRepository<Schema.Entity.User>>();
            _userService = new UserService(_repositoryUser.Object, _mapper, _uow.Object);

            _userTest = new UserDto
            {
                Id = Guid.NewGuid(),
                UserName = "User Test",
                CreatedDate = DateTimeOffset.UtcNow
            };
        }

        [Test]
        public void CreateAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(() => _userService.CreateAsync(null));
            Assert.AreEqual(excepted.ParamName, "Invalid argument named user");
        }

        [Test]
        public void CreateAsync_User_Created_When_Dto_Valid()
        {
            var user = _mapper.MapTo<Schema.Entity.User>(_userTest);

            var result = _userService.CreateAsync(_userTest);

            Assert.AreEqual(result.Result, _userTest.Id);
            _repositoryUser.Verify(c => c.Add(user), Times.Once);
            _uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void UpdateAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(() => _userService.UpdateAsync(null));
            Assert.AreEqual(excepted.ParamName, "Invalid argument named user");
        }

        [Test]
        public void UpdateAsync_User_Updated_When_Dto_Valid()
        {
            var user = _mapper.MapTo<Schema.Entity.User>(_userTest);

            var result = _userService.UpdateAsync(_userTest);

            Assert.AreEqual(result.Result.UserName, _userTest.UserName);
            _repositoryUser.Verify(c => c.Update(user, It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
            _uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void GetUserById_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(async () => await _userService.GetUserById(Guid.Empty));
            Assert.AreEqual(excepted.ParamName, "Invalid userId");
        }

        [Test]
        public void GetUserById_Return_Exist_Data()
        {
            var users = new List<Schema.Entity.User> {_mapper.MapTo<Schema.Entity.User>(_userTest)};
            _repositoryUser.Setup(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(users.AsQueryable());

            var result = _userService.GetUserById(_userTest.Id);

            Assert.IsNotNull(result.Result);
            _repositoryUser.Verify(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }

        [Test]
        public void GetUserById_Return_Without_Exist_Data()
        {
            var users = new List<Schema.Entity.User> ();
            _repositoryUser.Setup(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(users.AsQueryable());

            var result = _userService.GetUserById(_userTest.Id);

            Assert.IsNull(result.Result);
            _repositoryUser.Verify(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }

        [Test]
        public void FindByNameAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(async () => await _userService.FindByUserName(""));
            Assert.AreEqual(excepted.ParamName, "Invalid argument named userName");
        }

        [Test]
        public void FindByNameAsync_Return_Exist_Data()
        {
            var user = _mapper.MapTo<Schema.Entity.User>(_userTest);
            _repositoryUser.Setup(c => c.FindOneAsync(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(Task.FromResult(user));

            var result = _userService.FindByUserName(_userTest.UserName);

            Assert.IsNotNull(result.Result);
            _repositoryUser.Verify(c => c.FindOneAsync(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }

        [Test]
        public void FindByName_Async_Return_Without_Exist_Data()
        {
            _repositoryUser.Setup(c => c.FindOneAsync(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(Task.FromResult((Schema.Entity.User)null));

            var result = _userService.FindByUserName(_userTest.UserName);

            Assert.IsNull(result.Result);
            _repositoryUser.Verify(c => c.FindOneAsync(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }
    }

}
