﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using HvN.Lib.ModuleMapping.AutoMapper;
using BPS.Intake.Infrastructure.Types;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Framework.Service;
using BPS.Intake.Schema.Entity;
using Moq;
using NUnit.Framework;

namespace BPS.Intake.Appication.Test.User
{
    [TestFixture]
    public class UserStoreServiceTest
    {
        private IMapper _mapper;
        private Mock<IUnitOfWork> _uow;
        private Mock<IUserRepository<Schema.Entity.User>> _repositoryUser;
        private Mock<IUserRepository<Role>> _repositoryRole;
        private UserStoreService _userStoreService;
        private UserDto _userTest;

        [SetUp]
        public void Setup()
        {
            AutoMapper.Mapper.CreateMap<Schema.Entity.User, UserDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<Role, RoleDto>().ReverseMap();
            _mapper = new AutoMapperMapper();
            _uow = new Mock<IUnitOfWork>();
            _repositoryUser = new Mock<IUserRepository<Schema.Entity.User>>();
            _repositoryRole = new Mock<IUserRepository<Role>>();
            _userStoreService = new UserStoreService(_repositoryUser.Object, _mapper, _uow.Object, _repositoryRole.Object);

            _userTest = new UserDto
            {
                Id = Guid.NewGuid(),
                UserName = "User Test",
                CreatedDate = DateTimeOffset.UtcNow
            };
        }

        [Test]
        public void CreateAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(() => _userStoreService.CreateAsync(null));
            Assert.AreEqual(excepted.ParamName, "Invalid argument named userDto");
        }

        [Test]
        public void CreateAsync_User_Created_When_Dto_Valid()
        {
            var user = new Schema.Entity.User { Id = _userTest.Id };
            var role = new Role { Id = Guid.NewGuid(), RoleType = RoleType.User };
            _repositoryUser.Setup(v => v.GetByKey(_userTest.Id)).Returns(user);
            _repositoryRole.Setup(c => c.FindOne(It.IsAny<Expression<Func<Role, bool>>>())).Returns(role);

            _userStoreService.CreateAsync(_userTest);

            _repositoryUser.Verify(c => c.Add(user), Times.Once);
            _repositoryUser.Verify(v => v.GetByKey(_userTest.Id), Times.Once);
            _repositoryRole.Verify(c => c.FindOne(It.IsAny<Expression<Func<Role, bool>>>()), Times.Once);
            _repositoryUser.Verify(v => v.Update(user, It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
            _uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(() => _userStoreService.DeleteAsync(null));
            Assert.AreEqual(excepted.ParamName, "Invalid argument named userDto");
        }

        [Test]
        public void DeleteAsync_User_Deleted_When_Dto_Valid()
        {
            var user = _mapper.MapTo<Schema.Entity.User>(_userTest);

            _userStoreService.DeleteAsync(_userTest);

            _repositoryUser.Verify(c => c.Delete(user), Times.Once);
            _uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void UpdateAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(() => _userStoreService.UpdateAsync(null));
            Assert.AreEqual(excepted.ParamName, "Invalid argument named userDto");
        }

        [Test]
        public void UpdateAsync_User_Updated_When_Dto_Valid()
        {
            var user = _mapper.MapTo<Schema.Entity.User>(_userTest);

            _userStoreService.UpdateAsync(_userTest);

            _repositoryUser.Verify(c => c.Update(user, It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
            _uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void FindByIdAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(async () => await _userStoreService.FindByIdAsync(Guid.Empty));
            Assert.AreEqual(excepted.ParamName, "Invalid argument named userId");
        }

        [Test]
        public void FindByIdAsync_Return_Exist_Data()
        {
            var user = new Schema.Entity.User { Id = _userTest.Id };
            _repositoryUser.Setup(c => c.GetByKey(user.Id)).Returns(user);

            var result = _userStoreService.FindByIdAsync(user.Id);

            Assert.IsNotNull(result.Result);
            _repositoryUser.Verify(c => c.GetByKey(user.Id), Times.Once);
        }

        [Test]
        public void FindByIdAsync_Return_Without_Exist_Data()
        {
            _repositoryUser.Setup(c => c.GetByKey(It.IsAny<Guid>())).Returns((Schema.Entity.User)null);

            var result = _userStoreService.FindByIdAsync(Guid.NewGuid());

            Assert.IsNull(result.Result);
            _repositoryUser.Verify(c => c.GetByKey(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public void FindByNameAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(async () => await _userStoreService.FindByNameAsync(""));
            Assert.AreEqual(excepted.ParamName, "Null or empty argument: userName");
        }

        [Test]
        public void FindByNameAsync_Return_Exist_Data()
        {
            var user = new Schema.Entity.User { Id = _userTest.Id };
            _repositoryUser.Setup(c => c.FindOne(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(user);

            var result = _userStoreService.FindByNameAsync(_userTest.UserName);

            Assert.IsNotNull(result.Result);
            _repositoryUser.Verify(c => c.FindOne(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }

        [Test]
        public void FindByNameAsync_Return_Without_Exist_Data()
        {
            _repositoryUser.Setup(c => c.FindOne(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns((Schema.Entity.User)null);

            var result = _userStoreService.FindByNameAsync(_userTest.UserName);

            Assert.IsNull(result.Result);
            _repositoryUser.Verify(c => c.FindOne(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }



        [Test]
        public void GetUserRolesByNameAsync_Throw_Exception_When_Dto_Invalid()
        {
            var excepted = Assert.Throws<ArgumentNullException>(async () => await _userStoreService.GetUserRolesByNameAsync(""));
            Assert.AreEqual(excepted.ParamName, "Null or empty argument: userName");
        }

        [Test]
        public void GetUserRolesByNameAsync_Return_Exist_Data()
        {
            var users = new List<Schema.Entity.User> { new Schema.Entity.User { Id = _userTest.Id } };
            _repositoryUser.Setup(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(users.AsQueryable());

            var result = _userStoreService.GetUserRolesByNameAsync(_userTest.UserName);

            Assert.IsNotNull(result.Result);
            _repositoryUser.Verify(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }

        [Test]
        public void GetUserRolesByNameAsync_Return_Without_Exist_Data()
        {
            var users = new List<Schema.Entity.User>();
            _repositoryUser.Setup(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(users.AsQueryable());

            var result = _userStoreService.GetUserRolesByNameAsync(_userTest.UserName);

            Assert.IsNull(result.Result);
            _repositoryUser.Verify(c => c.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }


        [Test]
        public void AddToRoleAsync_User_Created_When_Dto_Valid()
        {
            var user = new Schema.Entity.User { Id = _userTest.Id };
            var role = new Role { Id = Guid.NewGuid(), RoleType = RoleType.User };
            _repositoryUser.Setup(v => v.GetByKey(_userTest.Id)).Returns(user);
            _repositoryRole.Setup(c => c.FindOne(It.IsAny<Expression<Func<Role, bool>>>())).Returns(role);

            _userStoreService.AddToRoleAsync(_userTest, ((int)role.RoleType).ToString());

            _repositoryUser.Verify(v => v.GetByKey(_userTest.Id), Times.Once);
            _repositoryRole.Verify(c => c.FindOne(It.IsAny<Expression<Func<Role, bool>>>()), Times.Once);
            _repositoryUser.Verify(v => v.Update(user, It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
            _uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void GetRolesAsync_Return_Exist_Data()
        {
            var user = _mapper.MapTo<Schema.Entity.User>(_userTest);
            user.Roles = new List<Role> { new Role { Id = Guid.NewGuid(), RoleType = RoleType.User }, new Role { Id = Guid.NewGuid(), RoleType = RoleType.Admin }, new Role { Id = Guid.NewGuid(), RoleType = RoleType.Trainning } };
            var users = new List<Schema.Entity.User> { user };
            _repositoryUser.Setup(v => v.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(users.AsQueryable());

            var result = _userStoreService.GetRolesAsync(_userTest);

            Assert.AreEqual(result.Result.Count, 3);
            _repositoryUser.Verify(v => v.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }


        [Test]
        public void GetAllUserRolesAsync_Return_Exist_Data()
        {
            var users = new List<Schema.Entity.User> { _mapper.MapTo<Schema.Entity.User>(_userTest) };
            _repositoryUser.Setup(v => v.Find(c => !c.IsDeleted)).Returns(users.AsQueryable());

            var result = _userStoreService.GetAllUserRolesAsync();

            Assert.IsNotNull(result);
            _repositoryUser.Verify(v => v.Find((c => !c.IsDeleted)), Times.Once);
        }

        [Test]
        public void GetAllRoleAsync_Return_Exist_Data()
        {
            var roles = new List<Role> { new Role() };
            _repositoryRole.Setup(v => v.Find(c => !c.IsDeleted)).Returns(roles.AsQueryable());

            var result = _userStoreService.GetAllRoleAsync();

            Assert.IsNotNull(result);
            _repositoryRole.Verify(v => v.Find(c => !c.IsDeleted), Times.Once);
        }


        [Test]
        public void IsInRoleAsync_Return_Exist_Data()
        {
            var role = new Role {Id = Guid.NewGuid(), RoleType = RoleType.User};
            var user = new Schema.Entity.User { Id = _userTest.Id, Roles = new List<Role>{role}};
            var users = new List<Schema.Entity.User> {user};
            _repositoryRole.Setup(c => c.FindOne(It.IsAny<Expression<Func<Role, bool>>>())).Returns(role);
            _repositoryUser.Setup(v => v.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>())).Returns(users.AsQueryable());

            var result = _userStoreService.IsInRoleAsync(_userTest, ((int)role.RoleType).ToString());

            Assert.IsTrue(result.Result);
            _repositoryRole.Verify(v => v.FindOne(It.IsAny<Expression<Func<Role, bool>>>()), Times.Once);
            _repositoryUser.Verify(v => v.Find(It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
        }

        [Test]
        public void RemoveFromRoleAsync_User_Created_When_Dto_Valid()
        {
            var user = new Schema.Entity.User { Id = _userTest.Id };
            var role = new Role { Id = Guid.NewGuid(), RoleType = RoleType.User };
            _repositoryUser.Setup(v => v.GetByKey(_userTest.Id)).Returns(user);
            _repositoryRole.Setup(c => c.FindOne(It.IsAny<Expression<Func<Role, bool>>>())).Returns(role);

            _userStoreService.RemoveFromRoleAsync(_userTest, ((int)role.RoleType).ToString());

            _repositoryUser.Verify(v => v.GetByKey(_userTest.Id), Times.Once);
            _repositoryRole.Verify(c => c.FindOne(It.IsAny<Expression<Func<Role, bool>>>()), Times.Once);
            _repositoryUser.Verify(v => v.Update(user, It.IsAny<Expression<Func<Schema.Entity.User, bool>>>()), Times.Once);
            _uow.Verify(u => u.CommitChanges(It.IsAny<string>()), Times.Once);
        }
    }
}
