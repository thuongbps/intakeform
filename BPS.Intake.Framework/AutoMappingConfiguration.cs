﻿using HvN.Lib.ModuleMapping.AutoMapper;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Schema.Entity;

namespace BPS.Intake.Framework
{
    public class AutoMappingConfiguration : AutoMapperModuleMappingBase
    {
        public override void Configure()
        {
            AutoMapper.Mapper.CreateMap<User, UserDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<Role, RoleDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRP, SRPDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRPRegion, SRPRegionDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRPBrand, SRPBrandDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<RegionDto, Region>().ReverseMap();
            AutoMapper.Mapper.CreateMap<PageValue, PageValueDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<TransitionNextPage, TransitionNextPageDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<TransitionPageToPage, TransitionPageToPageDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRPQuestionCategory, QuestionCategoryDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<DropdownDataSourceDto, DropdownDataSource>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRPQuestionGroup, QuestionGroupDto>().ReverseMap();
        }
    }
}
