﻿using HvN.Lib.Data.EF.Repository;
using System.Data.Entity;
using System.Reflection;
using System.Threading;
using BPS.Intake.Schema.Entity;

namespace BPS.Intake.Framework.Repository
{
    public class IntakeContext : GenericContext
    {
        public IntakeContext()
            : base("IntakeContext")
        {
            UserName = Thread.CurrentPrincipal.Identity.Name;
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual void SetOriginalVersionValues(object entity, byte[] version)
        {
            Entry(entity).OriginalValues["Version"] = version;
        }

        public virtual void UpdateEntity(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(User)));
            modelBuilder.Entity<SRPRegion>().HasKey(k => new { k.SRPId, k.RegionId });
            modelBuilder.Entity<SRPBrand>().HasKey(k => new { k.SRPId, k.BrandId });
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<SRP> SRPs { get; set; }
        public DbSet<SRPQuestion> Questions { get; set; }
        public DbSet<SRPQuestionGroup> QuestionGroups { get; set; }
        public DbSet<SRPQuestionCategory> QuestionCategories { get; set; }
        public DbSet<SRPAnswer> Answers { get; set; }
    }
}