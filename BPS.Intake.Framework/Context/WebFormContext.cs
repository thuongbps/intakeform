﻿using HvN.Lib.Data.EF.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Framework.Context
{
    public class WebFormContext : DbContext
    {
        public WebFormContext() : base("WebFormContext")
        {
        }
    }
}
