﻿using BPS.Intake.Domain.Dto;
using BPS.Intake.Infrastructure.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Framework.Extensions
{
    public static class OrderExtensions
    {
        private const string SortAsending = "asc";
        public static IEnumerable<T> Order<T>(this IEnumerable<T> entities, JQueryDataTableParamDto searchParams)
        {
            var propertyName = SortColumnName(searchParams);

            if (!entities.Any() || string.IsNullOrEmpty(propertyName))
                return entities;

            var propertyInfo = entities.First().GetType().GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            if (searchParams.sSortDir_0 == SortAsending)
            {
                return entities.OrderBy(e => propertyInfo.GetValue(e, null));
            }
            return entities.OrderByDescending(e => propertyInfo.GetValue(e, null));
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> entities, JQueryDataTableParamDto searchParams)
        {
            var sortField = searchParams.SortColumnName();

            if (!entities.Any() || string.IsNullOrEmpty(sortField))
            {
                return entities;
            }

            var param = Expression.Parameter(typeof(T), "p");
            var prop = Expression.Property(param, sortField);
            var exp = Expression.Lambda(prop, param);
            string method = searchParams.sSortDir_0 == SortAsending ? "OrderBy" : "OrderByDescending";

            Type[] types = new Type[] { entities.ElementType, exp.Body.Type };
            var mce = Expression.Call(typeof(Queryable), method, types, entities.Expression, exp);

            return entities.Provider.CreateQuery<T>(mce);
        }

        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> entities, string propertyName)
        {
            if (!entities.Any() || string.IsNullOrEmpty(propertyName))
                return entities;

            var propertyInfo = entities.First().GetType().GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            return entities.OrderBy(e => propertyInfo.GetValue(e, null));
        }

        public static IEnumerable<T> OrderByDescending<T>(this IEnumerable<T> entities, string propertyName)
        {
            if (!entities.Any() || string.IsNullOrEmpty(propertyName))
                return entities;

            var propertyInfo = entities.First().GetType().GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            return entities.OrderByDescending(e => propertyInfo.GetValue(e, null));
        }

        public static IOrderedEnumerable<T> OrderDropdown<T, TKey>(this IEnumerable<T> src, SortOrderType orderType, string propName)
        {
            try
            {

                var lableTarget = Expression.Label(typeof(int), "a");
                var type = typeof(T);
                var prop = type.GetProperty(propName);
                var param = Expression.Parameter(type, "a");

                var expToUpper = Expression.Call(Expression.PropertyOrField(param, prop.Name), "ToUpper", null);
                var startsWithMethod = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });
                var expStartsWith = Expression.Call(expToUpper, startsWithMethod,
                    Expression.Constant("Other"));

                var expResult1 = Expression.Return(Expression.Label(typeof(int)), Expression.Constant(1));
                var expResult2 = Expression.Return(Expression.Label(typeof(int)), Expression.Constant(2));
                var exp = Expression.Block(Expression.IfThenElse(expStartsWith, expResult2, expResult1), Expression.Label(lableTarget, Expression.Constant(1)));
                var lamda = Expression.Lambda<Func<T, int>>(exp, param);
                if (orderType == SortOrderType.ASC)
                {
                    return src.OrderBy(lamda.Compile());
                }
                else
                {
                    return src.OrderByDescending(lamda.Compile());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SortColumnName(this JQueryDataTableParamDto dataTableParamDto)
        {
            var columnNames = dataTableParamDto.sColumns.Split(',');
            return columnNames[dataTableParamDto.iSortCol_0];
        }
    }
}
