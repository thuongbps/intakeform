﻿using System;
using System.Linq;
using Castle.DynamicProxy;
using HvN.Lib.IoC.Seedwork.IoC;
using HvN.Lib.IoC.Seedwork.Interceptors;
using HvN.Lib.Data.Seedwork.Repository;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.ModuleMapping.AutoMapper;
using HvN.Lib.CrossCutting.Seedwork.Caching;
using HvN.Lib.Caching.MemoryCache;
using HvN.Lib.CrossCutting.Seedwork.Services;
using HvN.Lib.Shared.Utils;
using System.Data.Entity;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Framework.Service;
using Microsoft.AspNet.Identity;
using HvN.Lib.IoC.Autofac;

namespace BPS.Intake.Framework
{
    public class IocRegister : IContainerRegister
    {
        public void Register(IContainerManager container)
        {
            // For using GenericException, the ExceptionInterceptors should be considered 
            // whether it should be used or not in the application service layer.
            container.AddComponent(typeof(ExceptionInterceptors), typeof(ExceptionInterceptors), "Exception-Interceptor", IocComponentLifeStyle.Singleton);

            // Infrastructure components
            container.AddComponent(typeof(IMapper), typeof(AutoMapperMapper), "", IocComponentLifeStyle.Singleton);
            container.AddComponent(typeof(ICacheManager), typeof(MemoryCacheManager), "", IocComponentLifeStyle.Singleton);
            container.AddComponent(typeof(IUnitOfWork), typeof(UnitOfWork), "", IocComponentLifeStyle.PerLifeTimeScope);
            container.AddComponent(typeof(IRepository<>), typeof(Repository<>));
            container.AddComponent(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            container.AddComponent(typeof(IUserRepository<>), typeof(UserRepository<>));
            container.AddComponent(typeof(IWebFormRepository<,>), typeof(WebFormRepository<,>));

            // Application components
            container.AddComponent(typeof(BaseService), typeof(BaseService));
            container.AddComponent(typeof(IUserStore<UserDto, Guid>), typeof(UserStoreService));
            container.AddComponent(typeof(IUserLoginStore<UserDto, Guid>), typeof(UserStoreService));
            container.AddComponent(typeof(IUserRoleStore<UserDto, Guid>), typeof(UserStoreService));
            container.AddComponent(typeof(IUserStoreService), typeof(UserStoreService));
            container.AddComponent(typeof(IWebFormService), typeof(WebFormService));
            container.AddComponentInstance(typeof(IIocEngine), AutofacEngine.Instance, "");

            var services = new ApplicationTypeFinder().FindClassesOfType<IApplicationServiceBase>();
            foreach (var service in services)
            {
                var serviceInterface = service.GetInterfaces().First();
                container.AddComponent(serviceInterface, service, "", IocComponentLifeStyle.Transient,
                    new Tuple<Type, IProxyGenerationHook>(typeof(DebugLoggingInterceptors), null),
                    new Tuple<Type, IProxyGenerationHook>(typeof(CacheInterceptor), null),
                    new Tuple<Type, IProxyGenerationHook>(typeof(TransactionInterceptor), null),
                    new Tuple<Type, IProxyGenerationHook>(typeof(ExceptionInterceptors), null));
            }
        }
    }
}
