﻿using HvN.Lib.Data.EF.Repository;
using HvN.Lib.Data.Seedwork.Repository.SessionStorage;
using System;
using BPS.Intake.Schema.Entity;
using HvN.Lib.Data.Seedwork.Entity;

namespace BPS.Intake.Framework.Repository
{
    public class BaseRepository<TEntity> : GenericRepository<TEntity, int, IntakeContext>, IBaseRepository<TEntity>
        where TEntity : BaseEntity<int>
    {
        public BaseRepository(IDbContextStorage dbContextStorage)
            : base(dbContextStorage)
        {
        }

    }
}
