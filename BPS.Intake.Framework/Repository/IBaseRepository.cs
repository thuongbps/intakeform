﻿using HvN.Lib.Data.Seedwork.Entity;
using HvN.Lib.Data.Seedwork.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Framework.Repository
{
    public interface IBaseRepository<TEntity> : IGenericRepository<TEntity, int, IntakeContext>
        where TEntity : BaseEntity<int>
    { }
}
