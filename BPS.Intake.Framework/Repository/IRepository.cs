﻿using HvN.Lib.Data.Seedwork.Repository;
using System;
using BPS.Intake.Schema.Entity;

namespace BPS.Intake.Framework.Repository
{
    public interface IRepository<TEntity> : IGenericRepository<TEntity, int, IntakeContext>
        where TEntity : AbstractEntity
    { }
}
