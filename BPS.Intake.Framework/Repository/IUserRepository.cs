﻿using HvN.Lib.Data.Seedwork.Entity;
using HvN.Lib.Data.Seedwork.Repository;
using System;

namespace BPS.Intake.Framework.Repository
{
    public interface IUserRepository<TEntity> : IGenericRepository<TEntity, Guid, IntakeContext>
        where TEntity : BaseEntity<Guid>
    {
    }
}
