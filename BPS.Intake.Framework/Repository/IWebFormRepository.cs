﻿using BPS.Intake.Domain.Dto;
using BPS.Intake.Framework.Context;
using HvN.Lib.Data.Seedwork.Entity;
using HvN.Lib.Data.Seedwork.Repository;
using System.Collections.Generic;

namespace BPS.Intake.Framework.Repository
{
    public interface IWebFormRepository<TEntity, TId> : IGenericRepository<TEntity, TId, WebFormContext>
        where TEntity : BaseEntity<TId>
    {
        IEnumerable<string> GetCtyhocn(string inncodes);
        void UpdateUserLoginAudit(LoginDto dto, string innCodes, string lobbyInnCodes);
    }
}
