﻿using HvN.Lib.Data.EF.Repository;
using HvN.Lib.Data.Seedwork.Repository.SessionStorage;
using System;
using BPS.Intake.Schema.Entity;

namespace BPS.Intake.Framework.Repository
{
    public class Repository<TEntity> : GenericRepository<TEntity, int, IntakeContext>, IRepository<TEntity>
        where TEntity : AbstractEntity
    {
        public Repository(IDbContextStorage dbContextStorage)
            : base(dbContextStorage)
        {
        }

    }
}
