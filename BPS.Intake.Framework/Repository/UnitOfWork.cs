﻿using System;
using System.Data;
using HvN.Lib.Data.Seedwork.Repository.SessionStorage;

namespace BPS.Intake.Framework.Repository
{
    public class UnitOfWork : HvN.Lib.Data.EF.UnitOfWork
    {
        private readonly string _typeOfDbContext;
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public UnitOfWork(IDbContextStorage dbContextStorage)
            : base(dbContextStorage)
        {
            _typeOfDbContext = typeof(IntakeContext).FullName;
        }

        public override IDisposable BeginTransaction(IsolationLevel isolationLevel, string typeOfDbContext = "")
        {
            return base.BeginTransaction(isolationLevel, typeOfDbContext == "" ? _typeOfDbContext : typeOfDbContext);
        }

        public override void CommitChanges(string typeOfDbContext = "")
        {
            try
            {
                base.CommitChanges(typeOfDbContext == "" ? _typeOfDbContext : typeOfDbContext);
            }
            catch(Exception ex)
            {
                log.Info("CommitChanges: " + ex.Message);
            }
        }

        public override void CommitTransaction(string typeOfDbContext = "")
        {
            base.CommitTransaction(typeOfDbContext == "" ? _typeOfDbContext : typeOfDbContext);
        }

        public override void RollbackTransaction(string typeOfDbContext = "")
        {
            base.RollbackTransaction(typeOfDbContext == "" ? _typeOfDbContext : typeOfDbContext);
        }
    }
}
