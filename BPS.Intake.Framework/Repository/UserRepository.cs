﻿using HvN.Lib.Data.EF.Repository;
using HvN.Lib.Data.Seedwork.Entity;
using System;
using HvN.Lib.Data.Seedwork.Repository.SessionStorage;

namespace BPS.Intake.Framework.Repository
{
    public class UserRepository<TEntity> : GenericRepository<TEntity, Guid, IntakeContext>, IUserRepository<TEntity>
        where TEntity : BaseEntity<Guid>
    {
        public UserRepository(IDbContextStorage dbContextStorage) : base(dbContextStorage)
        {
        }
    }
}
