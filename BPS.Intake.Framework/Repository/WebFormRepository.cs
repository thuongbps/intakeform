﻿using HvN.Lib.Data.EF.Repository;
using HvN.Lib.Data.Seedwork.Entity;
using System.Collections.Generic;
using HvN.Lib.Data.Seedwork.Repository.SessionStorage;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Framework.Context;
using System.Data.SqlClient;
using System.Linq;

namespace BPS.Intake.Framework.Repository
{
    public class WebFormRepository<TEntity, TId> : GenericRepository<TEntity, TId, WebFormContext>, IWebFormRepository<TEntity, TId>
        where TEntity : BaseEntity<TId>
    {
        public WebFormRepository(IDbContextStorage dbContextStorage) : base(dbContextStorage)
        {
        }

        public IEnumerable<string> GetCtyhocn(string inncodes)
        {
            var sSql = string.Format("SELECT Ctyhocn FROM [PIM_Data]..[AllHotelProperty] WHERE Ctyhocn IN ({0}) AND Status <> 'T'", string.Join(",", inncodes.Split('|')));

            var result = this.DbContext.Database
                .SqlQuery<string>(sSql).AsEnumerable();

            return result;
        }

        public void UpdateUserLoginAudit(LoginDto dto, string innCodes, string lobbyInnCodes)
        {
            DbContext.Database.ExecuteSqlCommand("exec sp_UpdateUserLoginAudit @OnQId, @Email, @FirstName, @LastName, @LobbyUserType, @InnCodes, @LobbyInnCodes",
                new SqlParameter("@OnQId", dto.OnQId),
                new SqlParameter("@Email", dto.Email),
                new SqlParameter("@FirstName", dto.FirstName),
                new SqlParameter("@LastName", dto.LastName),
                new SqlParameter("@LobbyUserType", dto.OnQUserType),
                new SqlParameter("@InnCodes", innCodes),
                new SqlParameter("@LobbyInnCodes", lobbyInnCodes));
        }
    }
}
