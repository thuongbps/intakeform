USE [Intake]
GO
SET IDENTITY_INSERT [dbo].[Brands] ON 

INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (1, N'Luxury', NULL, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (2, N'Waldorf', 1, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (3, N'LXR', 1, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (5, N'Lifestyle', NULL, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (6, N'Canopy', 5, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (7, N'Motto', 5, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (9, N'Full Service', NULL, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (10, N'Hilton', 9, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (11, N'Grand Vacations', 9, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (13, N'Doubletree', 9, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (14, N'Curio', 9, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (15, N'Tapestry', 9, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (16, N'Signia', 9, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (17, N'All Suites', NULL, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (19, N'Embassy Suites', 17, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (20, N'Homewood', 17, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (21, N'Home 2', 17, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (22, N'Focus Service', NULL, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (23, N'Garden Inn', 22, 1)
INSERT [dbo].[Brands] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (24, N'Tru', 22, 1)
SET IDENTITY_INSERT [dbo].[Brands] OFF
SET IDENTITY_INSERT [dbo].[ExtraAnswerValueTypes] ON 

INSERT [dbo].[ExtraAnswerValueTypes] ([Id], [Name]) VALUES (1, N'strings')
INSERT [dbo].[ExtraAnswerValueTypes] ([Id], [Name]) VALUES (2, N'number')
INSERT [dbo].[ExtraAnswerValueTypes] ([Id], [Name]) VALUES (3, N'date')
SET IDENTITY_INSERT [dbo].[ExtraAnswerValueTypes] OFF
SET IDENTITY_INSERT [dbo].[Regions] ON 

INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (1, N'The rate is Global', NULL, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (2, N'The rate is for areas in the Americas', NULL, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (3, N'All Americas', 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (4, N'North America', 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (5, N'United States ', 4, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (6, N'Destination Markets', 5, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (7, N'Canada', 4, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (8, N'Mexico', 4, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (9, N'Central America', 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (10, N'All Central America ', 9, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (11, N'Destination Markets', 9, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (12, N'Caribbean', 11, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (13, N'South America', 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (14, N'Other/Specific Property List', 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (15, N'The rate is for areas Internationally', NULL, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (16, N'All International properties', 15, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (17, N'EMEA', 15, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (18, N'All EMEA', 17, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (19, N'Europe', 17, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (20, N'All Europe', 19, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (21, N'Central Europe', 19, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (22, N'HUKI (Hilton UK and Ireland)', 19, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (23, N'Middle East/Africa', 17, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (24, N'Turkey', 17, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (25, N'Other/Specific Property List', 17, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (26, N'APAC', 15, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (27, N'All APAC', 26, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (28, N'APAC Excluding Greater China', 26, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (29, N'Greater China (GCM)', 26, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (30, N'Japan/Korea (JKM)', 26, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (31, N'Australasia ', 26, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (32, N'All Australasia', 31, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (33, N'Australia/New Zealand (AUNZ)', 31, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (34, N'Australia', 31, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (35, N'New Zealand/Southeast Asia (SEA)', 31, 1)
INSERT [dbo].[Regions] ([Id], [Name], [ParentId], [IsMultipleChoice]) VALUES (36, N'Other/Specific Property List', 15, 1)
SET IDENTITY_INSERT [dbo].[Regions] OFF
SET IDENTITY_INSERT [dbo].[SRPQuestionCategories] ON 

INSERT [dbo].[SRPQuestionCategories] ([Id], [Name], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted]) VALUES (1, N'Choice', NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[SRPQuestionCategories] ([Id], [Name], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted]) VALUES (4, N'Input', NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[SRPQuestionCategories] ([Id], [Name], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted]) VALUES (6, N'Both', NULL, NULL, NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[SRPQuestionCategories] OFF
SET IDENTITY_INSERT [dbo].[SRPStatuses] ON 

INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (1, N'Started', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (2, N'GeneralInfo', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (3, N'BrandAndRegion', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (4, N'Description', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (5, N'Dates', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (6, N'Rates', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (7, N'Clients', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[SRPStatuses] ([Id], [Name], [Description], [Type], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedDate], [UpdatedDate], [DeletedDate], [IsDeleted], [NextStatusId], [PreviousStatusId]) VALUES (8, N'Review', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[SRPStatuses] OFF
