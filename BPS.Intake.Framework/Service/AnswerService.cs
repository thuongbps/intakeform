﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Schema.Entity;

namespace BPS.Intake.Framework.Service
{
    public class AnswerService : BaseService, IAnswerService
    {

        private IRepository<SRPAnswer> _answerOptionRepository;

        public AnswerService(IRepository<SRPAnswer> answerOptionRepository, IMapper mapper, IUnitOfWork uow)
            : base(mapper, uow)
        {
            _answerOptionRepository = answerOptionRepository;
        }

        
    }
}
