﻿using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;

namespace BPS.Intake.Framework.Service
{
    public class BaseService
    {
        protected readonly IMapper Mapper;
        protected readonly IUnitOfWork Uow;

        public BaseService(IMapper mapper, IUnitOfWork uow)
        {
            Mapper = mapper;
            Uow = uow;
        }
    }
}
