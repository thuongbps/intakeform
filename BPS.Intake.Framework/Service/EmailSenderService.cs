﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Configuration;
using System.Threading.Tasks;
using BPS.Intake.Domain.Service;

namespace BPS.Intake.Framework.Service
{
    public class EmailSenderService : IEmailSender
    {
        public async Task<bool> Send(List<string> emailto, string subject, string bodyContent)
        {
            try
            {
                var smtpHost = ConfigurationManager.AppSettings["SmtpHost"];
                var smtpPort = ConfigurationManager.AppSettings["SmtpPort"];
                using (var client = new SmtpClient(smtpHost, Convert.ToInt32(smtpPort)))
                {
                    var userName = ConfigurationManager.AppSettings["SmtpUserName"];
                    var password = ConfigurationManager.AppSettings["SmtpPassword"];

                    client.Credentials = new System.Net.NetworkCredential(userName, password);
                    client.EnableSsl = true;

                    var mail = new MailMessage
                    {
                        From = new MailAddress(ConfigurationManager.AppSettings["SmtpFromMailAddress"], ConfigurationManager.AppSettings["SmtpFromMailName"]),
                        Subject = subject,
                        Body = bodyContent
                    };
                    if (emailto.Any())
                    {
                        foreach (var recipient in emailto)
                        {
                            mail.To.Add(recipient);
                        }
                    }
                    await client.SendMailAsync(mail);
                }

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
