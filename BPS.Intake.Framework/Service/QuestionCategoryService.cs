﻿using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;

namespace BPS.Intake.Framework.Service
{
    public class QuestionCategoryService : BaseService, IQuestionCategoryService
    {
        private readonly IRepository<Schema.Entity.SRPQuestionCategory> _repository;

        public QuestionCategoryService(IRepository<Schema.Entity.SRPQuestionCategory> repository, IMapper mapper, IUnitOfWork uow)
            : base(mapper, uow)
        {
            _repository = repository;
        }

        
    }
}
