﻿using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Schema.Entity;

namespace BPS.Intake.Framework.Service
{
    public class QuestionGroupService : BaseService, IQuestionGroupService
    {
        private readonly IRepository<SRPQuestionGroup> _groupRepository;
        private readonly IRepository<SRPQuestion> _questionRepository;

        public QuestionGroupService(IMapper mapper, IUnitOfWork uow,
                IRepository<SRPQuestionGroup> groupRepository,
                IRepository<SRPQuestion> questionRepository)
            : base(mapper, uow)
        {
            _groupRepository = groupRepository;
            _questionRepository = questionRepository;
        }

        
    }
}
