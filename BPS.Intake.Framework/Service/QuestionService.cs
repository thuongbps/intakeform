﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using Castle.Core.Internal;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Schema.Entity;
using BPS.Intake.Infrastructure.Types;

namespace BPS.Intake.Framework.Service
{
    public class QuestionService : BaseService, IQuestionService
    {
        private readonly IRepository<SRPQuestion> _questionRepository;
        private readonly IRepository<SRPQuestionGroup> _groupRepository;

        public QuestionService(IRepository<SRPQuestionGroup> groupRepository , IRepository<SRPQuestion> questionRepository, IMapper mapper, IUnitOfWork uow)
            : base(mapper, uow)
        {
            _questionRepository = questionRepository;
            _groupRepository = groupRepository;
        }

    }
}
