﻿using BPS.Intake.Domain.Service;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Schema.Entity;
using BPS.Intake.Domain.Dto;
using System.Data.Entity;
using BPS.Intake.Framework.Extensions;
using System;

namespace BPS.Intake.Framework.Service
{
    public class SRPService : BaseService, ISRPService
    {
        private readonly IRepository<SRP> _SRPRepository;
        private readonly IBaseRepository<Region> _RegionRepository;
        private readonly IBaseRepository<PageValue> _PageValueRepository;
        private readonly IBaseRepository<SessionForm> _SessionFormRepository;
        private readonly IBaseRepository<TransitionNextPage> _TransitionNextPageRepository;
        private readonly IBaseRepository<TransitionPageToPage> _TransitionPageToPageRepository;
        private IMapper _mapper;
        public SRPService(IRepository<SRP> SRPRepository, IBaseRepository<Region> RegionRepository, IBaseRepository<SessionForm> SessionFormRepository, IBaseRepository<PageValue> PageValueRepository, IBaseRepository<TransitionPageToPage> TransitionPageToPageRepository, IBaseRepository<TransitionNextPage> TransitionNextPageRepository, IMapper mapper, IUnitOfWork uow) : base(mapper, uow)
        {
            _SRPRepository = SRPRepository;
            _RegionRepository = RegionRepository;
            _PageValueRepository = PageValueRepository;
            _SessionFormRepository = SessionFormRepository;
            _TransitionNextPageRepository = TransitionNextPageRepository;
            _TransitionPageToPageRepository = TransitionPageToPageRepository;
            _mapper = mapper;
        }

        public Task<SRPDto> GetSRP(int id)
        {
            var data = _SRPRepository.Find(f => f.Id == id).Include(i => i.SRPRegions).Include(i => i.SRPBrands);

            return Task.FromResult(_mapper.MapTo<SRPDto>(data));
        }

        public Task<JQueryDataTableResultDto<SRPDto>> GetSRPs(JQueryDataTableParamDto searchParams)
        {
            var result = new JQueryDataTableResultDto<SRPDto>();

            var lst = _SRPRepository.DbSet;

            result.iTotalRecords = lst.Count();
            result.iTotalDisplayRecords = result.iTotalRecords;
            //lst = lst.OrderBy(searchParams)
            //            .Skip(searchParams.iDisplayStart)
            //            .Take(searchParams.iDisplayLength);
            result.Result = _mapper.MapTo<List<SRPDto>>(lst);
            result.sEcho = searchParams.sEcho;

            return Task.FromResult(result);
        }
        public Task<List<RegionDto>> GetRegion(int? parentID)
        {
            var result = _RegionRepository.Find(f => f.ParentId == parentID).Include(i => i.DropdownDataSources).OrderBy(o => o.DisplayOrder).ToList();
            //_PageValueRepository.Add(new PageValue { ControlId='aaa',  });
            //Uow.CommitChanges();
                return Task.FromResult(_mapper.MapTo<List<RegionDto>>(result));
        }
        public Task<List<RegionDto>> GetRegionByParentID(int sessionId,int id)
        {
            var parent = _TransitionPageToPageRepository.FindOne(f => f.SessionId == sessionId && f.ToPageId == id); ;

            var result = _RegionRepository.Find(f => f.ParentId == parent.FromPageId).OrderBy("DisplayOrder");// GetRegion(parent.ParentId);
            return Task.FromResult(_mapper.MapTo<List<RegionDto>>(result));
        }
        public Task<List<PageValueDto>> GetPageValue(int? sessionID, List<RegionDto> region)
        {
            if (sessionID == null)
            {
                var sessionTemp = new SessionForm ();
                sessionTemp.FormName = "SRP";
                _SessionFormRepository.Add(sessionTemp);
                _SessionFormRepository.DbContext.SaveChanges();
              
                foreach (var item in region)
                {
                    _PageValueRepository.Add(new PageValue { ControlId = item.Id, SessionId= sessionTemp.Id, Value=null});
                    Uow.CommitChanges();
                }
                sessionID = sessionTemp.Id;
            }
            var result = _PageValueRepository.Find(f => f.SessionId == sessionID);
            return Task.FromResult(_mapper.MapTo<List<PageValueDto>>(result));
        }

        public Task<List<PageValueDto>> SavePageValue(int? sessionID, int? controlId,string value)
        {
            var entity=_PageValueRepository.FindOne(p => p.SessionId == sessionID && p.ControlId==controlId);
            if (entity != null)
            {
                entity.Value = value;
                _PageValueRepository.Update(entity, p => p.SessionId == sessionID && p.ControlId == controlId);
            }else
            {
                _PageValueRepository.Add(new PageValue { ControlId = controlId.Value, SessionId = sessionID.Value, Value = value });
          //      Uow.CommitChanges();
            }
            Uow.CommitChanges();
            var result = _PageValueRepository.Find(f => f.SessionId == sessionID);
            return Task.FromResult(_mapper.MapTo<List<PageValueDto>>(result));
        }

        public Task<List<TransitionPageToPageDto>> SaveTransitionPageToPage(int sessionID, int fromPageID, int toPageID)
        {
            var entity = _TransitionPageToPageRepository.FindOne(p => p.SessionId == sessionID && p.FromPageId == fromPageID && p.ToPageId == toPageID);
            if (entity == null)
            //{
            //    entity.Value = value;
            //    _PageValueRepository.Update(entity, p => p.SessionId == sessionID && p.ControlId == controlId);
            //}
            //else
            {
                entity= _TransitionPageToPageRepository.FindOne(p => p.SessionId == sessionID && p.FromPageId == fromPageID);
                if (entity != null)
                {
                    entity.ToPageId = toPageID;
                    _TransitionPageToPageRepository.Update(entity, p => p.SessionId == sessionID && p.FromPageId == fromPageID);
                }
                else
                {
                    _TransitionPageToPageRepository.Add(new TransitionPageToPage {SessionId=sessionID,FromPageId=fromPageID,ToPageId=toPageID });
                }
            }
            Uow.CommitChanges();
            var result = _TransitionPageToPageRepository.Find(f => f.SessionId == sessionID);
            return Task.FromResult(_mapper.MapTo<List<TransitionPageToPageDto>>(result));
        }
        public Task<bool> HaveNextPage(int id)
        {
            var entity = _RegionRepository.FindOne(f => f.ParentId == id);
            if (entity != null)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }

        public Task<bool> HavePreviousPage(int sessionID,int pageID)
        {
            var entity = _TransitionPageToPageRepository.FindOne(f => f.SessionId == sessionID && f.ToPageId == pageID);
            if (entity != null)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }

        public Task<List<TransitionNextPageDto>> GetCurrentPage(List<RegionDto> region)
        {
            int ? PageId = 0;
            foreach (var item in region)
            {
                PageId = item.ParentId;

            }
            var result = _TransitionNextPageRepository.Find(f => f.CurrentPageId == PageId);
            return Task.FromResult(_mapper.MapTo<List<TransitionNextPageDto>>(result));
        }
    }
}
