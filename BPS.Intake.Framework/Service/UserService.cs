﻿using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Schema.Entity;

namespace BPS.Intake.Framework.Service
{
    public class UserService : BaseService, IUserService
    {
        private readonly IUserRepository<User> _userRepository;

        public UserService(IUserRepository<User> userRepository, IMapper mapper, IUnitOfWork uow)
            : base(mapper, uow)
        {
            _userRepository = userRepository;
        }

        public Task<Guid> CreateAsync(UserDto user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("Invalid argument named user");
            }

            var userEntity = Mapper.MapTo<User>(user);
            _userRepository.Add(userEntity);
            Uow.CommitChanges();

            return Task.FromResult(userEntity.Id);
        }

        public Task<UserDto> FindByUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("Invalid argument named userName");
            }

            var user = _userRepository.Find(i => i.UserName.Equals(userName));
            user = user.Include(c => c.Roles);
            var result = user.FirstOrDefault();
            if (result == null) return Task.FromResult<UserDto>(null);

            return Task.FromResult(Mapper.MapTo<UserDto>(result));
        }

        public Task<UserDto> UpdateAsync(UserDto user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("Invalid argument named user");
            }

            var userEntity = Mapper.MapTo<User>(user);
            _userRepository.Update(userEntity, u => u.Id == user.Id);

            Uow.CommitChanges();

            return Task.FromResult(user);
        }

        public Task<UserDto> GetUserById(Guid userId)
        {
            if (userId == Guid.Empty)
            {
                throw new ArgumentNullException("Invalid userId");
            }
            var userInfo = _userRepository.Find(u => u.Id == userId).Include(c => c.Roles).FirstOrDefault();
            if (userInfo == null) return Task.FromResult<UserDto>(null);
            var userDto = Mapper.MapTo<UserDto>(userInfo);
            return Task.FromResult(userDto);

        }
    }
}
