﻿using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using BPS.Intake.Infrastructure.Types;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Schema.Entity;
using Microsoft.AspNet.Identity;
using System.Linq;

namespace BPS.Intake.Framework.Service
{
    public class UserStoreService : BaseService, IUserLoginStore<UserDto, Guid>, IUserRoleStore<UserDto, Guid>, IUserStoreService
    {
        private readonly IUserRepository<User> _userRepository;
        private readonly IUserRepository<Role> _roleRepository;

        public UserStoreService(IUserRepository<User> userRepository, IMapper mapper, IUnitOfWork uow, IUserRepository<Role> roleRepository)
            : base(mapper, uow)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        public Task CreateAsync(UserDto userDto)
        {
            if (userDto == null)
            {
                throw new ArgumentNullException("Invalid argument named userDto");
            }

            var user = Mapper.MapTo<User>(userDto);
            _userRepository.Add(user);
            Uow.CommitChanges();
            AddToRoleAsync(Mapper.MapTo<UserDto>(user), ((int)RoleType.User).ToString());
            return Task.FromResult<object>(null);
        }



        public Task DeleteAsync(UserDto userDto)
        {
            if (userDto == null)
                throw new ArgumentNullException("Invalid argument named userDto");
            var user = Mapper.MapTo<User>(userDto);
            _userRepository.Delete(user);

            Uow.CommitChanges();

            return Task.FromResult<object>(null);
        }

        public Task<UserDto> FindByIdAsync(Guid userId)
        {
            if (userId == default(Guid))
            {
                throw new ArgumentNullException("Invalid argument named userId");
            }

            UserDto result = null;
            var user = _userRepository.GetByKey(userId);

            if (user != null)
            {
                result = Mapper.MapTo<UserDto>(user);
            }

            return Task.FromResult(result);
        }

        public Task<UserDto> FindByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("Null or empty argument: userName");
            }

            UserDto result = null;
            var user = _userRepository.FindOne(i => i.UserName == userName);

            if (user != null)
            {
                result = Mapper.MapTo<UserDto>(user);
            }

            return Task.FromResult(result);
        }

        public Task<UserDto> GetUserRolesByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("Null or empty argument: userName");
            }

            var user = _userRepository.Find(i => i.UserName.Equals(userName));
            user = user.Include(c => c.Roles);
            var result = user.FirstOrDefault();
            if (result == null) return Task.FromResult<UserDto>(null);
            return Task.FromResult(Mapper.MapTo<UserDto>(result));
        }

        public Task UpdateAsync(UserDto userDto)
        {
            if (userDto == null)
            {
                throw new ArgumentNullException("Invalid argument named userDto");
            }

            var user = Mapper.MapTo<User>(userDto);
            _userRepository.Update(user, u => u.Id == user.Id);

            Uow.CommitChanges();

            return Task.FromResult<object>(null);
        }

        public void Dispose()
        {
        }

        public Task AddLoginAsync(UserDto user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<UserDto> FindAsync(UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(UserDto user)
        {
            throw new NotImplementedException();
        }

        public Task RemoveLoginAsync(UserDto user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }



        public Task AddToRoleAsync(UserDto user, string roleName)
        {
            int roleType;
            if (string.IsNullOrEmpty(roleName) || !Int32.TryParse(roleName, out roleType)) return Task.FromResult(0);
            var userEntity = _userRepository.GetByKey(user.Id);
            if (userEntity == null) return Task.FromResult(0);
            var role = _roleRepository.FindOne(r => (int)r.RoleType == roleType);
            if (role == null) return Task.FromResult(0);
            userEntity.UpdatedDate = DateTimeOffset.UtcNow;
            if (userEntity.Roles == null) userEntity.Roles = new List<Role>();
            userEntity.Roles.Add(role);
            _userRepository.Update(userEntity, c => c.Id == user.Id);
            Uow.CommitChanges();
            return Task.FromResult(0);
        }

        public Task<IList<string>> GetRolesAsync(UserDto user)
        {
            var users = _userRepository.Find(i => i.Id == user.Id).Include(c => c.Roles);
            var userEntity = users.FirstOrDefault();
            return Task.FromResult<IList<string>>(userEntity == null ? null : userEntity.Roles.Select(r => ((int)r.RoleType).ToString()).ToList());
        }


        public Task<IList<UserDto>> GetAllUserRolesAsync()
        {
            var userList = _userRepository.Find(c=>!c.IsDeleted);
            userList = userList.Include(c => c.Roles);
            return Task.FromResult(Mapper.MapTo<IList<UserDto>>(userList));
        }
        public Task<IList<RoleDto>> GetAllRoleAsync()
        {
            var roleList = _roleRepository.Find(c => !c.IsDeleted);
            return Task.FromResult(Mapper.MapTo<IList<RoleDto>>(roleList));
        }

        public Task<bool> IsInRoleAsync(UserDto user, string roleName)
        {
            int roleType;
            if (string.IsNullOrEmpty(roleName) || !int.TryParse(roleName, out roleType)) return Task.FromResult(false);
            var role = _roleRepository.FindOne(r => (int)r.RoleType == roleType);

            var userRole = _userRepository.Find(c => c.Id == user.Id && c.Roles.Select(s => s.Id).Contains(role.Id));
            return Task.FromResult(userRole.Any());
        }

        public Task RemoveFromRoleAsync(UserDto user, string roleName)
        {
            int roleType;
            if (string.IsNullOrEmpty(roleName) || !Int32.TryParse(roleName, out roleType)) return Task.FromResult(0);
            var userEntity = _userRepository.GetByKey(user.Id);
            if (userEntity == null) return Task.FromResult(0);
            var role = _roleRepository.FindOne(r => (int)r.RoleType == roleType);
            if (role == null) return Task.FromResult<object>(null);
            if (userEntity.Roles == null) userEntity.Roles = new List<Role>();
            userEntity.Roles.Remove(role);
            _userRepository.Update(userEntity, c => c.Id == user.Id);
            Uow.CommitChanges();
            return Task.FromResult(0);
        }


    }
}
