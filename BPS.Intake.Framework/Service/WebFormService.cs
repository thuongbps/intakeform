﻿using BPS.Intake.Domain.Service;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using HvN.Lib.Data.Seedwork.Repository;
using BPS.Intake.Framework.Repository;
using BPS.Intake.Schema.Entity;
using BPS.Intake.Domain.Dto;

namespace BPS.Intake.Framework.Service
{
    public class WebFormService : BaseService, IWebFormService
    {
        private readonly IWebFormRepository<ExternalEntity, int> _webFormRepository;
        public WebFormService(IMapper mapper, IUnitOfWork uow, IWebFormRepository<ExternalEntity, int> webFormRepository) : base(mapper, uow)
        {
            _webFormRepository = webFormRepository;
        }

        public Task<IEnumerable<string>> GetCtyhocn(string inncodes)
        {
            var response = _webFormRepository.GetCtyhocn(inncodes);

            return Task.FromResult(response);
        }

        public void UpdateUserLoginAudit(LoginDto dto, string innCodes, string lobbyInnCodes)
        {
            _webFormRepository.UpdateUserLoginAudit(dto, innCodes, lobbyInnCodes);
        }
    }
}
