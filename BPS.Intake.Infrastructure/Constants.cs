﻿using System.Configuration;

namespace BPS.Intake.Infrastructure
{
    public class IntakeConstants
    {
        #region paging modify

        public const int DefaultMaxDisplayPages = 6;

        public const string DefaultPageQueryParameter = "PageIndex";

        public const int DefaultPageCount = 5;

        public const int DefaultPageSize = 10;

        public const string DefaultPageQueryName = "page";

        public static string DomainName = ConfigurationSettings.AppSettings["DomainName"].ToString();
        #endregion

        public const int PAGESIZE = 10;
        public const int CURRENTPAGE = 0;
    }
}
