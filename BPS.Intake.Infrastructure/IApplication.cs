﻿namespace BPS.Intake.Infrastructure
{
    public interface IApplication
    {
        IResourceProvider ResourceProvider { get; }
        void Start();
    }
}
