﻿namespace BPS.Intake.Infrastructure
{
    public interface IResourceProvider
    {
        string GetMessage(string key);
        string GetMessageFormat(string key, params object[] args);
        string GetLabel(string key);
    }
}
