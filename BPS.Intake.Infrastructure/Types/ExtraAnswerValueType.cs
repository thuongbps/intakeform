﻿namespace BPS.Intake.Infrastructure.Types
{
    public enum ExtraAnswerValueType
    {
        strings = 1,
        number,
        date
    }
}
