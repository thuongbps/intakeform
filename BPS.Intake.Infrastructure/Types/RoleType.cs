﻿namespace BPS.Intake.Infrastructure.Types
{
    public enum RoleType
    {
        Admin = 0x0F00, // 3840
        Trainning = 0x00F0, // 240
        User = 0x000F, // 15
    }
}
