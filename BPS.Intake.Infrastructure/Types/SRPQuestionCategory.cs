﻿namespace BPS.Intake.Infrastructure.Types
{
    public enum SRPQuestionCategory
    {
        Choice = 1,
        Input,
        ChoiceAndInput
    }
}
