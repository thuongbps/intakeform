﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Infrastructure.Types
{
    public enum SortOrderType
    {
        ASC = 0,
        DESC = 1,
    }
}
