﻿using HvN.Lib.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BPS.Intake.Infrastructure.Utility
{
    public class SupportedOpenGenericApplicationTypeFinder : ApplicationTypeFinder
    {
        protected override bool DoesTypeImplementOpenGeneric(Type type, Type openGeneric)
        {
            if (type.IsGenericTypeDefinition) return false;
            var genericTypeDefinition = openGeneric.GetGenericTypeDefinition();

            var implementedTypes = openGeneric.IsInterface ? type.GetInterfaces() : GetBaseTypes(type);

            var isMatch = implementedTypes.Any(t => t.IsGenericType
                                                    && genericTypeDefinition.IsAssignableFrom(t.GetGenericTypeDefinition()));

            return isMatch;
        }

        private IEnumerable<Type> GetBaseTypes(Type concreteType)
        {
            var baseType = concreteType.BaseType;
            while (baseType != null)
            {
                yield return baseType;

                baseType = baseType.BaseType;
            }
        }
    }
}
