﻿using HvN.Lib.IoC.Seedwork.IoC;
using BPS.Intake.Infrastructure;

namespace BPS.Intake.Resource
{
    public class IocRegister : IContainerRegister
    {
        public void Register(IContainerManager container)
        {
            container.AddComponentInstance(typeof(IResourceProvider), new ResourceProvider());
        }
    }
}
