﻿using BPS.Intake.Infrastructure;

namespace BPS.Intake.Resource
{
    public class ResourceProvider : IResourceProvider
    {
        public string GetMessage(string key)
        {
            return Message.ResourceManager.GetString(key);
        }

        public string GetMessageFormat(string key, params object[] args)
        {
            var msg = Message.ResourceManager.GetString(key);

            return string.IsNullOrEmpty(msg) ? string.Empty : string.Format(msg, args);
        }

        public string GetLabel(string key)
        {
            return Label.ResourceManager.GetString(key);
        }
    }
}
