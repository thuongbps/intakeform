﻿using System;
using HvN.Lib.Data.Seedwork.Entity;

namespace BPS.Intake.Schema.Entity
{
    public abstract class AbstractEntity : BaseEntity<int>
    {
        public Guid? CreatedBy { get; set; }

        public Guid? UpdatedBy { get; set; }

        public Guid? DeletedBy { get; set; }

        public DateTimeOffset? CreatedDate { get; set; }

        public DateTimeOffset? UpdatedDate { get; set; }

        public DateTimeOffset? DeletedDate { get; set; }

        public bool IsDeleted { get; set; }
    }
}
