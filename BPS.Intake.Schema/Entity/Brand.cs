﻿using HvN.Lib.Data.Seedwork.Entity;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class Brand : BaseEntity<int>
    {
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public bool IsMultipleChoice { get; set; }
        public virtual IList<SRPBrand> SRPBrands { get; set; }
    }
}
