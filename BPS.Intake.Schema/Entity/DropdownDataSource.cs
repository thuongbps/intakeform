﻿using HvN.Lib.Data.Seedwork.Entity;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class DropdownDataSource : AbstractEntity
    {
        public int DropdownId { get; set; }
        public string OptionValue { get; set; }
        //srp
        public virtual Region Region { get; set; }
    }
}
