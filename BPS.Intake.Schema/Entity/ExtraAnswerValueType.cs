﻿using HvN.Lib.Data.Seedwork.Entity;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class ExtraAnswerValueType : BaseEntity<int>
    {
        public string Name { get; set; }
        public virtual IList<SRPExtraAnswer> SRPExtraAnswers { get; set; }
    }
}
