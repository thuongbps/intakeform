﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class BrandMapping : BaseEntityWithVersionMapping<Brand, int>
    {
        public BrandMapping()
        {
            ToTable("Brands").HasKey(k => k.Id);
            HasMany(m => m.SRPBrands).WithRequired().HasForeignKey(k => k.BrandId);
        }
    }
}
