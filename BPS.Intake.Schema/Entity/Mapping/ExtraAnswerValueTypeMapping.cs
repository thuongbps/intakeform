﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class ExtraAnswerValueTypeMapping : BaseEntityWithVersionMapping<ExtraAnswerValueType, int>
    {
        public ExtraAnswerValueTypeMapping()
        {
            ToTable("ExtraAnswerValueTypes").HasKey(k => k.Id);
        }
    }
}
