﻿using HvN.Lib.Data.EF.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class PageValueMapping : BaseEntityWithVersionMapping<PageValue, int>
    {
        public PageValueMapping()
        {
            ToTable("PageValues").HasKey(k => k.Id);
       }
    }
}
