﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class RegionMapping : BaseEntityWithVersionMapping<Region, int>
    {
        public RegionMapping()
        {
            ToTable("Regions").HasKey(k => k.Id);
            HasMany(m => m.SRPRegions).WithRequired().HasForeignKey(k => k.RegionId);
            HasMany(m => m.DropdownDataSources).WithRequired().HasForeignKey(k => k.DropdownId);
        }
    }
}
