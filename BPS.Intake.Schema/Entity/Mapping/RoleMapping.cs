﻿using HvN.Lib.Data.EF.Mapping;
using System;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class RoleMapping : BaseEntityWithVersionMapping<Role, Guid>
    {
        public RoleMapping()
        {
        }
    }
}
