﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class SRPAnswerMapping : BaseEntityWithVersionMapping<SRPAnswer, int>
    {
        public SRPAnswerMapping()
        {
            ToTable("SRPAnswers").HasKey(k => k.Id);
            HasRequired(r => r.Question)
                .WithMany(r => r.Answers)
                .HasForeignKey(k => k.QuestionId)
                .WillCascadeOnDelete(false);
            HasRequired(r => r.SRP)
                .WithMany(r => r.SRPAnswers)
                .HasForeignKey(k => k.SRPId)
                .WillCascadeOnDelete(false);
        }
    }
}
