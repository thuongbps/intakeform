﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class SRPExtraAnswerMapping : BaseEntityWithVersionMapping<SRPExtraAnswer, int>
    {
        public SRPExtraAnswerMapping()
        {
            ToTable("SRPExtraAnswers").HasKey(k => k.Id);
            HasRequired(r => r.SRPAnswer)
                .WithMany(m => m.SRPExtraAnswers)
                .HasForeignKey(k => k.SRPAnswerId)
                .WillCascadeOnDelete(false);
            HasRequired(r => r.ExtraAnswerValueType)
                .WithMany(m => m.SRPExtraAnswers)
                .HasForeignKey(k => k.ExtraAnswerValueTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
