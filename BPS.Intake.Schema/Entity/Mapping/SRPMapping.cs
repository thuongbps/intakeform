﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class SRPMapping : BaseEntityWithVersionMapping<SRP, int>
    {
        public SRPMapping()
        {
            ToTable("SRPs").HasKey(k => k.Id);
            HasRequired(r => r.SRPStatus)
                .WithMany(m => m.SRPs)
                .HasForeignKey(k => k.SRPStatusId)
                .WillCascadeOnDelete(false);
            HasMany(m => m.SRPRegions)
                .WithRequired()
                .HasForeignKey(k => k.SRPId);
            HasMany(m => m.SRPBrands)
                .WithRequired()
                .HasForeignKey(k => k.SRPId);
        }
    }
}
