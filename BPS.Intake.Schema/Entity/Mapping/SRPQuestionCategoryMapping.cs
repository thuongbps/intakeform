﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class SRPQuestionCategoryMapping : BaseEntityWithVersionMapping<SRPQuestionCategory, int>
    {
        public SRPQuestionCategoryMapping()
        {
            ToTable("SRPQuestionCategories").HasKey(k => k.Id);
        }
    }
}
