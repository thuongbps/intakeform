﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class SRPQuestionGroupMapping : BaseEntityWithVersionMapping<SRPQuestionGroup, int>
    {
        public SRPQuestionGroupMapping()
        {
            ToTable("SRPQuestionGroups").HasKey(k => k.Id);
            HasRequired(r => r.SRPStatus)
                .WithMany(m => m.SRPQuestionGroups)
                .HasForeignKey(k => k.SRPStatusId)
                .WillCascadeOnDelete(false);
        }
    }
}
