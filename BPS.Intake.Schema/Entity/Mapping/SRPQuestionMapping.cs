﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class SRPQuestionMapping : BaseEntityWithVersionMapping<SRPQuestion, int>
    {
        public SRPQuestionMapping()
        {
            ToTable("SRPQuestions").HasKey(k => k.Id);
            HasRequired(r => r.QuestionCategory)
                .WithMany(m => m.Questions)
                .HasForeignKey(k => k.QuestionCategoryId)
                .WillCascadeOnDelete(false);
            HasRequired(r => r.QuestionGroup)
                .WithMany(m => m.Questions)
                .HasForeignKey(k => k.QuestionGroupId)
                .WillCascadeOnDelete(false);
        }
    }
}
