﻿using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class SRPStatusMapping : BaseEntityWithVersionMapping<SRPStatus, int>
    {
        public SRPStatusMapping()
        {
            ToTable("SRPStatuses").HasKey(k => k.Id);
        }
    }
}
