﻿using HvN.Lib.Data.EF.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Schema.Entity.Mapping
{

    public class SessionFormMapping : BaseEntityWithVersionMapping<SessionForm, int>
    {
        public SessionFormMapping()
        {
            ToTable("SessionForms").HasKey(k => k.Id);
        }
    }
}
