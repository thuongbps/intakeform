﻿using HvN.Lib.Data.EF.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class TransitionNextPageMapping : BaseEntityWithVersionMapping<TransitionNextPage, int>
    {
        public TransitionNextPageMapping()
        {
            ToTable("TransitionNextPages").HasKey(k => k.Id);
        }
    }
}
