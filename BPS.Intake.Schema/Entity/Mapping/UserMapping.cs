﻿using System;
using HvN.Lib.Data.EF.Mapping;

namespace BPS.Intake.Schema.Entity.Mapping
{
    public class UserMapping : BaseEntityWithVersionMapping<User, Guid>
    {
        public UserMapping()
        {
            HasMany(b => b.Roles)
                   .WithMany(m => m.Users)
                   .Map(cs =>
                   {
                       cs.MapLeftKey("UserRefId");
                       cs.MapRightKey("RoleRefId");
                       cs.ToTable("UserRoles");
                   });
        }
    }
}
