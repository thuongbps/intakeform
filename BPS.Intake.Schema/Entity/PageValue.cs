﻿using HvN.Lib.Data.Seedwork.Entity;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class PageValue : AbstractEntity
    {
        public int SessionId { get; set; }
        public int ControlId { get; set; }
        public string Value { get; set; }
    }
}
