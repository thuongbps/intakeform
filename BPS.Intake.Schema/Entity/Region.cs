﻿using HvN.Lib.Data.Seedwork.Entity;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class Region : BaseEntity<int>
    {
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public bool IsMultipleChoice { get; set; }
        public int? GroupID { get; set; }
        public int? DependentID { get; set; }
        public int? DisplayOrder { get; set; }
        public string TypeOfObject { get; set; }
        public int? DependencyLevel { get; set; }
        public string DependencyWhere { get; set; }
        public string Validation { get; set; }
        public bool IsRequire { get; set; }
        public int? MaxLength { get; set; }
        public string TextMode { get; set; }
        public virtual IList<SRPRegion> SRPRegions { get; set; }
        public virtual IList<DropdownDataSource> DropdownDataSources { get; set; }
        //ddls
    }
}
