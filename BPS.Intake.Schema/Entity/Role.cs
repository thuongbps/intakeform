﻿using System.Collections.Generic;
using BPS.Intake.Infrastructure.Types;
using HvN.Lib.Data.Seedwork.Entity;
using System;

namespace BPS.Intake.Schema.Entity
{
    public class Role : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public RoleType RoleType { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public Guid? DeletedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }
        public bool IsDeleted { get; set; }
        public virtual IList<User> Users { get; set; }
    }
}
