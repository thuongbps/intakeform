﻿using System;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class SRP : AbstractEntity
    {
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public int SRPStatusId { get; set; }
        public virtual SRPStatus SRPStatus { get; set; }
        public virtual IList<SRPAnswer> SRPAnswers { get; set; }
        public virtual IList<SRPRegion> SRPRegions { get; set; }
        public virtual IList<SRPBrand> SRPBrands { get; set; }
    }
}
