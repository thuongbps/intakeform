﻿using System;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class SRPAnswer : AbstractEntity
    {
        public int QuestionId { get; set; }
        public int CurrentGroupId { get; set; }
        public int SRPId { get; set; }
        public virtual SRPQuestion Question { get; set; }
        public virtual SRP SRP { get; set; }
        public virtual IList<SRPExtraAnswer> SRPExtraAnswers { get; set; }
    }
}
