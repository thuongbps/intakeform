﻿using System;

namespace BPS.Intake.Schema.Entity
{
    public class SRPBrand
    {
        public virtual int BrandId { get; set; }
        public virtual int SRPId { get; set; }
        public virtual string Value { get; set; }
        public virtual SRP SRP { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual byte[] Version { get; set; }
        public virtual Guid? CreatedBy { get; set; }
        public virtual Guid? UpdatedBy { get; set; }
        public virtual Guid? DeletedBy { get; set; }
        public virtual DateTimeOffset? CreatedDate { get; set; }
        public virtual DateTimeOffset? UpdatedDate { get; set; }
        public virtual DateTimeOffset? DeletedDate { get; set; }
        public virtual bool IsDeleted { get; set; }
    }
}
