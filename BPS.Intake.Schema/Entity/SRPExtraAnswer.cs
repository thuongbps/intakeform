﻿namespace BPS.Intake.Schema.Entity
{
    public class SRPExtraAnswer : AbstractEntity
    {
        public int SRPAnswerId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int ExtraAnswerValueTypeId { get; set; }
        public virtual SRPAnswer SRPAnswer { get; set; }
        public virtual ExtraAnswerValueType ExtraAnswerValueType { get; set; }
    }
}
