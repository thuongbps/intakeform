﻿using System;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class SRPQuestion : AbstractEntity
    {
        public string Content { get; set; }
        public int? ParentId { get; set; }
        public int QuestionGroupId { get; set; }
        public int? QuestionCategoryId { get; set; }
        public bool IsBeginQuestionGroup { get; set; }
        public bool IsEndQuetionGroup { get; set; }
        public bool IsDisable { get; set; }
        public bool IsMultipleChoice { get; set; }
        public int? NumberInput { get; set; }
        public string Description { get; set; }
        public virtual SRPQuestionCategory QuestionCategory { get; set; }
        public virtual SRPQuestionGroup QuestionGroup { get; set; }
        public virtual IList<SRPAnswer> Answers { get; set; }
    }
}
