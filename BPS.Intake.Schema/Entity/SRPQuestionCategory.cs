﻿using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class SRPQuestionCategory : AbstractEntity
    {
        public string Name { get; set; }        
        public virtual IList<SRPQuestion> Questions { get; set; }
    }
}
