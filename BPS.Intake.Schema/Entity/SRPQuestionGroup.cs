﻿using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class SRPQuestionGroup : AbstractEntity
    {
        public string Name { get; set; }
        public int? NextGroupId { get; set; }
        public int? PreviousGroupId { get; set; }
        public int SRPStatusId { get; set; }
        public virtual IList<SRPQuestion> Questions { get; set; }
        public virtual SRPStatus SRPStatus { get; set; }
    }
}
