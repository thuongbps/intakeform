﻿using BPS.Intake.Infrastructure.Types;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class SRPStatus : AbstractEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int? NextStatusId { get; set; }
        public int? PreviousStatusId { get; set; }
        public SRPStatusType Type { get; set; }
        public virtual IList<SRP> SRPs { get; set; }
        public virtual IList<SRPQuestionGroup> SRPQuestionGroups { get; set; }
    }
}
