﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Schema.Entity
{
    public class SessionForm : AbstractEntity
    {
        public string FormName { get; set; }
    }
}
