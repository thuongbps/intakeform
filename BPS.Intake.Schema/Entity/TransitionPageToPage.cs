﻿using HvN.Lib.Data.Seedwork.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPS.Intake.Schema.Entity
{
    public class TransitionPageToPage : BaseEntity<int>
    {
        public int SessionId { get; set; }
        public int FromPageId { get; set; }
        public int ToPageId { get; set; }
    }
}
