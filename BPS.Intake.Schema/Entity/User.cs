﻿using HvN.Lib.Data.Seedwork.Entity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace BPS.Intake.Schema.Entity
{
    public class User : BaseEntity<Guid>, IUser<Guid>
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OnQUserType { get; set; }
        public string OnQUserHotels { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public Guid? DeletedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }
        public bool IsDeleted { get; set; }
        public virtual IList<Role> Roles { get; set; }
    }
}
