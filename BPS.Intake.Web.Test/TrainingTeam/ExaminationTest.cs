﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using HvN.OTT.Infrastructure.Types;
using HvN.OTT.Domain.Dto;
using HvN.OTT.Domain.Service;
using HvN.OTT.Infrastructure;
using HvN.OTT.Web.Areas.TrainingTeam.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using HvN.OTT.Web.Areas.TrainingTeam.Models.Examination;

namespace HvN.OTT.Web.Test.TrainingTeam
{
    [TestClass]
    public class ExaminationTest
    {
        private Mock<IExaminationService> _examServiceMock;
        private Mock<IExaminationQuestionService> _examQuestionServiceMock;
        private Mock<IGroupService> _groupServiceMock;
        private Mock<ICategoryService> _categoryServiceMock;
        private ExaminationController _controller;
        private List<ExaminationDto> _listExamDto;
        private Mock<IResourceProvider> _resourceProvider;
        private Mock<IUserExamService> _userExamService;

        [TestInitialize]
        public void Initialize()
        {
            _examServiceMock = new Mock<IExaminationService>();
            _examQuestionServiceMock = new Mock<IExaminationQuestionService>();
            _groupServiceMock = new Mock<IGroupService>();
            _categoryServiceMock = new Mock<ICategoryService>();
            _resourceProvider = new Mock<IResourceProvider>();

            _controller = new ExaminationController(_examServiceMock.Object,
                _examQuestionServiceMock.Object,
                _groupServiceMock.Object,
                _userExamService.Object,
                _categoryServiceMock.Object,
                _resourceProvider.Object);
            _listExamDto = new List<ExaminationDto>
            {
                new ExaminationDto
                {
                    Name = "C#.NET 1",
                    Description = "Programming in C#",
                    ExamType = ExaminationType.Fixed,
                    LearningDuration = TimeSpan.Parse("0:30:00"),
                    ExamDuration = TimeSpan.Parse("0:30:00"),
                    CreatedDate = DateTimeOffset.UtcNow,
                    UpdatedDate = DateTimeOffset.UtcNow,
                    PassRate = 80
                },
                new ExaminationDto
                {
                    Name = "C#.NET 2",
                    Description = "Programming in C#",
                    ExamType = ExaminationType.Fixed,
                    LearningDuration = TimeSpan.Parse("0:30:00"),
                    ExamDuration = TimeSpan.Parse("0:30:00"),
                    CreatedDate = DateTimeOffset.UtcNow,
                    UpdatedDate = DateTimeOffset.UtcNow,
                    PassRate = 80
                },
                new ExaminationDto
                {
                    Name = "C#.NET 3",
                    Description = "Programming in C#",
                    ExamType = ExaminationType.Fixed,
                    LearningDuration = TimeSpan.Parse("0:30:00"),
                    ExamDuration = TimeSpan.Parse("0:30:00"),                  
                    PassRate = 80
                },
                new ExaminationDto
                {
                    Name = "C#.NET 4",
                    Description = "Programming in C#",
                    ExamType = ExaminationType.Fixed,
                    LearningDuration = TimeSpan.Parse("0:30:00"),
                    ExamDuration = TimeSpan.Parse("0:30:00"),
                    CreatedDate = DateTimeOffset.UtcNow,
                    UpdatedDate = DateTimeOffset.UtcNow,
                    PassRate = 80
                },
            };
        }

        [TestMethod]
        public void GetAll()
        {
            _examServiceMock.Setup(x => x.GetAllExamination()).Returns(_listExamDto);
            var result = (List<ExaminationDto>)(((ViewResult)_controller.ListTest()).Model);
            Assert.AreEqual(result.Count, 4);
            Assert.AreEqual("C#.NET 1", result[0].Name);
            Assert.AreEqual("C#.NET 2", result[1].Name);
            Assert.AreEqual("C#.NET 3", result[2].Name);
        }

        [TestMethod]
        public void Create()
        {
            var result = ((ViewResult) _controller.Create());
            Assert.IsNotNull(result);
        }

        //[TestMethod]
        //public void CreateExaminationSuccess()
        //{
        //    var model = new CreateEditExamModel
        //    {
        //        Id = new Guid(),
        //        Name = "exam zzz",
        //        Description = "aaa",
        //        ExamType = ExaminationType.Fixed,
        //        PassRate = 80,
        //        ExamDuration = TimeSpan.FromMinutes(200)
        //    };
        //    var result = (Guid)(((ViewResult)_controller.Create(model, model.ExaminationLevel).Result).Model);
        //    Assert.IsNotNull(result);
        //}

        [TestMethod]
        public void Edit()
        {
            Guid id = Guid.Parse("313cc2b7-4ca9-e511-aa49-0071c20a803a");           
            var result = _controller.Edit(id);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void EditExaminationSuccess()
        {
            var model = new CreateEditExamModel
            {
                Id = Guid.Parse("313cc2b7-4ca9-e511-aa49-0071c20a803a"),
                Name = "exam zzz",
                Description = "aaa",
                ExamType = ExaminationType.Fixed,
                PassRate = 80,
                ExamDuration = TimeSpan.FromMinutes(200)
            };
            var result = _controller.Edit(model);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DetailSuccess()
        {
            Guid id = Guid.Parse("313cc2b7-4ca9-e511-aa49-0071c20a803a");
            var result = _controller.Details(id);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeleteSuccess()
        {
            Guid id = Guid.Parse("313cc2b7-4ca9-e511-aa49-0071c20a803a");
            var result = _controller.Details(id);
            Assert.IsNotNull(result);
        }
    }
}
