﻿using System;
using System.Net;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BPS.Intake.Infrastructure.Types;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Web.Controllers;
using BPS.Intake.Web.Library.Extensions;
namespace BPS.Intake.Web.ActionFilter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class IntakeAuthorize : AuthorizeAttribute
    {
        private readonly IUserStoreService _userStoreService;
        public new RoleType Roles { get; set; }

        public IntakeAuthorize()
        {
            _userStoreService = DependencyResolver.Current.GetService<IUserStoreService>();
            Roles = RoleType.User;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated) // todo: Thuong double check, can use session
            {
                var currentUser = ((Controller)filterContext.Controller).GetUserInfo();
                if (currentUser == null)
                {
                    filterContext.Result = new RedirectResult("/Account/Login");
                }
                else
                {
                    UserDto user;
                    if (currentUser != null && currentUser.Roles != null)
                    {
                        user = new UserDto
                        {
                            Roles = currentUser.Roles
                        };
                    }
                    else
                    {
                        var username = HttpContext.Current.User.Identity.Name;
                        user = _userStoreService.GetUserRolesByNameAsync(username).Result;
                    }

                    if (user == null) filterContext.Result = new HttpUnauthorizedResult();
                    else
                    {
                        if (!user.IsInRoles(Roles)) filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                    }
                }
            }
            else
            {
                if (filterContext.HttpContext.Request.Url != null)
                    filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new
                                {
                                    controller = "Account",
                                    action = "Login",
                                    area = "",
                                    returnUrl =
                                        filterContext.HttpContext.Request.Url.GetComponents(
                                            UriComponents.PathAndQuery, UriFormat.SafeUnescaped)
                                }));
                else filterContext.Result = new RedirectResult("/Account/Login");
            }
        }
    }
}