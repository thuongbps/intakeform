﻿using System.Web.Optimization;

namespace BPS.Intake.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/waitingfor.js"));

            bundles.Add(new ScriptBundle("~/bundles/layout").Include(
                              "~/Scripts/colResizable-1.5.min.js",
                              "~/Scripts/jquery.nanoscroller.min.js",
                              "~/Scripts/notify.js",
                              "~/Scripts/jquery.blockUI.js",
                              "~/Scripts/select2.min.js",
                              "~/Scripts/tipr.js",
                              "~/Scripts/modernizr.custom.js",
                              "~/Scripts/Utilities.js",
                              "~/Scripts/jquery.countdown.js",
                              "~/Scripts/jquery.moment.js",
                              "~/Scripts/SummerNoteEditor.js",
                              "~/Content/admin-lte/js/adminlte.js",
                              //"~/Scripts/DataTables/dataTables.responsive.js",
                              "~/Scripts/DataTables/jquery.dataTables.js",
                              "~/Scripts/DataTables/dataTables.bootstrap4.js"
                              ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                                         "~/Content/bootstrap.css",
                                         "~/Content/font-awesome.css",
                                         "~/Content/nanoscroller.css",
                                         "~/Content/jquery-ui.css",
                                         "~/Content/jquery.steps.css",
                                         "~/Content/select2.css",
                                         "~/Content/select2-override.css",
                                         "~/Content/hint.css",
                                         "~/Content/Customize.css",
                                         "~/Scripts/Document/yendifplayer/yendifplayer.css",
                                         "~/Content/Style.css",
                                         "~/Content/admin-lte/css/AdminLTE.css",
                                         "~/Content/admin-lte/css/skins/_all-skins.css",
                                          "~/Content/admin-lte/bootstrap/css/bootstrap.css",
                                         "~/Content/styles/pagination.css",
                                         "~/Content/DataTables/css/dataTables.bootstrap4.css"
                         ));

            bundles.Add(new StyleBundle("~/Content/dropzonescss").Include(
                                "~/Scripts/dropzone/css/basic.css",
                                "~/Scripts/dropzone/css/dropzone.css"));

            bundles.Add(new ScriptBundle("~/bundles/Document").Include(
                                         "~/Scripts/Document/yendifplayer/yendifplayer.js"));

            bundles.Add(new ScriptBundle("~/bundles/dropzonescripts").Include(
                                         "~/Scripts/dropzone/dropzone.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/wizard").Include(
                                         "~/Scripts/jquery.bootstrap.wizard.js",
                                         "~/Scripts/jquery.steps.js"));

            bundles.Add(new ScriptBundle("~/bundles/chosen").Include(
                                      "~/Scripts/chosen/chosen.jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetime").Include(
                                      "~/Scripts/DateTime/moment-with-locales.js",
                                      "~/Scripts/DateTime/bootstrap-datetimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/chosencss").Include(
                                      "~/Scripts/chosen/css/chosen.css"));

            bundles.Add(new StyleBundle("~/Content/datetimecss").Include(
                                     "~/Scripts/DateTime/css/bootstrap-datetimepicker.css"));

        }
    }
}
