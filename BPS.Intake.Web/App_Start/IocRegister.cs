﻿using HvN.Lib.Data.Seedwork.Repository.SessionStorage;
using HvN.Lib.IoC.Seedwork.IoC;
using HvN.Lib.Shared.Utils;
using HvN.Lib.Web.SessionStorage;
using BPS.Intake.Web.Library.Injection;
using BPS.Intake.Web.Library.Management;

namespace BPS.Intake.Web
{
    public class IocRegister : IContainerRegister
    {
        public void Register(IContainerManager container)
        {
            container.AddComponent(typeof(ApplicationUserManager), typeof(ApplicationUserManager));
            container.AddComponent(typeof(ApplicationSignInManager), typeof(ApplicationSignInManager));

            container.RegisterControllers(new ApplicationTypeFinder());
            container.RegisterApiControllers(new ApplicationTypeFinder());
            container.AddComponentInstance(typeof(IDbContextStorage), new WebSessionStorage());
        }

    }
}