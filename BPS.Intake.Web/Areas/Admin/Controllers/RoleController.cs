﻿using BPS.Intake.Infrastructure.Types;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Infrastructure;
using BPS.Intake.Web.ActionFilter;
using System;
using System.Linq;
using System.Web.Mvc;
using BPS.Intake.Web.Areas.Admin.Models;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using BPS.Intake.Web.Controllers;
using BPS.Intake.Web.Library.Extensions;
using Microsoft.AspNet.Identity;

namespace BPS.Intake.Web.Areas.Admin.Controllers
{
    [IntakeAuthorize(Roles = RoleType.Admin)]
    public class RoleController : BaseController
    {
        private readonly IUserRoleStore<UserDto, Guid> _userRoleStore;
        private readonly IUserStoreService _userStoreService;
        private readonly IUserService _userService;
        
        private readonly IMapper _mapper;

        public RoleController(IUserRoleStore<UserDto, Guid> userRoleStore, IUserService userService, IMapper mapper, IResourceProvider resourceProvider)
            : base(resourceProvider)
        {
            _userRoleStore = userRoleStore;
            _userService = userService;
            _userStoreService = userRoleStore as IUserStoreService;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index()
        {
            var listUserRole = await _userStoreService.GetAllUserRolesAsync();
            listUserRole = listUserRole.Where(c => c.UserName != User.Identity.Name).ToList();
            var listRole = await _userStoreService.GetAllRoleAsync();

            return View(new UserRoleViewModel {ListRole = listRole, ListUser = listUserRole});
        }

        public async Task<bool> AssignUserrole(Guid userId, string roleType)
        {

            if (User.Identity.GetUserGuid() == userId) return true;
            var userRepo = await _userService.GetUserById(userId);
            var userDto = _mapper.MapTo<UserDto>(userRepo);
            var checkIsInRole = await _userRoleStore.IsInRoleAsync(userDto, roleType);

            if (checkIsInRole)
            {
                await _userRoleStore.RemoveFromRoleAsync(userDto, roleType);
            }
            else
            {
                await _userRoleStore.AddToRoleAsync(userDto, roleType);
            }
            return true;
        }
    }
}
