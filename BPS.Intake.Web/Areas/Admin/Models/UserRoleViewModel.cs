﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BPS.Intake.Domain.Dto;

namespace BPS.Intake.Web.Areas.Admin.Models
{
    public class UserRoleViewModel
    {
        public IList<UserDto> ListUser { get; set; }
        public IList<RoleDto> ListRole { get; set; }
    }
}