﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;
using BPS.Intake.Infrastructure.Types;
using BPS.Intake.Domain.Service;
using BPS.Intake.Web.Areas.TrainingTeam.Models.CommonSearch;
using System;


namespace BPS.Intake.Web.Areas.TrainingTeam.Controllers
{
    public class CommonSearchFilterController : Controller
    {
        private readonly IQuestionGroupService _groupService;
        private readonly IQuestionCategoryService _categoryService;

        public CommonSearchFilterController(IQuestionGroupService groupService, IQuestionCategoryService categoryService)
        {
            _groupService = groupService;
            _categoryService = categoryService;
        }

        private List<SelectListItem> GetSelectList(IEnumerable item, string dataValueField = "Id", string dataTextField = "Name")
        {
            List<SelectListItem> items = new SelectList(item, dataValueField, dataTextField).ToList();
            items.Insert(0, (new SelectListItem { Text = "-- Select an item --", Value = "" }));
            return items;
        }
        private List<SelectListItem> GetSelectList(IEnumerable item,object selectedValue, string dataValueField = "Id", string dataTextField = "Name")
        {
            List<SelectListItem> items = new SelectList(item, dataValueField, dataTextField, selectedValue).ToList();
            items.Insert(0, (new SelectListItem { Text = "-- Select an item --", Value = "" }));
            return items;
        }

        //public async Task<PartialViewResult> ControlSearchCategory(CategoryType categoryType)
        //{
        //    var categoryDtoList = await _categoryService.GetByType(categoryType);
        //    return PartialView("~/Areas/TrainingTeam/Views/CommonSearchFilter/_ControlSearchDropDownList.cshtml",
        //        new DropDownViewModel() { SelectList = GetSelectList(categoryDtoList), Name = "CategorySelect" });
        //}

        //public async Task<PartialViewResult> ControlSearchGroup(GroupType groupType)
        //{
        //    var groupDtoList = await _groupService.GetGroupByType(groupType);
        //    return PartialView("~/Areas/TrainingTeam/Views/CommonSearchFilter/_ControlSearchDropDownList.cshtml",
        //        new DropDownViewModel() { SelectList = GetSelectList(groupDtoList), Name = "GroupSelect" });
        //}

        public PartialViewResult ControlSearchType(SRPQuestionCategory questionType)
        {
            var dictionary = new Dictionary<int, string>();
            var enumerationType = typeof(SRPQuestionCategory);
            foreach (int value in Enum.GetValues(enumerationType))
            {
                var name = Enum.GetName(enumerationType, value);
                dictionary.Add(value, name);
            }
            return PartialView("~/Areas/TrainingTeam/Views/CommonSearchFilter/_ControlSearchDropDownList.cshtml",
                new DropDownViewModel() { SelectList = GetSelectList(dictionary, (int)questionType, "Key", "Value"), Name = "TypeSelect"});
        }

    }

}