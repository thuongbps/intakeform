﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BPS.Intake.Web.Models;

namespace BPS.Intake.Web.Areas.TrainingTeam.Models.CommonSearch
{
    public class CommonSearchViewModel : PagingViewModel
    {
        public int CategoryType { get; set; }

        public int GroupType { get; set; }

        public string CategoryId { get; set; }

        public string GroupId { get; set; }

        public int Type { get; set; }

        public bool IsEditable { get; set; }
    }

    public class DropDownViewModel
    {
        public List<SelectListItem> SelectList { get; set; }

        public string Name { get; set; }
    }
}
