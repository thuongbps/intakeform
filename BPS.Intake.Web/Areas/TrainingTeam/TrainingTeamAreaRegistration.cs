﻿using System.Web.Mvc;

namespace BPS.Intake.Web.Areas.TrainingTeam
{
    public class TrainingTeamAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TrainingTeam";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            context.Routes.MapMvcAttributeRoutes();
            context.MapRoute(
                "TrainingTeam_default",
                "TrainingTeam/{controller}/{action}/{id}",
                new {controller = "Home" , action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}