﻿using BPS.Intake.Domain.Dto;
using BPS.Intake.Web.Models;
using BPS.Intake.Web.Models.SRP;
using HvN.Lib.ModuleMapping.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web
{
    public partial class Startup
    {
        public void Configure()
        {
            AutoMapper.Mapper.CreateMap<JQueryDataTableParamModel, JQueryDataTableParamDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRPModel, SRPDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRPRegionModel, SRPRegionDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<SRPBrandModel, SRPBrandDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<RegionModel, RegionDto>().ReverseMap();
            AutoMapper.Mapper.CreateMap<DropdownDataSourceDto, DropdownDataSourceModel>().ReverseMap();
        }
    }
}