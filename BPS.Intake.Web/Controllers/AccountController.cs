﻿using System.Threading.Tasks;
using System.Web.Mvc;
using BPS.Intake.Infrastructure.Types;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using BPS.Intake.Web.Library.Management;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Net;
using System.Web.Routing;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using BPS.Intake.Web.Library.Extensions;
using System.Configuration;
using System.Web.Security;
using BPS.Intake.Infrastructure;

namespace BPS.Intake.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly IUserService _userService;
        private readonly IWebFormService _webFormService;
        private IMapper _mapper;
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IMapper mapper, IUserService userService, IWebFormService webFormService)
        {
            if (signInManager == null)
            {
                throw new ArgumentNullException("Invalid argument named signInManager");
            }

            if (userManager == null)
            {
                throw new ArgumentNullException("Invalid argument named userManager");
            }

            _userManager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _webFormService = webFormService;
            _mapper = mapper;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public async Task<ActionResult> Login(string returnUrl)
        {
            try
            {
                string evn = ConfigurationManager.AppSettings["Environment"];
                if(evn != "development")
                {
                    if (returnUrl == "/")
                    {
                        returnUrl = IntakeConstants.DomainName;
                    }

                    string authLoginUrl = "https://onqinsider.hilton.com/insider/onqlogin/?APPURL=" + returnUrl; //Request.Url.OriginalString;
                    string metadataFilePath = ConfigurationManager.AppSettings["UserSubListMetadataFilePath"];
                    string logoutUrl = ConfigurationManager.AppSettings["AuthenticationLogoutUrl"];

                    var logiInfo = new Authentication.SAMLAuth(authLoginUrl, metadataFilePath, _webFormService, _signInManager, logoutUrl).Authenticate();

                    switch (logiInfo.IsSucess)
                    {
                        case SignInStatus.Success:
                            FormsAuthentication.SetAuthCookie(logiInfo.OnQId, true);
                            if (!string.IsNullOrEmpty(returnUrl))
                            {
                                return RedirectToLocal(returnUrl);
                            }
                            return await RedirectByRole(logiInfo.OnQId);
                        case SignInStatus.Failure:
                            return RedirectToLocal(logoutUrl);
                    }
                }
                else
                {
                    var localUser = ConfigurationManager.AppSettings["DefaulLocalUserLogin"];
                    FormsAuthentication.SetAuthCookie(localUser, true);

                    return await RedirectByRole(localUser);
                }
            }
            catch (Exception ex)
            {
                Response.Cookies["SessionID"].Expires = DateTime.Now.AddYears(-1);
                log.Info("Login failed: " + ex.Message);
            }

            return null;
        }

        public async Task<PartialViewResult> LoginProfile()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
                return PartialView();
            UserDto user;
            var userInfo = User.GetUserInfo();
            if (userInfo != null)
            {
                user = new UserDto
                {
                    FirstName = userInfo.FirstName,
                    LastName = userInfo.LastName
                };
            }
            else
            {
                user = await _userService.FindByUserName(User.Identity.Name);
            }

            return PartialView("_loginProfile", user);
        }
        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            new Authentication.SAMLAuth().AbandonSession();

            return Redirect(ConfigurationManager.AppSettings["LogoutUrl"]);
        }

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("Index", "Home");
        }

        private async Task<ActionResult> RedirectByRole(string userName)
        {
            if (HttpContext.Request.IsAjaxRequest()) return RedirectToAction("Login", "Account");
            var user = await _userService.FindByUserName(userName);
            if (user == null) return new HttpUnauthorizedResult();
            var roles = user.Roles.Select(s => s.RoleType).ToList();
            if (!roles.Any()) return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (roles.Contains(RoleType.Admin))
            {
                return  new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new
                        {
                            controller = "Role",
                            action = "Index",
                            area = "Admin"
                        }));
            }
            else if (roles.Contains(RoleType.Trainning))
            {
                return new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new
                        {
                            controller = "Examination",
                            action = "List",
                            area = "TrainingTeam"
                        }));
            }
            else return new RedirectToRouteResult(
                new RouteValueDictionary(
                    new
                    {
                        controller = "Home",
                        action = "Index",
                        area = ""
                    }));
        }

        #endregion
    }
}