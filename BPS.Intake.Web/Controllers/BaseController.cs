﻿using System.Web.Mvc;
using BPS.Intake.Infrastructure;

namespace BPS.Intake.Web.Controllers
{
    public class BaseController : Controller
    {
        protected IResourceProvider ResourceProvider { get; private set; }

        // public static ApplicationUser CurrentUser { get; set; }

        public BaseController(IResourceProvider resourceProvider)
        {
            ResourceProvider = resourceProvider;
        }
    }
}