﻿using System;
using System.Web.Mvc;
using System.Threading.Tasks;
using BPS.Intake.Infrastructure.Types;
using BPS.Intake.Domain.Service;
using BPS.Intake.Infrastructure;
using BPS.Intake.Web.ActionFilter;
using System.Net;

namespace BPS.Intake.Web.Controllers
{
    [IntakeAuthorize(Roles = RoleType.User)]
    public class HomeController : BaseController
    {
        private readonly IQuestionService _questionService;

        public HomeController(
            IResourceProvider resourceProvider,
            IQuestionService questionService
            )
            : base(resourceProvider)
        {
            _questionService = questionService;
        }

        public ActionResult Index()
        {
            return View();
        }

       
    }
}