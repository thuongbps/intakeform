﻿using System.Web.Mvc;
using BPS.Intake.Infrastructure;
using BPS.Intake.Web.Library.Extensions;
using BPS.Intake.Web.Models.Profile;
using System.Linq;

namespace BPS.Intake.Web.Controllers
{
    public class ProfileController : BaseController
    {
        public ProfileController(IResourceProvider resourceProvider) : base(resourceProvider)
        {
        }
        public ActionResult Index()
        {
            var user = User.GetUserInfo();

            var model = new UserProfileModel
                            {
                                UserName = user.UserName,
                                SurName = user.LastName,
                                Email = user.Email,
                                GivenName = user.FirstName,
                                //LineManager = user.LineManger,
                                Roles = user.Roles.Select(s => s.Name).ToList()
                            };

            return View(model);
        }
    }
}