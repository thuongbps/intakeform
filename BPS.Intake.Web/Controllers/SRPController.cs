﻿using System.Linq;
using System.Web.Mvc;
using BPS.Intake.Infrastructure;
using BPS.Intake.Domain.Service;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Web.Models;
using System.Threading.Tasks;
using HvN.Lib.CrossCutting.Seedwork.Mapping;
using BPS.Common.Utilities;
using BPS.Intake.Web.Models.SRP;
using System.Collections.Generic;
using BPS.Intake.Web.Library.Helper.Html;

namespace BPS.Intake.Web.Controllers
{
    public class SRPController : BaseController
    {
        private readonly ISRPService _srpService;
        private readonly IMapper _mapper;
        public SRPController(IResourceProvider resourceProvider, ISRPService srpService, IMapper mapper) 
            : base(resourceProvider)
        {
            _srpService = srpService;
            _mapper = mapper;
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: SRP
        public async Task<JsonResult> GetSRPs(JQueryDataTableParamModel param)
        {
            var paramDto = new JQueryDataTableParamDto();
            paramDto = _mapper.MapToInstance<JQueryDataTableParamDto>(param, paramDto);
            var dataGrid = await _srpService.GetSRPs(paramDto);

            return Json(new
            {
                data = from w in dataGrid.Result
                       select new[] {
                           w.Id.ToString(),
                           w.Name,
                           this.Url.Action("Edit", "SRP", new { id = w.Id }, this.Request.Url.Scheme),
                           w.Version.RowVersionToString()},
                draw = dataGrid.sEcho,
                recordsFiltered = dataGrid.iTotalDisplayRecords,
                recordsTotal = dataGrid.iTotalRecords,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NextRegion(int parentId, int sessionId)
        {
            //determine next page: get next page ID
            var model = new SRPModel();
            model.Regions = _mapper.MapTo<List<RegionModel>>(_srpService.GetRegion(parentId).Result);
            var regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions);
            var pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.GetPageValue(sessionId, regiontemp).Result);
            foreach (var item in pageValue)
            {
                var entity = model.Regions.Find(f => f.Id == item.ControlId);
                if (entity != null)
                {
                    if (item.Value == "True")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = true;
                    }
                    else if (item.Value == "False")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = false;
                    }
                    else
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).Value = item.Value;
                    }
                    //      model.SessionId = item.SessionId;
                }
            }

            var transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            var conditionGroupId = 0;
            var pageID = 0;
            foreach (var item in transition)
            {
                if (conditionGroupId == item.ConditionGroupId)
                {
                    pageID = 0;
                }
                else
                {
                    if (pageID>0)
                    {
                        break;
                    }
                }
                conditionGroupId = item.ConditionGroupId;
                if (pageID == 0)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                pageID = item.NextPageId;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                pageID = item.NextPageId;
                            }
                        }
                    }
                }
            }

            model.Regions = _mapper.MapTo<List<RegionModel>>(_srpService.GetRegion(pageID).Result);
            regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions);

            pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.GetPageValue(sessionId, regiontemp).Result);
            foreach (var item in pageValue)
            {
                var entity = model.Regions.Find(f => f.Id == item.ControlId);
                if (entity != null)
                {
                    if (item.Value == "True")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = true;
                    }
                    else if (item.Value == "False")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = false;
                    }
                    else
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).Value = item.Value;
                    }
                    //      model.SessionId = item.SessionId;
                }
            }

            bool bnextPage = false;
            conditionGroupId = 0;
            transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            foreach (var item in transition)
            {
                if (conditionGroupId == item.ConditionGroupId)
                {
                    bnextPage = false;
                }
                else
                {
                    if (bnextPage)
                    {
                        break;
                    }
                }
                conditionGroupId = item.ConditionGroupId;
                if (!bnextPage)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                bnextPage = true;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                bnextPage = true;
                            }
                        }
                    }
                }
            }

            foreach (var item in model.Regions)
            {
                if (item.DependencyLevel != null)
                {
                    model.Regions.Find(f => f.Id == item.Id).DependencyLevel = item.DependencyLevel * 25;
                }
                //determine have next page
                //if (!bnextPage)
                //{
                //    if (item.TypeOfObject== "RadioButton" || item.TypeOfObject == "Checkbox")
                //    {
                //        if (item.IsChecked)
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }else if (item.TypeOfObject == "Dropdown" || item.TypeOfObject == "Textbox")
                //    {
                //        if (item.Value !=null && item.Value !="")
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //}
            }

            _mapper.MapTo<List<TransitionPageToPageDto>>(_srpService.SaveTransitionPageToPage(sessionId, parentId, pageID).Result);

            bool bprevPage = false;
            bprevPage = _srpService.HavePreviousPage(sessionId, pageID).Result;

            model.HavePreviousPage = bprevPage ? 1 : 0;
            model.HaveNextPage = bnextPage ? 1 : 0;
            model.SessionId = sessionId;
            return Json(new
            {
                view = HtmlHelperPartialToString.RenderPartialToString("~/Views/SRP/Inc/_FirstQuestion.cshtml", model, ControllerContext)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrevRegion(int parentId, int sessionId)
        {
            var model = new SRPModel();
            model.Regions = _mapper.MapTo<List<RegionModel>>(_srpService.GetRegionByParentID(sessionId,parentId).Result);

            var regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions);
            var pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.GetPageValue(sessionId, regiontemp).Result);

            foreach (var item in pageValue)
            {
                var entity = model.Regions.Find(f => f.Id == item.ControlId);
                if (entity != null)
                {
                    if (item.Value == "True")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = true;
                    }
                    else if (item.Value == "False")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = false;
                    }
                    else
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).Value = item.Value;
                    }
                }
            //    model.SessionId = item.SessionId;
            }

            bool bnextPage = false;
            var currentPage = 0;
            var conditionGroupId = 0;
            var transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            foreach (var item in transition)
            {
                currentPage = item.CurrentPageId;
                if (conditionGroupId == item.ConditionGroupId)
                {
                    bnextPage = false;
                }else
                {
                    if (bnextPage)
                    {
                        break;
                    }
                }
                conditionGroupId = item.ConditionGroupId;
                if (!bnextPage)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                bnextPage = true;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                bnextPage = true;
                            }
                        }
                    }
                }
            }

            foreach (var item in model.Regions)
            {
                if (item.DependencyLevel != null)
                {
                    model.Regions.Find(f => f.Id == item.Id).DependencyLevel = item.DependencyLevel * 25;
                }
                //determine have next page
                //if (!bnextPage)
                //{
                //    if (item.TypeOfObject == "RadioButton" || item.TypeOfObject == "Checkbox")
                //    {
                //        if (item.IsChecked)
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //    else if (item.TypeOfObject == "Dropdown" || item.TypeOfObject == "Textbox")
                //    {
                //        if (item.Value != null && item.Value != "")
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //}
            }

            bool bprevPage = false;
            bprevPage = _srpService.HavePreviousPage(sessionId, currentPage).Result;

            model.HavePreviousPage = bprevPage ? 1 : 0;
            model.HaveNextPage = bnextPage ? 1 : 0;
            model.SessionId = sessionId;
            return Json(new
            {
                view = HtmlHelperPartialToString.RenderPartialToString("~/Views/SRP/Inc/_FirstQuestion.cshtml", model, ControllerContext)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Checkbox_Change(int id,int parentId, bool bvalue,int sessionId)
        {
            
            var model = new SRPModel();
            var response = _srpService.GetRegion(parentId).Result;
            model.Regions = _mapper.MapTo<List<RegionModel>>(response);
            var regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions);
            
            var value = "";
            if (bvalue)
            {
               value = "True";
            }
            else
            {
                value = "False";
            }
            var pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId,id,value).Result);
            foreach (var item in pageValue)
            {
                var entity = model.Regions.Find(f => f.Id == item.ControlId);
                if (entity != null)
                {
                    if (item.Value == "True")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = true;
                    }
                    else if (item.Value == "False")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = false;
                    }
                    else
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).Value = item.Value;
                    }
                }
            }
            //remove dependency value
            foreach (var itemRegion in model.Regions)
            {
                if (itemRegion.DependentID != null)
                {
                    foreach (var itemdetail in model.Regions)
                    {
                        if (itemRegion.DependentID == itemdetail.Id)
                        {
                            if (itemdetail.TypeOfObject == "RadioButton" || itemdetail.TypeOfObject == "Checkbox")
                            {
                                if ((itemRegion.DependencyWhere == "True" && !itemdetail.IsChecked) || (itemRegion.DependencyWhere == "False" && itemdetail.IsChecked))
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }

                            if (itemdetail.TypeOfObject == "Dropdown" || itemdetail.TypeOfObject == "Textbox")
                            {
                                if (itemRegion.DependencyWhere != itemdetail.Value)
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }

            bool bnextPage = false;
            var conditionGroupId = 0;
            var transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            foreach (var item in transition)
            {
                if (conditionGroupId == item.ConditionGroupId)
                {
                    bnextPage = false;
                }else
                {
                    if (bnextPage)
                    {
                        break;
                    }
                }
                conditionGroupId = item.ConditionGroupId;
                if (!bnextPage)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                bnextPage = true;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                bnextPage = true;
                            }
                        }
                    }
                }
            }

            foreach (var item in model.Regions)
            {
                if (item.DependencyLevel != null)
                {
                    model.Regions.Find(f => f.Id == item.Id).DependencyLevel = item.DependencyLevel * 25;
                }
                //determine have next page
                //if (!bnextPage)
                //{
                //    if (item.TypeOfObject == "RadioButton" || item.TypeOfObject == "Checkbox")
                //    {
                //        if (item.IsChecked)
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //    else if (item.TypeOfObject == "Dropdown" || item.TypeOfObject == "Textbox")
                //    {
                //        if (item.Value != null && item.Value != "")
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //}
            }

            bool bprevPage = false;
            bprevPage = _srpService.HavePreviousPage(sessionId, parentId).Result;

            model.HavePreviousPage = bprevPage ? 1 : 0;
            model.HaveNextPage = bnextPage ? 1 : 0;
            model.SessionId = sessionId;
            return Json(new
            {
                view = HtmlHelperPartialToString.RenderPartialToString("~/Views/SRP/Inc/_FirstQuestion.cshtml", model, ControllerContext)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Radio_Change(int id, int parentId, bool bvalue, int sessionId)
        {

            var model = new SRPModel();
            model.Regions = _mapper.MapTo<List<RegionModel>>(_srpService.GetRegion(parentId).Result);
            var regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions);
            // determine GroupID
            var groupID = model.Regions.Find(f => f.Id == id).GroupID;

            foreach (var itemRegion in model.Regions)
            {
                if (itemRegion.GroupID == groupID)
                {
                    _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "False").Result);
                }
            }

            var value = "";
            if (bvalue)
            {
                value = "True";
            }
            else
            {
                value = "False";
            }

            var pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, id, value).Result);
            foreach (var item in pageValue)
            {
                var entity = model.Regions.Find(f => f.Id == item.ControlId);
                if (entity != null)
                {
                    if (item.Value == "True")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = true;
                    }
                    else if (item.Value == "False")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = false;
                    }
                    else
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).Value = item.Value;
                    }
                }
            }

            //remove dependency value
            foreach (var itemRegion in model.Regions)
            {
                if (itemRegion.DependentID != null)
                {
                    foreach (var itemdetail in model.Regions)
                    {
                        if (itemRegion.DependentID == itemdetail.Id)
                        {
                            if (itemdetail.TypeOfObject == "RadioButton" || itemdetail.TypeOfObject == "Checkbox")
                            {
                                if ((itemRegion.DependencyWhere == "True" && !itemdetail.IsChecked) || (itemRegion.DependencyWhere == "False" && itemdetail.IsChecked))
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }

                            if (itemdetail.TypeOfObject == "Dropdown" || itemdetail.TypeOfObject == "Textbox")
                            {
                                if (itemRegion.DependencyWhere != itemdetail.Value)
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(sessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }

            bool bnextPage = false;
            var conditionGroupId = 0;
            var transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            foreach (var item in transition)
            {
                if (conditionGroupId == item.ConditionGroupId)
                {
                    bnextPage = false;
                }else
                {
                    if (bnextPage)
                    {
                        break;
                    }
                }
                
                conditionGroupId = item.ConditionGroupId;
                if (!bnextPage)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                bnextPage = true;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                bnextPage = true;
                            }
                        }
                    }
                }
            }

            foreach (var item in model.Regions)
            {
                if (item.DependencyLevel != null)
                {
                    model.Regions.Find(f => f.Id == item.Id).DependencyLevel = item.DependencyLevel * 25;
                }
                //determine have next page
                //if (!bnextPage)
                //{
                //    if (item.TypeOfObject == "RadioButton" || item.TypeOfObject == "Checkbox")
                //    {
                //        if (item.IsChecked)
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //    else if (item.TypeOfObject == "Dropdown" || item.TypeOfObject == "Textbox")
                //    {
                //        if (item.Value != null && item.Value != "")
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //}
            }

            bool bprevPage = false;
            bprevPage = _srpService.HavePreviousPage(sessionId, parentId).Result;

            model.HavePreviousPage = bprevPage ? 1 : 0;
            model.HaveNextPage = bnextPage ? 1 : 0;
            model.SessionId = sessionId;
            return Json(new
            {
                view = HtmlHelperPartialToString.RenderPartialToString("~/Views/SRP/Inc/_FirstQuestion.cshtml", model, ControllerContext)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Textbox_Blur(SaveTempModel smodel)
        {

            var model = new SRPModel();
            model.Regions = _mapper.MapTo<List<RegionModel>>(_srpService.GetRegion(smodel.ParentId).Result);
            var regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions);

            var pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, smodel.Id, smodel.Value).Result);
            foreach (var item in pageValue)
            {
                var entity = model.Regions.Find(f => f.Id == item.ControlId);
                if (entity != null)
                {
                    if (item.Value == "True")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = true;
                    }
                    else if(item.Value == "False")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = false;
                    }
                    else
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).Value = item.Value;
                    }
                }
            }

            //remove dependency value
            foreach (var itemRegion in model.Regions)
            {
                if (itemRegion.DependentID != null)
                {
                    foreach (var itemdetail in model.Regions)
                    {
                        if (itemRegion.DependentID == itemdetail.Id)
                        {
                            if (itemdetail.TypeOfObject == "RadioButton" || itemdetail.TypeOfObject == "Checkbox")
                            {
                                if ((itemRegion.DependencyWhere == "True" && !itemdetail.IsChecked) || (itemRegion.DependencyWhere == "False" && itemdetail.IsChecked))
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }

                            if (itemdetail.TypeOfObject == "Dropdown" || itemdetail.TypeOfObject == "Textbox")
                            {
                                if (itemRegion.DependencyWhere != itemdetail.Value)
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }

            bool bnextPage = false;
            var conditionGroupId = 0;
            var transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            foreach (var item in transition)
            {
                if (conditionGroupId == item.ConditionGroupId)
                {
                    bnextPage = false;
                }
                else
                {
                    if (bnextPage)
                    {
                        break;
                    }
                }
                conditionGroupId = item.ConditionGroupId;
                if (!bnextPage)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                bnextPage = true;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                bnextPage = true;
                            }
                        }
                    }
                }
            }

            foreach (var item in model.Regions)
            {
                if (item.DependencyLevel != null)
                {
                    model.Regions.Find(f => f.Id == item.Id).DependencyLevel = item.DependencyLevel * 25;
                }
                //determine have next page
                //if (!bnextPage)
                //{
                //    if (item.TypeOfObject == "RadioButton" || item.TypeOfObject == "Checkbox")
                //    {
                //        if (item.IsChecked)
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //    else if (item.TypeOfObject == "Dropdown" || item.TypeOfObject == "Textbox")
                //    {
                //        if (item.Value != null && item.Value != "")
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //}
            }

            bool bprevPage = false;
            bprevPage = _srpService.HavePreviousPage(smodel.SessionId, smodel.ParentId).Result;

            model.HavePreviousPage = bprevPage ? 1 : 0;

            model.HaveNextPage = bnextPage ? 1 : 0;
            model.SessionId = smodel.SessionId;
            return Json(new
            {
                view = HtmlHelperPartialToString.RenderPartialToString("~/Views/SRP/Inc/_FirstQuestion.cshtml", model, ControllerContext)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Dropdown_Change(SaveTempModel smodel)
        {

            var model = new SRPModel();
            model.Regions = _mapper.MapTo<List<RegionModel>>(_srpService.GetRegion(smodel.ParentId).Result);
            var regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions);

            var pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, smodel.Id, smodel.Value).Result);
            foreach (var item in pageValue)
            {
                var entity = model.Regions.Find(f => f.Id == item.ControlId);
                if (entity != null)
                {
                    if (item.Value == "True")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = true;
                    }
                    else if (item.Value == "False")
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).IsChecked = false;
                    }
                    else
                    {
                        model.Regions.Find(f => f.Id == item.ControlId).Value = item.Value;
                    }
                }
            }

            //remove dependency value
            foreach (var itemRegion in model.Regions)
            {
                if (itemRegion.DependentID != null)
                {
                    foreach (var itemdetail in model.Regions)
                    {
                        if (itemRegion.DependentID == itemdetail.Id)
                        {
                            if (itemdetail.TypeOfObject == "RadioButton" || itemdetail.TypeOfObject == "Checkbox")
                            {
                                if ((itemRegion.DependencyWhere == "True" && !itemdetail.IsChecked) || (itemRegion.DependencyWhere == "False" && itemdetail.IsChecked))
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }

                            if (itemdetail.TypeOfObject == "Dropdown" || itemdetail.TypeOfObject == "Textbox")
                            {
                                if (itemRegion.DependencyWhere != itemdetail.Value)
                                {
                                    if (itemRegion.TypeOfObject == "RadioButton" || itemRegion.TypeOfObject == "Checkbox")
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "False").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).IsChecked = false;
                                    }
                                    else
                                    {
                                        _mapper.MapTo<List<PageValueDto>>(_srpService.SavePageValue(smodel.SessionId, itemRegion.Id, "").Result);
                                        model.Regions.Find(f => f.Id == itemRegion.Id).Value = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }

            bool bnextPage = false;
            var conditionGroupId = 0;
            var transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            foreach (var item in transition)
            {
                if (conditionGroupId == item.ConditionGroupId)
                {
                    bnextPage = false;
                }
                else
                {
                    if (bnextPage)
                    {
                        break;
                    }
                }
                conditionGroupId = item.ConditionGroupId;
                if (!bnextPage)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                bnextPage = true;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                bnextPage = true;
                            }
                        }
                    }
                }
            }

            foreach (var item in model.Regions)
            {
                if (item.DependencyLevel != null)
                {
                    model.Regions.Find(f => f.Id == item.Id).DependencyLevel = item.DependencyLevel * 25;
                }
                //determine have next page
                //if (!bnextPage)
                //{
                //    if (item.TypeOfObject == "RadioButton" || item.TypeOfObject == "Checkbox")
                //    {
                //        if (item.IsChecked)
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //    else if (item.TypeOfObject == "Dropdown" || item.TypeOfObject == "Textbox")
                //    {
                //        if (item.Value != null && item.Value != "")
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //}
            }

            bool bprevPage = false;
            bprevPage = _srpService.HavePreviousPage(smodel.SessionId, smodel.ParentId).Result;

            model.HavePreviousPage = bprevPage ? 1 : 0;
            model.HaveNextPage = bnextPage ? 1 : 0;
            model.SessionId = smodel.SessionId;
            return Json(new
            {
                view = HtmlHelperPartialToString.RenderPartialToString("~/Views/SRP/Inc/_FirstQuestion.cshtml", model, ControllerContext)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Add()
        {
            var model = new SRPModel();
            PopulateData(model);

            return View("Edit", model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            //PopulateData(model);
            return View();
        }

        [HttpPost]
        public ActionResult Save()
        {
            //
            return View("Index");
        }

        private void PopulateData(SRPModel model)
        {
            model.Regions = _mapper.MapTo<List<RegionModel>>(_srpService.GetRegion(1).Result);
            var regiontemp = _mapper.MapTo<List<RegionDto>>(model.Regions); 
            // initialize sectionId for new 
            var pageValue = _mapper.MapTo<List<PageValueDto>>(_srpService.GetPageValue(null, regiontemp).Result);
            foreach (var item in pageValue)
            {
                model.SessionId = item.SessionId;
                    break;
            }

            bool bnextPage = false;
            var conditionGroupId = 0;
            var transition = _mapper.MapTo<List<TransitionNextPageDto>>(_srpService.GetCurrentPage(regiontemp).Result);
            foreach (var item in transition)
            {
                if (conditionGroupId == item.ConditionGroupId)
                {
                    bnextPage = false;
                }
                else
                {
                    if (bnextPage)
                    {
                        break;
                    }
                }
                conditionGroupId = item.ConditionGroupId;
                if (!bnextPage)
                {
                    var entity = model.Regions.Find(f => f.Id == item.ControlId);
                    if (entity != null)
                    {
                        if (entity.TypeOfObject == "RadioButton" || entity.TypeOfObject == "Checkbox")
                        {
                            if ((item.WithValue == "True" && entity.IsChecked) || (item.WithValue == "False" && !entity.IsChecked))
                            {
                                bnextPage = true;
                            }
                        }
                        else if (entity.TypeOfObject == "Dropdown" || entity.TypeOfObject == "Textbox")
                        {
                            if (entity.Value == item.WithValue)
                            {
                                bnextPage = true;
                            }
                        }
                    }
                }
            }

            bool bprevPage = false;
            bprevPage = _srpService.HavePreviousPage(model.SessionId, 1).Result;

            foreach (var item in model.Regions)
            {
                if (item.DependencyLevel != null)
                {
                    model.Regions.Find(f => f.Id == item.Id).DependencyLevel = item.DependencyLevel * 25;
                }
                //determine have next page
                //if (!bnextPage)
                //{
                //    if (item.TypeOfObject == "RadioButton" || item.TypeOfObject == "Checkbox")
                //    {
                //        if (item.IsChecked)
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //    else if (item.TypeOfObject == "Dropdown" || item.TypeOfObject == "Textbox")
                //    {
                //        if (item.Value != null && item.Value != "")
                //        {
                //            bnextPage = _srpService.HaveNextPage(item.Id).Result;
                //        }
                //    }
                //}
            }

            model.HaveNextPage = bnextPage?1:0;
            model.HavePreviousPage = bprevPage ? 1 : 0;
        }
    }
}