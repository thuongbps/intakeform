﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using HvN.Lib.Data.Seedwork.Repository.SessionStorage;
using HvN.Lib.IoC.Autofac;
using BPS.Intake.Application;
using BPS.Intake.Web.Library.Injection;
using BPS.Intake.Web.Library.Binders;

namespace BPS.Intake.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = false;
            ModelBinders.Binders.DefaultBinder = new IntakeModelBinder();
            Bootstrapper.Boot();
            AutofacInstaller.Install();
            DependencyResolver.SetResolver(new AutofacDependencyResolver());
        }

        protected void Application_EndRequest()
        {
            var webSessionStorage = AutofacEngine.Instance.Resolve<IDbContextStorage>();
            webSessionStorage.Dispose();
        }

    }
}
