﻿using HvN.Lib.ModuleMapping.AutoMapper;
using BPS.Intake.Domain.Dto;
using BPS.Intake.Web.Models;

namespace BPS.Intake.Web.Library
{
    public class AutoMappingConfiguration : AutoMapperModuleMappingBase
    {
        public override void Configure()
        {
            ConfigureLogin();
        }

        private void ConfigureLogin()
        {
            AutoMapper.Mapper.CreateMap<LoginDto, LoginModel>().ReverseMap();
        }
    }
}