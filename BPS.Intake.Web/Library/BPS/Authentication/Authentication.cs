﻿using BPS.Common.Utilities;
#if !LOCAL_DEBUG
using BPSAuthentication;
#endif
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

/// <summary>
/// Summary description for Authentication
/// </summary>

namespace BPS.Common.Authentication {
    public class WebFormAuthenticationHandler : IHttpHandler, IRequiresSessionState {
        public bool IsReusable {
            get {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context) {
            HttpContext.Current.Response.StatusCode = 200;
            //HttpContext.Current.Response.Write(JsonConvert.SerializeObject(new WebFormAuthenticationHelper(), new Newtonsoft.Json.Converters.StringEnumConverter()));
        }
    }

    public class WebFormAuthenticationHelper {
        #region Constants
        private const string COOKIE_AUTH_USER_ID = "sesUserID";
        private const string COOKIE_AUTH_USER_NAME = "sesUserName";
        private const string COOKIE_AUTH_USER_EMAIL = "sesUserEmail";
        private const string COOKIE_AUTH_USER_TYPE = "sesOnQUserType";
        private const string COOKIE_AUTH_USER_HOTELS = "sesOnQUserHotels";

        private const string SAML_SESSION_LOGGED_IN = "LoggedIn";
        private const string SAML_SESSION_LOGGED_USER = "LoggedUser";

        private const string COOKIE_SESSION_ID = "SessionID";

        private const string CONF_SSO_USERNAME = "SSO_UserName";
        private const string CONF_SSO_PASSWORD = "SSO_Password";
        private const string CONF_SSO_DOMAIN = "SSO_Domain";

        private const string SECURITY_SESSION_USER_ID = "userid";
        private const string SECURITY_SESSION_USER_NAME = "userdesc";
        private const string SECURITY_SESSION_USER_EMAIL = "emailaddress";

        private const int TABLE_USER_TYPE = 1;
        private const int TABLE_USER_HOTELS = 0;
        private const int ROW_USER_TYPE = 0;

        private const string FIELD_USER_TYPE = "UserType";
        private const string FIELD_USER_HOTEL = "Ctyhocn";

        private const char HOTEL_STRING_SEPERATOR = '|';
        #endregion

        public enum ServerType {
            PROD,
            UAT,
            DEV,
            Local
        }

        public string UserID { get; private set; }
        public string UserName { get; private set; }
        public string UserEmail { get; private set; }
        public string UserType { get; private set; }

        public bool IsAuthenticated {
            get {
                return UserID != null;
            }
        }

        public bool IsUserCorporate {
            get {
                return UserType == "C";
            }
        }

        public ServerType HostServerType {
            get {
                if (Context.Request.Url.Host.ToLower() == "localhost") return ServerType.Local;
                else if (Context.Server.MapPath(".").IndexOf("DEV") > -1) return ServerType.DEV;
                else if (Context.Server.MapPath(".").IndexOf("UAT") > -1) return ServerType.UAT;
                else return ServerType.PROD;
            }
        }

        public string[] UserHotels { get; private set; }
        private HttpContext Context;

        //DONE
        private void ClearAuthentication() {
            Cookies.Remove(COOKIE_AUTH_USER_ID);
            Cookies.Remove(COOKIE_AUTH_USER_NAME);
            Cookies.Remove(COOKIE_AUTH_USER_EMAIL);
            Cookies.Remove(COOKIE_AUTH_USER_TYPE);
            Cookies.Remove(COOKIE_AUTH_USER_HOTELS);

            Cookies.Remove(COOKIE_SESSION_ID);

            UserID = null;
            UserName = null;
            UserEmail = null;
            UserType = null;
            UserHotels = new string[0];
        }

        //DONE
        private string[] ParseHotelsString(string Hotels) {
            if (Hotels == null) return new string[0];

            string[] hotels = Hotels.ToUpper().Split(HOTEL_STRING_SEPERATOR);
            List<string> hotelsFiltered = new List<string>();

            foreach (var s in hotels) {
                if (!string.IsNullOrEmpty(s))
                    hotelsFiltered.Add(s);
            }

            return hotelsFiltered.ToArray();
        }

        //DONE
        private void PerformPRODAuthentication() {
#if !LOCAL_DEBUG
            if (Context.Session[SAML_SESSION_LOGGED_IN] != null && Context.Session[SAML_SESSION_LOGGED_USER] != null) {
                LoginDetailDto loggedInUser = (LoginDetailDto)HttpContext.Current.Session[SAML_SESSION_LOGGED_USER];
                if (loggedInUser != null) {
                    UserID = loggedInUser.OnQId;
                    UserName = loggedInUser.FullName;
                    UserEmail = loggedInUser.Email.ToLower();
                    UserType = loggedInUser.OnQUserType;
                    UserHotels = ParseHotelsString(loggedInUser.OnQUserHotels);
                }
            }
#endif
        }

        // todo: thuong --> add hilton service and comment in
        private void PerformDEVUATAuthentication() {
//#if !LOCAL_DEBUG
//            string sessionID = Cookies.Get(COOKIE_SESSION_ID);

//            if (sessionID != null) {
//                if (!PerformCookieAuthentication()) {
//                    // Cookies not set, try authenticating with web service
//                    if (Context.Session[SAML_SESSION_LOGGED_IN] == null) {
//                        FormsAuthentication.RedirectToLoginPage(HttpContext.Current.Request.Url.ToString());
//                        HttpContext.Current.Response.End();
//                        return;
//                    }

//                    var securityWebService = new com.hilton.onqinsiderservices.clsPortalSecurityService();

//                    securityWebService.Credentials = new System.Net.NetworkCredential(
//                        ConfigurationManager.AppSettings[CONF_SSO_USERNAME],
//                        ConfigurationManager.AppSettings[CONF_SSO_PASSWORD],
//                        ConfigurationManager.AppSettings[CONF_SSO_DOMAIN]);

//                    try {
//                        UserID = securityWebService.SessionGetValue(sessionID, SECURITY_SESSION_USER_ID);
//                        UserName = securityWebService.SessionGetValue(sessionID, SECURITY_SESSION_USER_NAME);
//                        UserEmail = securityWebService.SessionGetValue(sessionID, SECURITY_SESSION_USER_EMAIL).ToLower();

//                        // Get user type and hotel list
//                        DataSet dsHotelList = securityWebService.HotelList(sessionID, 0);

//                        DataTable dtUserType = dsHotelList.Tables[TABLE_USER_TYPE];
//                        List<string> dtUserHotelsList = new List<string>();

//                        if (dtUserType.Rows.Count > 0) UserType = (string)dtUserType.Rows[ROW_USER_TYPE][FIELD_USER_TYPE];

//                        if (!IsUserCorporate) { // Corporate users have access to all hotels
//                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
//                            DataTable dtUserHotels = dsHotelList.Tables[TABLE_USER_HOTELS];

//                            foreach (DataRow row in dtUserHotels.Rows) dtUserHotelsList.Add(row[FIELD_USER_HOTEL].ToString());
//                        }

//                        UserHotels = dtUserHotelsList.ToArray();

//                        Cookies.Set(COOKIE_AUTH_USER_ID, UserID);
//                        Cookies.Set(COOKIE_AUTH_USER_NAME, UserName);
//                        Cookies.Set(COOKIE_AUTH_USER_EMAIL, UserEmail);
//                        Cookies.Set(COOKIE_AUTH_USER_TYPE, UserType);
//                        Cookies.Set(COOKIE_AUTH_USER_HOTELS,
//                            string.Join(HOTEL_STRING_SEPERATOR.ToString(), dtUserHotelsList.ToArray()));
//                    }
//                    catch (System.Exception) {
//                        ClearAuthentication();
//                    }
//                }
//            }
//            else ClearAuthentication();
//#endif
        }

        //DONE
        private void PerformLocalAuthentication() {
            if (File.Exists("C:\\BPS\\IS_PUBLIC")) {
                ClearAuthentication();
                return;
            }

            this.UserID = "jcate";
            this.UserName = "Jeremy Cate";
            this.UserEmail = "jcate@b-p-s.us";
            this.UserType = "C";
        }

        //DONE
        private bool PerformCookieAuthentication() {
            UserID = Cookies.Get(COOKIE_AUTH_USER_ID);
            UserName = Cookies.Get(COOKIE_AUTH_USER_NAME);
            UserEmail = Cookies.Get(COOKIE_AUTH_USER_EMAIL) != null ?
                Cookies.Get(COOKIE_AUTH_USER_EMAIL).ToLower() : null;
            UserType = Cookies.Get(COOKIE_AUTH_USER_TYPE);
            UserHotels = ParseHotelsString(Cookies.Get(COOKIE_AUTH_USER_HOTELS));

            return (UserID != null) && (UserName != null) && (UserEmail != null);
        }

        public WebFormAuthenticationHelper(HttpContext Context = null) {
            this.Context = Context == null ? HttpContext.Current : Context;

            if (HostServerType == ServerType.Local) PerformLocalAuthentication();
            else if (HostServerType == ServerType.DEV || HostServerType == ServerType.UAT) PerformDEVUATAuthentication();
            else PerformPRODAuthentication();
        }
    }
}