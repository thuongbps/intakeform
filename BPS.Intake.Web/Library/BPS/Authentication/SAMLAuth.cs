﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web.Security;
using System.Data.SqlClient;
using System.Security.Cryptography;
using BPSAuthentication;
using System.Data;
using System.Security;
using BPS.Intake.Web.Models;
using BPS.Intake.Web.Library.Management;
using Microsoft.AspNet.Identity.Owin;
using BPS.Intake.Domain.Service;
using BPS.Intake.Domain.Dto;
/// <summary>
/// SAML Security 2.0
/// </summary>
/// 
namespace BPS.Authentication
{
    public class SAMLAuth
    {
        private const string StatusNs = "urn:oasis:names:tc:SAML:2.0:status:";
        private string _authLoginUrl;
        private string _metadataFilePath;
        private string _logoutUrl;
        private readonly IWebFormService _webFormService;
        private readonly ApplicationSignInManager _signInManager;
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public SAMLAuth()
        { 
        }

        public SAMLAuth(string authLoginUrl, string metadataFileName, IWebFormService webFormService, ApplicationSignInManager signInManager, string logoutUrl = "")
        {
            _authLoginUrl = authLoginUrl;
            _metadataFilePath = metadataFileName;
            _logoutUrl = logoutUrl;
            _webFormService = webFormService;
            _signInManager = signInManager;
        }

        public LoginModel Authenticate()
        {
            var loginInfo = new LoginModel();

            try
            {
                var samlAssertionRaw = GetSamlFromHttpRequest();
                var certificateData = System.IO.File.ReadAllBytes(_metadataFilePath);
                loginInfo = AuthenticatePing(samlAssertionRaw, certificateData);
            }
            catch (System.Exception ex)
            {
                //BPSException.BPSException.LogException(_authLoginUrl, null, "SAML Authentication", ex.Message, "Email");
                log.Info("SAML Authentication: " + ex.Message);
                //todo: Thuong --> Sendmail
                loginInfo.IsSucess = SignInStatus.Failure;
            }

            return loginInfo;
        }

        private string GetSamlFromHttpRequest()
        {
            var sSessID = string.Empty;
            if(HttpContext.Current.Request.Cookies["SessionID"] != null)
            {
                sSessID = HttpContext.Current.Request.Cookies["SessionID"].Value;
            }

            // login
            if (System.Web.HttpContext.Current.Session["Timeout"] == null)
            {
                HttpContext.Current.Session["Timeout"] = true;
                HttpContext.Current.Response.Redirect(_authLoginUrl);
            }
           
            //// time out
            //if(HttpContext.Current.Session["Timeout"] == null && !string.IsNullOrEmpty(sSessID))
            //{
            //    if(!string.IsNullOrEmpty(_logoutUrl))
            //    {
            //        LogOut(_logoutUrl);
            //    }
            //}

            var rawSamlData = System.Web.HttpContext.Current.Request.Form["SAMLResponse"];
            // the sample data sent us may be already encoded, 
            // which results in double encoding
            if (rawSamlData.Contains("%"))
            {
                rawSamlData = HttpUtility.UrlDecode(rawSamlData);
            }
            if (rawSamlData == null) return null;
            var samlData = Convert.FromBase64String(rawSamlData);

            // read back into a UTF string
            var samlAssertion = Encoding.UTF8.GetString(samlData);
            return samlAssertion;
        }

        private LoginModel AuthenticatePing(string samlAssertionRaw, byte[] certificateData)
        {
            // load a new XML document
            var assertion = new XmlDocument { PreserveWhitespace = true };
            assertion.LoadXml(samlAssertionRaw);


            //*******************************************************************************************************
            //creates a new XML document
            var metadataFile = new XmlDocument { PreserveWhitespace = true };

            //creates a string from the metadata.xml file
            string xmlString = System.IO.File.ReadAllText(_metadataFilePath);

            //loads the string into the new xml document
            metadataFile.LoadXml(xmlString);


            //*******************************************************************************************************

            // use a namespace manager to avoid the worst of xpaths
            var ns = new XmlNamespaceManager(assertion.NameTable);
            ns.AddNamespace("samlp", @"urn:oasis:names:tc:SAML:2.0:protocol");
            ns.AddNamespace("saml", @"urn:oasis:names:tc:SAML:2.0:assertion");
            ns.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);


            //gets the Signature node from the metadata.xml file
            XmlNodeList nodeList = metadataFile.GetElementsByTagName("ds:Signature");

            //creates a new SignedXML document from the metadata.xml file
            SignedXml metadataSignedXML = new SignedXml(metadataFile.DocumentElement);

            //loads the Signature into the SignedXML document
            metadataSignedXML.LoadXml(nodeList[0] as XmlElement);


            // get the signature XML node from the SAML Response
            var signNode = assertion.SelectSingleNode("/samlp:Response/saml:Assertion/ds:Signature", ns);

            // load the XML signature from the SAML Response
            var signedXml = new SignedXml(assertion.DocumentElement);
            signedXml.LoadXml(signNode as XmlElement);


            //Gets the X509 cert from the SAML Response
            X509Certificate2 xmlCertificate = new X509Certificate2();
            xmlCertificate = GetFirstX509Certificate(signedXml);

            //Gets the X509 cert from the metadata.xml file
            X509Certificate2 metadataCertificate = new X509Certificate2();
            metadataCertificate = metadataSignedXML.KeyInfo.OfType<KeyInfoX509Data>().First().Certificates.OfType<X509Certificate2>().First();

            //Check the Signature of the SAML Response using the X509 cert from the metadata.xml file
            if (!signedXml.CheckSignature(metadataCertificate, true))
            {
                throw new SecurityException("Signature check failed.");
            }

            var statusNode = assertion.SelectSingleNode("/samlp:Response/samlp:Status/samlp:StatusCode", ns);
            var statusValue = statusNode.Attributes["Value"].Value.Replace(StatusNs, "");

            if (statusValue != "Success")
                throw new InvalidOperationException("Could not authenticate via PING. Please contact Hilton support for assistance");

            var attributes = assertion.SelectNodes("/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute", ns);
            var samlUserAttributes = new Dictionary<string, string>();

            foreach (XmlNode attribute in attributes)
            {
                //TODO: refactor? attributes is an XmlNodeList (not a List<XmlNode> or anything implementing IEnumerable) -> may be forced to loop
                var attributeName = attribute.Attributes["Name"].Value.ToLower();
                var attributeValue = attribute.InnerText;
                samlUserAttributes.Add(attributeName, attributeValue);
            }

            var onQId = assertion.SelectSingleNode("samlp:Response/saml:Assertion/saml:Subject/saml:NameID", ns).InnerText;
            var innCodes = string.Empty;

            foreach (XmlNode xmlNodes2 in assertion.SelectNodes("samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name = 'inncodes']/saml:AttributeValue", ns))
            {
                innCodes = string.Concat(innCodes, xmlNodes2.InnerText, "|");
            }

            var lobbyInnCodes = string.Empty;

            foreach (XmlNode xmlNodes2 in assertion.SelectNodes("samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name = 'lobbyuserinncodelist']/saml:AttributeValue", ns))
            {
                lobbyInnCodes = string.Concat(lobbyInnCodes, xmlNodes2.InnerText, "|");
            }

            //var innCodesResponse = _webFormService.GetCtyhocn(innCodes).GetAwaiter().GetResult();
            //var lobbyInnCodesResponse = _webFormService.GetCtyhocn(lobbyInnCodes).GetAwaiter().GetResult();

            //var innCodeParam = innCodesResponse.Count() == 0 ? string.Empty : string.Join("|", innCodesResponse);
            //var lobbyIncodeParam = lobbyInnCodesResponse.Count() == 0 ? string.Empty : string.Join("|", lobbyInnCodesResponse);

            var loginInfo = new LoginModel
            {
                Email = samlUserAttributes.ContainsKey("mail") ? HttpUtility.HtmlDecode(samlUserAttributes["mail"]) : null,
                FirstName = samlUserAttributes.ContainsKey("firstname") ? HttpUtility.HtmlDecode(samlUserAttributes["firstname"]) : null,
                LastName = samlUserAttributes.ContainsKey("lastname") ? HttpUtility.HtmlDecode(samlUserAttributes["lastname"]) : null,
				OnQUserType = samlUserAttributes.ContainsKey("lobbyusertype") ? HttpUtility.HtmlDecode(samlUserAttributes["lobbyusertype"]) : null,
                OnQId = HttpUtility.HtmlDecode(onQId),
                //OnQUserHotels = CombineInnCodes(lobbyIncodeParam, innCodeParam)
            };

            //_webFormService.UpdateUserLoginAudit(AutoMapper.Mapper.Map<LoginModel, LoginDto>(loginInfo), innCodeParam, lobbyIncodeParam);

            //Create cookie
            var cookie = new HttpCookie("IsAuth", "true");
            cookie.Expires = DateTime.Now.AddDays(1);
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);

            HttpCookie userNameCookie = new HttpCookie("sesUserName");
            userNameCookie.Value = HttpUtility.HtmlDecode(loginInfo.FullName);

            HttpCookie userIdCookie = new HttpCookie("sesUserID");
            userIdCookie.Value = HttpUtility.HtmlDecode(loginInfo.OnQId);

            HttpCookie userEmailCookie = new HttpCookie("sesUserEmail");
            userEmailCookie.Value = HttpUtility.HtmlDecode(loginInfo.Email);

            HttpCookie userTypeCookie = new HttpCookie("sesOnQUserType");
            HttpCookie userHotelsCookie = new HttpCookie("sesOnQUserHotels");
            
			if (!String.IsNullOrEmpty(loginInfo.OnQUserType))
			{
				if (loginInfo.OnQUserType == "C" || loginInfo.OnQUserType == "CE")
				{
					userTypeCookie.Value = "C";
					userHotelsCookie.Value = string.Empty;
				}
				else
				{
					userTypeCookie.Value = loginInfo.OnQUserType;
					userHotelsCookie.Value = loginInfo.OnQUserHotels;
				}
			}
			else
			{
				if (String.IsNullOrEmpty(loginInfo.OnQUserHotels))
				{
					userTypeCookie.Value = "C";
					userHotelsCookie.Value = string.Empty;
				}
				else
				{
					userTypeCookie.Value = "R";
					userHotelsCookie.Value = loginInfo.OnQUserHotels;
				}
			}

            HttpCookie accessToken = new HttpCookie("sesToken");
            accessToken.Value = GenerateToken(System.Web.HttpContext.Current.Request.Cookies["SessionID"].Value, loginInfo.OnQId, ConfigurationManager.AppSettings["SecretTokenKey"].ToString());;

            HttpContext.Current.Response.Cookies.Add(userNameCookie);
            HttpContext.Current.Response.Cookies.Add(userIdCookie);
            HttpContext.Current.Response.Cookies.Add(userEmailCookie);
            HttpContext.Current.Response.Cookies.Add(userTypeCookie);
            HttpContext.Current.Response.Cookies.Add(userHotelsCookie);
            HttpContext.Current.Response.Cookies.Add(accessToken);

            HttpContext.Current.Session["LoggedUser"] = loginInfo;
            HttpContext.Current.Session["LoggedIn"] = true;
            HttpContext.Current.Session["LoggedOut"] = null;

            // add user to database
            _signInManager.LoginDto = AutoMapper.Mapper.Map<LoginModel, LoginDto>(loginInfo);
            var result = _signInManager.PasswordSignInAsync(loginInfo.OnQId, "", true, shouldLockout: false).GetAwaiter().GetResult();

            loginInfo.IsSucess = result;

            return loginInfo;
        }

        private X509Certificate2 GetFirstX509Certificate(SignedXml signedXml)
        {
            return signedXml.KeyInfo.OfType<KeyInfoX509Data>().First().
                Certificates.OfType<X509Certificate2>().First();
        }

        private bool IsValidCertificate(X509Certificate2 certificate, byte[] certificateData)
        {
            // decode the keys
            var cms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber);
            cms.Decode(certificateData);

            // Placeholder for the certificate to validate

            // Placeholder for the extra collection of certificates to be used
            //var certificates = new X509Certificate2Collection();
            var certificates = cms.Certificates;

            var chain = new X509Chain
            {
                ChainPolicy =
                {
                    RevocationMode = X509RevocationMode.NoCheck,
                    VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority
                }
            };

            //chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
            chain.ChainPolicy.ExtraStore.AddRange(certificates);

            return chain.Build(certificate);
            // we have a keychain of X509Certificate2s, we need a collection of tokens
        }

        public void AbandonSession()
        {
            //Clear cookie
            ClearCookie(".OnQInsiderAuth");
            ClearCookie("SessionID");
            ClearCookie("IsAuth");
            ClearCookie("ASP.NET_SessionId");
            ClearCookie("AuthCookie");
            ClearCookie("sesUserName");
            ClearCookie("sesUserID");
            ClearCookie("sesUserEmail");
            ClearCookie("sesOnQUserType");
            ClearCookie("sesOnQUserHotels");
            ClearCookie("sesFormID");
            ClearCookie("sesFormName");
            ClearCookie("sesToken");

            //Clear session
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.RemoveAll();

            //Set variables to indicate user has been logged out
            System.Web.HttpContext.Current.Session["LoggedUser"] = null;
            System.Web.HttpContext.Current.Session["LoggedIn"] = null;
            System.Web.HttpContext.Current.Session["LoggedOut"] = true;
            System.Web.HttpContext.Current.Session["RequestedURL"] = null;
            System.Web.HttpContext.Current.Session["Timeout"] = null;

            //Redirect to logout page
            //System.Web.HttpContext.Current.Response.Redirect(logoutURL);
        }

        private void ClearCookie(string cookieName)
        {
            HttpCookie cookie = new HttpCookie(cookieName, null);
            cookie.Expires = DateTime.Now.AddYears(-1);
            System.Web.HttpContext.Current.Response.Cookies.Remove(cookieName);
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }
		
		private string CombineInnCodes(string inncodes1, string inncodes2)
        {
            List<string> ctyhocnList = new List<string>();
            string[] arr;

            if (!string.IsNullOrEmpty(inncodes1))
            {
                arr = inncodes1.Split('|');
                foreach (var item in arr)
                {
                    if (!ctyhocnList.Contains(item))
                    {
                        ctyhocnList.Add(item);
                    }
                }
            }

            if (!string.IsNullOrEmpty(inncodes2))
            {
                arr = inncodes2.Split('|');
                foreach (var item in arr)
                {
                    if (!ctyhocnList.Contains(item))
                    {
                        ctyhocnList.Add(item);
                    }
                }
            }

            return (ctyhocnList.Count > 0) ? String.Join("|", ctyhocnList.ToArray()) : string.Empty;
        }
		
        private string GenerateToken(string sessionId, string userId, string secretKey)
        {
            string plainText = string.Join(":", new string[] { sessionId, userId, DateTime.UtcNow.Ticks.ToString() });

            return Encrypt(plainText, secretKey);
        }

        //Ref: https://www.codeproject.com/Articles/14150/Encrypt-and-Decrypt-Data-with-C
        private string Encrypt(string toEncrypt, string secretKey)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(secretKey));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();

            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        private string Decrypt(string cipherString, string secretKey)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(secretKey));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateDecryptor();

            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();

            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}
