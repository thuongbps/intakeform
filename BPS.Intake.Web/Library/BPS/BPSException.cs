﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BPS.Workflow;

namespace BPS.BPSException
{
    /// <summary>
    /// Summary description for Exception
    /// </summary>
    public class BPSException
    {
        public BPSException()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void LogException(string Application, string CurrentUser, string Subject, string Body, string LogType)
        {
            User user = new User(CurrentUser);
            Email email;

            switch (LogType)
            {
                case "Email":
                    email = new Email("david@b-p-s.us;cong@b-p-s.us", "ERROR in " + Application + ": " + Subject, "The user " + user.Name + " had the following error report at " + DateTime.Now + ": " + Body);
                    email.Send();
                    break;
                case "Debug":
                    email = new Email("cong@b-p-s.us;huan@b-p-s.us", "ERROR in " + Application + ": " + Subject, "The user " + user.Name + " at " + DateTime.Now + ": " + Body);
                    email.Send();
                    break;
                default:
                    break;
            }
        }
    }

}