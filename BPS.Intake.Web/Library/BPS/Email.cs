﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace BPS.Workflow
{
    /// <summary>
    /// Summary description for Email
    /// </summary>
    public class Email
    {
        private string _SenderEmail;

        public string SenderEmail
        {
            get { return _SenderEmail; }
            set { _SenderEmail = value; }
        }

        private string _CCEmail;

        public string CCEmail
        {
            get { return _CCEmail; }
            set { _CCEmail = value; }
        }

        private string _ToEmail;

        public string ToEmail
        {
            get { return _ToEmail; }
            set { _ToEmail = value; }
        }
        
        private User _Recipient;

        public User Recipient
        {
            get { return _Recipient; }
            set { _Recipient = value; }
        }

        private string _Subject;

        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        private string _Body;

        public string Body
        {
            get { return _Body; }
            set { _Body = value; }
        }

        private int? _TaskID;

        public int? TaskID
        {
            get
            {
                if (null == _TaskID)
                {
                    return null;
                }
                else
                {
                    return _TaskID.Value;   
                }
            }
            set { _TaskID = value; }
        }

        private User _CurrentUser;

        public User CurrentUser
        {
            get { return _CurrentUser; }
            set { _CurrentUser = value; }
        }
        
        public Email(User Recipient, string Subject, string Body, int? TaskID = null, User CurrentUser = null)
        {
            if (TaskID != null)
            {
                _TaskID = (int)TaskID;
            }

            if (CurrentUser != null)
            {
                _CurrentUser = CurrentUser;
            }

            _SenderEmail = Recipient.Email;

            _Recipient = new User("dbrunow", false);
            _ToEmail = _Recipient.Email;
            _Subject = Subject;
            _Body = Body;
        }

        public Email(string From, string To, string CC, string Subject, string Body, int? TaskID = null, User CurrentUser = null)
        {
            if (TaskID != null)
            {
                _TaskID = (int)TaskID;
            }

            if (CurrentUser != null)
            {
                _CurrentUser = CurrentUser;
            }

            _SenderEmail = From;
            _ToEmail = To;
            _CCEmail = CC;
            _Subject = Subject;
            _Body = Body;
        }

        public Email(string RecipientEmail, string Subject, string Body)
        {
            _SenderEmail = "david.brunow@hilton.com";
            _Recipient = new User("", RecipientEmail, "");
            _ToEmail = RecipientEmail;
            _Subject = Subject;
            _Body = Body;
        }

        public string CleanEmailAddress(string emailAddress)
        {
            var email = emailAddress;

            if(email != null)
            {
                email = email.Replace(";", ",");
            }

            return email;
        }

        public void Send()
        {
            var userState = _ToEmail + " " + DateTime.Now;
            var smtpClient = new System.Net.Mail.SmtpClient();
            var emailServer = System.Configuration.ConfigurationManager.AppSettings["EmailServer"];

            smtpClient.Host = emailServer;
            smtpClient.ServicePoint.MaxIdleTime = 2;
            smtpClient.ServicePoint.ConnectionLimit = 1;

            var mailMessage = new System.Net.Mail.MailMessage();

            var isTesting = true;

            _ToEmail = CleanEmailAddress(_ToEmail);
            _CCEmail = CleanEmailAddress(_CCEmail);

            if(isTesting) {
                mailMessage.IsBodyHtml = true;
                mailMessage.From = new System.Net.Mail.MailAddress(_SenderEmail);
                
                if (!(string.IsNullOrWhiteSpace(_ToEmail)))
                {
                    mailMessage.To.Add(_ToEmail);
                }

                if(!(string.IsNullOrWhiteSpace(_CCEmail)))
                {
                    mailMessage.CC.Add(_CCEmail);
                }

                mailMessage.Subject = _Subject;
                mailMessage.Body = _Body.Replace("\n", "<br>");
            }

            smtpClient.Send(mailMessage);

            if (_TaskID != null)
            {
                var sSQL = "";
                var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
                var sqlCommand = new SqlCommand(sSQL, sqlConnection);

                sqlCommand.Parameters.AddWithValue("TaskID", _TaskID);
                sqlCommand.Parameters.AddWithValue("Sender", _CurrentUser.Email);
                sqlCommand.Parameters.AddWithValue("FromMailbox", _SenderEmail);
                sqlCommand.Parameters.AddWithValue("Recipient", _ToEmail);
                sqlCommand.Parameters.AddWithValue("CC", _CCEmail);
                sqlCommand.Parameters.AddWithValue("Subject", _Subject);
                sqlCommand.Parameters.AddWithValue("Body", _Body);
                sqlCommand.Parameters.AddWithValue("TimeStamp", DateTime.Now);

                sSQL = "INSERT INTO Worklog..TaskEmailHistory (TaskID, Sender, FromMailbox, Recipient, CC, Subject, Body, TimeStamp) VALUES (@TaskID, @Sender, @FromMailbox, @Recipient, @CC, @Subject, @Body, @TimeStamp)";

                sqlCommand.CommandText = sSQL;

                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();

                if (_Body.Contains("/surveys/"))
                {
                    TaskField.MarkSurveySent(_TaskID.Value);
                }
            }
        }

        public static List<Dictionary<string, string>> GetEmailHistory(long TaskID)
        {
            var emailHistory = new List<Dictionary<string, string>>();
            var email = new Dictionary<string, string>();

            var sSql = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSql, sqlConnection);
            sqlCommand.Parameters.AddWithValue("TaskID", TaskID);

            sSql = "SELECT Sender, Recipient, Subject, Body, TimeStamp FROM Worklog..TaskEmailHistory WHERE TaskID = @TaskID";

            sqlCommand.CommandText = sSql;

            sqlConnection.Open();

            using (var dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    email = new Dictionary<string, string>();

                    if (dr["Sender"] != DBNull.Value)
                    {
                        email.Add("Sender", (string)dr["Sender"]);
                    }

                    if (dr["Recipient"] != DBNull.Value)
                    {
                        email.Add("Recipient", (string)dr["Recipient"]);
                    }

                    if (dr["Subject"] != DBNull.Value)
                    {
                        email.Add("Subject", (string)dr["Subject"]);
                    }

                    if (dr["Body"] != DBNull.Value)
                    {
                        email.Add("Body", (string)dr["Body"]);
                    }

                    if (dr["TimeStamp"] != DBNull.Value)
                    {
                        email.Add("TimeStamp", ((DateTime)dr["TimeStamp"]).ToString());
                    }

                    emailHistory.Add(email);
                }
            }

            foreach (var emailAttachment in TaskAttachment.GetAllEmailTaskAttachments(TaskID))
            {
                email = new Dictionary<string, string>
                {
                    {
                        "EmailAttachmentName", emailAttachment.Name
                    },
                    {
                        "EmailAttachmentLink", emailAttachment.Location
                    },
                    {
                        "EmailAttachmentTime", emailAttachment.DateAttached
                    }
                };

                emailHistory.Add(email);
            }

            return emailHistory;
        }

        public static List<KeyValuePair<string, string>> GetSendBackText()
        {
            var sendBackTextList = new List<KeyValuePair<string, string>>();

            var sSQL = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSQL, sqlConnection);

            sSQL = "SELECT SB_Name, SB_Description, SB_Verbiage FROM Worklog..SendBackText";

            sqlCommand.CommandText = sSQL;

            sqlConnection.Open();

            string sendBackName = "",
                   sendBackDescription = "",
                   sendBackVerbiage = "";

            using (var dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (dr["SB_Name"] != DBNull.Value)
                    {
                        sendBackName = (string)dr["SB_Name"];
                    }
                    if (dr["SB_Description"] != DBNull.Value)
                    {
                        sendBackDescription = " - " + (string)dr["SB_Description"];
                    }
                    if (dr["SB_Verbiage"] != DBNull.Value)
                    {
                        sendBackVerbiage = (string)dr["SB_Verbiage"];
                    }

                    sendBackTextList.Add(new KeyValuePair<string, string>(sendBackVerbiage, sendBackName + sendBackDescription));
                }
            }
            sqlConnection.Close();

            return sendBackTextList;
        }
    }
}