﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace BPS.Workflow
{
    /// <summary>
    /// Summary description for TaskAttachment
    /// </summary>
    public class TaskAttachment
    {
        private long _ID;

        public long ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _Location;

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private DateTime _DateAttached;

        public string DateAttached
        {
            get { return _DateAttached.ToString(); }
            set { _DateAttached = DateTime.Parse(value); }
        }
        
        public TaskAttachment(long ID)
        {
            _ID = ID;

            var sSQL = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSQL, sqlConnection);
            sqlCommand.Parameters.AddWithValue("AttachmentID", ID);

            sSQL = "SELECT Location, Name, AttachedWhen FROM Worklog..TAttachments WHERE AttachmentID = @AttachmentID";

            sqlCommand.CommandText = sSQL;
            sqlConnection.Open();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (dr["Name"] != DBNull.Value)
                    {
                        _Name = (string)dr["Name"];
                    }
                    if (dr["AttachedWhen"] != DBNull.Value)
                    {
                        _DateAttached = (DateTime)dr["AttachedWhen"];
                    }
                    if (dr["Location"] != DBNull.Value)
                    {
                        _Location = GetDownloadURL((string)dr["Location"]);
                    }
                }
            }
            sqlConnection.Close();
        }

        public TaskAttachment(long TaskID, string Location, string Name)
        {
            _Location = Location;
            _Name = Name;
            _DateAttached = DateTime.Now;

            var sSQL = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSQL, sqlConnection);

            sqlCommand.Parameters.AddWithValue("TaskItemID", TaskID);

            sqlCommand.Parameters.AddWithValue("Location", _Location);
            sqlCommand.Parameters.AddWithValue("Name", _Name);
            sqlCommand.Parameters.AddWithValue("AttachedWhen", _DateAttached);

            sSQL = "INSERT INTO Worklog..TAttachments (TaskItemID, Location, Name, AttachedWhen) VALUES (@TaskItemID, @Location, @Name, @AttachedWhen)";

            sqlCommand.CommandText = sSQL;

            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public static List<TaskAttachment> GetAllEmailTaskAttachments(long taskId)
        {
            var sSql = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSql, sqlConnection);
            sqlCommand.Parameters.AddWithValue("TaskItemID", taskId);

            sSql = "SELECT AttachmentID FROM Worklog..TAttachments WHERE TaskItemID = @TaskItemID AND (Location LIKE '%.msg' OR Location LIKE '%.eml') ORDER BY AttachedWhen DESC";

            sqlCommand.CommandText = sSql;
            sqlConnection.Open();

            var taskAttachmentIdList = new List<int>();

            using (var dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    taskAttachmentIdList.Add((int)dr["AttachmentID"]);
                }
            }
            sqlConnection.Close();

            return taskAttachmentIdList.Select(taskAttachmentId => new TaskAttachment(taskAttachmentId)).ToList();
        }

        public static List<TaskAttachment> GetAllTaskAttachments(long TaskID)
        {
            var theseAttachments = new List<TaskAttachment>();
            var sSQL = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSQL, sqlConnection);
            sqlCommand.Parameters.AddWithValue("TaskItemID", TaskID);

            sSQL = "SELECT AttachmentID FROM Worklog..TAttachments WHERE TaskItemID = @TaskItemID AND Location NOT LIKE '%.msg' AND Location NOT LIKE '%.eml' ORDER BY AttachedWhen DESC";

            sqlCommand.CommandText = sSQL;
            sqlConnection.Open();

            var taskAttachmentIDList = new List<int>();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    taskAttachmentIDList.Add((int)dr["AttachmentID"]);
                }
            }
            sqlConnection.Close();

            foreach (int taskAttachmentID in taskAttachmentIDList)
            {
                theseAttachments.Add(new TaskAttachment(taskAttachmentID));
            }

            return theseAttachments;
        }

        private string GetDownloadURL(string location)
        {
            var url = location;
            var parts = location.Split('/');
            var subPos = -1;
	                
			if (location.IndexOf("/attachments/") != -1)
			{
				subPos = location.IndexOf("attachments");
				url = location.Substring(0, subPos) + "download.aspx?t=attachments&f=" + parts[parts.Length - 1];
			}	
            
			if (location.IndexOf("/att/") != -1)
			{
				subPos = location.IndexOf("att");
				url = location.Substring(0, subPos) + "worklog/download.aspx?t=att&y=" + parts[parts.Length - 3] + "&m=" + parts[parts.Length - 2] + "&f=" + parts[parts.Length - 1];
			}	
			
            return url;
        }
    }
}