﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace BPS.Workflow
{
    /// <summary>
    /// Summary description for TaskField
    /// </summary>
    public class TaskField
    {
        private long _ID;

        public long ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _Value;

        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private int _DisplayOrder;

        public int DisplayOrder
        {
            get { return _DisplayOrder; }
            set { _DisplayOrder = value; }
        }
        
        
        public TaskField(long ID)
        {
            _ID = ID;

            string sSQL = "";
            SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);
            sqlCommand.Parameters.AddWithValue("TaskFieldID", _ID);

            sSQL = "SELECT FieldName, FieldData FROM Worklog..TaskItemFields WHERE TaskFieldID = @TaskFieldID";

            sqlCommand.CommandText = sSQL;
            sqlConnection.Open();

            List<long> taskFieldIDList = new List<long>();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (DBNull.Value != dr["FieldName"])
                    {
                        _Name = (string)dr["FieldName"];
                    }
                    if (DBNull.Value != dr["FieldData"])
                    {
                        _Value = (string)dr["FieldData"];
                    }
                }
            }
            sqlConnection.Close();
        }

        public TaskField(string Name, string DefaultValue = null, int? DisplayOrder = null)
        {
            _Name = Name;
            _Value = DefaultValue;

            if (null != DisplayOrder)
            {
                _DisplayOrder = (int)DisplayOrder;
            }
        }

        public void Save(int? TaskID = null)
        {
            string sSQL = "";
            SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);

            if (null != _ID)
            {
                sqlCommand.Parameters.AddWithValue("TaskFieldID", _ID);
                sqlCommand.Parameters.AddWithValue("FieldName", _Name + "");
                sqlCommand.Parameters.AddWithValue("FieldData", _Value);

                sSQL = "UPDATE Worklog..TaskItemFields SET FieldName = @FieldName, FieldData = @FieldData WHERE TaskFieldID = @TaskFieldID";

                sqlCommand.CommandText = sSQL;

                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            else if(null != TaskID)
            {
                sqlCommand.Parameters.AddWithValue("TaskItemID", TaskID);
                sqlCommand.Parameters.AddWithValue("FieldName", _Name);
                sqlCommand.Parameters.AddWithValue("FieldData", _Value);
                sqlCommand.Parameters.AddWithValue("DisplayOrder", _DisplayOrder);

                sSQL = "INSERT INTO Worklog..TaskItemFields (TaskItemID, FieldName, FieldData, DisplayOrder) VALUES (@TaskItemID, @FieldName, @FieldData, @DisplayOrder)";

                sqlCommand.CommandText = sSQL;

                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }

        public static string GetSabreRateCode(long taskId)
        {
            var sabreRateCode = "";

            var sSql = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSql, sqlConnection);
            sqlCommand.Parameters.AddWithValue("TaskItemID", taskId);

            sSql = @"SELECT 
                    FieldData 
                    FROM Worklog..TaskItemFields 
                    WHERE TaskItemID = @TaskItemID 
                    AND FieldName = 'RateAccessCode_Sabre'
                    AND FieldData IS NOT NULL
                    AND FieldData <> ''";

            sqlCommand.CommandText = sSql;
            sqlConnection.Open();

            using (var dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (DBNull.Value != dr["FieldData"])
                    {
                        sabreRateCode = (string)dr["FieldData"];   
                    }
                }
            }
            sqlConnection.Close();

            return sabreRateCode;
        }

        public static List<TaskField> GetAllTaskFields(long TaskID)
        {
            List<TaskField> taskFieldList = new List<TaskField>();

            string sSQL = "";
            SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);
            sqlCommand.Parameters.AddWithValue("TaskItemID", TaskID);

            sSQL = "SELECT TaskFieldID FROM Worklog..TaskItemFields WHERE TaskItemID = @TaskItemID ORDER BY DisplayOrder ASC";

            sqlCommand.CommandText = sSQL;
            sqlConnection.Open();

            List<int> taskFieldIDList = new List<int>();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    taskFieldIDList.Add((int)dr["TaskFieldID"]);
                }
            }
            sqlConnection.Close();

            foreach (int taskFieldID in taskFieldIDList)
            {
                taskFieldList.Add(new TaskField(taskFieldID));
            }

            return taskFieldList;
        }

        public static List<TaskField> GetDefaultTaskFields(long QueueID)
        {
            List<TaskField> taskFieldList = new List<TaskField>();

            string sSQL = "";
            string tempFieldName = "",
                   tempFieldValueDefault = "";
            int tempDisplayOrderDefault = 0;

            SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);
            sqlCommand.Parameters.AddWithValue("TaskTypeID", QueueID);

            sSQL = "SELECT FieldName, FieldDataDefault, DisplayOrderDefault FROM Worklog..TaskTypeFields WHERE TaskTypeID = @TaskTypeID ORDER BY DisplayOrderDefault ASC";

            sqlCommand.CommandText = sSQL;
            sqlConnection.Open();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    tempFieldName = "";
                    tempFieldValueDefault = "";
                    tempDisplayOrderDefault = 0;

                    if(DBNull.Value != dr["FieldName"]) {
                        tempFieldName = (string)dr["FieldName"];
                    }
                    if (DBNull.Value != dr["FieldDataDefault"])
                    {
                        tempFieldValueDefault = (string)dr["FieldName"];
                    }
                    if (DBNull.Value != dr["DisplayOrderDefault"])
                    {
                        tempDisplayOrderDefault = (int)dr["DisplayOrderDefault"];
                    }
                    taskFieldList.Add(new TaskField(tempFieldName, tempFieldValueDefault, tempDisplayOrderDefault));
                }
            }
            sqlConnection.Close();

            return taskFieldList;

        }

        public static void MarkSurveySent(long taskId)
        {
            const string sql = @"INSERT INTO Worklog..TaskItemFields
                        (FieldName, FieldData, TaskItemID)
                        VALUES
                        ('WebSurvey', @TimeStamp, @TaskItemID)";

            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sql, sqlConnection);

            sqlCommand.Parameters.AddWithValue("TaskItemID", taskId);
            sqlCommand.Parameters.AddWithValue("TimeStamp", DateTime.Now);

            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }
    }

}