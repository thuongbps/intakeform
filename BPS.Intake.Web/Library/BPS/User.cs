﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace BPS.Workflow
{
	
    /// <summary>
    /// Summary description for User
    /// </summary>
    public class User
    {
        private string _ID;

        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _NetworkID;

        public string NetworkID
        {
            get { return _NetworkID; }
            set { _NetworkID = value; }
        }
        
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _Phone;

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        
        public string Name
        {
            get { return _GivenName + " " + _FamilyName; }
        }

        private string _GivenName;

        public string GivenName
        {
            get { return _GivenName; }
            set { _GivenName = value; }
        }

        private string _FamilyName;

        public string FamilyName
        {
            get { return _FamilyName; }
            set { _FamilyName = value; }
        }
        
        private string _Title;

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Office;

        public string Office
        {
            get { return _Office; }
            set { _Office = value; }
        }

        private DateTime _HireDate;

        public string HireDate
        {
            get { return _HireDate.ToString(); }
            set { _HireDate = DateTime.Parse(value); }
        }

        private string _TeamName;

        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }

        private string _SupervisorID;

        public string SupervisorID
        {
            get { return _SupervisorID; }
            set { _SupervisorID = value; }
        }

        private string _SupervisorName;

        public string SupervisorName
        {
            get { return _SupervisorName; }
            set { _SupervisorName = value; }
        }

        private string _EmployeeType;

        public string EmployeeType
        {
            get { return _EmployeeType; }
            set { _EmployeeType = value; }
        }

        private int _SecurityLevel;

        public int SecurityLevel
        {
            get { return _SecurityLevel; }
            set { _SecurityLevel = value; }
        }
        
        private bool _IsActive;

        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        private bool _IsManager;

        public bool IsManager
        {
            get { return _IsManager; }
            set { _IsManager = value; }
        }

        public User()
        {

        }

        public User(string ID, bool setupCurrentTask = true)
        {
            _NetworkID = ID;
            var sSql = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSql, sqlConnection);
            sqlCommand.Parameters.AddWithValue("NetworkID", _NetworkID);
            sqlCommand.Parameters.AddWithValue("UserName", ID);

            sSql = "SELECT FirstName, LastName, Title, Sec, Office, HireDate, TeamID, SupID, SecGroups, NetworkID, Active, EmpTypeID, Manager, Email, CurrentTaskID, UserName FROM Worklog..AllUsers WHERE NetworkID = @NetworkID OR UserName = @UserName";

            sqlCommand.CommandText = sSql;
            sqlConnection.Open();
            int teamId = -1,
                currentTaskId = 0;

            using (var dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (dr["FirstName"] != DBNull.Value)
                    {
                        _GivenName = (string)dr["FirstName"];
                    }
                    if (dr["LastName"] != DBNull.Value)
                    {
                        _FamilyName = (string)dr["LastName"];
                    }
                    if (dr["Title"] != DBNull.Value)
                    {
                        _Title = (string)dr["Title"];
                    }
                    if (dr["Sec"] != DBNull.Value)
                    {
                        _SecurityLevel = (byte)dr["Sec"];
                    }
                    if (dr["Office"] != DBNull.Value)
                    {
                        _Office = (string)dr["Office"];
                    }
                    if (dr["HireDate"] != DBNull.Value)
                    {
                        _HireDate = (DateTime)dr["HireDate"];
                    }
                    if (dr["TeamID"] != DBNull.Value)
                    {
                        teamId = (int)dr["TeamID"];
                    }
                    if (dr["SupID"] != DBNull.Value)
                    {
                        _SupervisorID = (string)dr["SupID"];
                    }
                    if (dr["UserName"] != DBNull.Value)
                    {
                        _ID = (string)dr["UserName"];
                    }
                    if (dr["Active"] != DBNull.Value)
                    {
                        _IsActive = (bool)dr["Active"];
                    }
                    if (dr["EmpTypeID"] != DBNull.Value)
                    {
                        _EmployeeType = (string)dr["EmpTypeID"];
                    }
                    if (dr["Manager"] != DBNull.Value)
                    {
                        _IsManager = (bool)dr["Manager"];
                    }
                    if (dr["Email"] != DBNull.Value)
                    {
                        _Email = (string)dr["Email"];
                    }
                    if (dr["CurrentTaskID"] != DBNull.Value) 
                    {
                        currentTaskId = (int)dr["CurrentTaskID"];
                    }
                    if (dr["NetworkID"] != DBNull.Value)
                    {
                        NetworkID = (string)dr["NetworkID"];
                    }
                }
            }
            sqlConnection.Close();

            //_MyTeam = new Team(teamId);
            //if (0 != currentTaskId && setupCurrentTask)
            //{
            //    _CurrentTask = new Task(currentTaskId);
            //}

            //if (!String.IsNullOrEmpty(_SupervisorID))
            //{
            //    var supervisor = new Supervisor(_SupervisorID);
            //    if (supervisor != null)
            //    {
            //        _SupervisorName = supervisor.Name;
            //    }
            //}
        }

        public User(string Name, string Email, string Phone)
        {
            _GivenName = Name;

            if (Name.Split(null).Length > 1)
            {
                _GivenName = Name.Split(null)[0];
                _FamilyName = Name.Replace(_GivenName + " ", "");
            }

            _Email = Email;
            _Phone = Phone;
        }

        //public void Update(Dictionary<string, object> data)
        //{
        //    var userData = data;
        //    var originalId = _NetworkID;

        //    if (userData.ContainsKey("ID"))
        //    {
        //        _ID = HttpUtility.HtmlEncode((string) userData["ID"]);
        //    }

        //    if (userData.ContainsKey("GivenName"))
        //    {
        //        _GivenName = HttpUtility.HtmlEncode((string)userData["GivenName"]);
        //    }

        //    if (userData.ContainsKey("FamilyName"))
        //    {
        //        _FamilyName = HttpUtility.HtmlEncode((string)userData["FamilyName"]);
        //    }

        //    if (userData.ContainsKey("Title"))
        //    {
        //        _Title = HttpUtility.HtmlEncode((string)userData["Title"]);
        //    }

        //    if (userData.ContainsKey("Email"))
        //    {
        //        _Email = HttpUtility.HtmlEncode((string)userData["Email"]);
        //    }

        //    if (userData.ContainsKey("TeamID"))
        //    {
        //        if (null != userData["TeamID"])
        //        {
        //            _MyTeam = new Team((int)userData["TeamID"]);   
        //        }
        //    }

        //    if (userData.ContainsKey("SupervisorID"))
        //    {
        //        _SupervisorID = HttpUtility.HtmlEncode((string)userData["SupervisorID"]);
        //    }

        //    if (userData.ContainsKey("IsManager"))
        //    {
        //        _IsManager = (bool)userData["IsManager"];
        //    }

        //    if (userData.ContainsKey("IsActive"))
        //    {
        //        _IsActive = (bool)userData["IsActive"];
        //    }

        //    var sSql = "";
        //    var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
        //    var sqlCommand = new SqlCommand(sSql, sqlConnection);

        //    sqlCommand.Parameters.AddWithValue("UserName", _ID);
        //    sqlCommand.Parameters.AddWithValue("NetworkID", _NetworkID);
        //    sqlCommand.Parameters.AddWithValue("FirstName", _GivenName);
        //    sqlCommand.Parameters.AddWithValue("LastName", _FamilyName);
        //    sqlCommand.Parameters.AddWithValue("Title", _Title);
        //    sqlCommand.Parameters.AddWithValue("Email", _Email);
        //    sqlCommand.Parameters.AddWithValue("TeamID", null == _MyTeam ? (object)DBNull.Value : _MyTeam.ID);
        //    sqlCommand.Parameters.AddWithValue("SupID", _SupervisorID ?? (object)DBNull.Value);
        //    sqlCommand.Parameters.AddWithValue("Manager", _IsManager);
        //    sqlCommand.Parameters.AddWithValue("Active", _IsActive);

        //    if ("-1" == originalId)
        //    {
        //        sSql = @"INSERT INTO Worklog..AllUsers 
        //                (UserName, NetworkID, FirstName, LastName, TeamID, SupID, Title, Email, Manager, Active) 
        //                VALUES 
        //                (@UserName, @NetworkID, @FirstName, @LastName, @TeamID, @SupID, @Title, @Email, @Manager, @Active)";

        //        sqlCommand.CommandText = sSql;

        //        sqlConnection.Open();
        //        sqlCommand.ExecuteNonQuery();
        //        sqlConnection.Close();
        //    }
        //    else
        //    {
        //        sSql = @"UPDATE Worklog..AllUsers
        //                 SET UserName = UserName, 
        //                 NetworkID = @NetworkID, 
        //                 FirstName = @FirstName,
        //                 LastName = @LastName,
        //                 TeamID = @TeamID,
        //                 SupID = @SupID,
        //                 Title = @Title,
        //                 Email = @Email,
        //                 Manager = @Manager,
        //                 Active = @Active
        //                 WHERE NetworkID = @NetworkID";

        //        sqlCommand.CommandText = sSql;

        //        sqlConnection.Open();
        //        sqlCommand.ExecuteNonQuery();
        //        sqlConnection.Close();
        //    }
        //}

        public void UpdateEmail(string Email)
        {
            string sSQL = "";
            SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);

            sqlCommand.Parameters.AddWithValue("NetworkID", _NetworkID);
            sqlCommand.Parameters.AddWithValue("Email", HttpUtility.HtmlEncode(Email));

            sSQL = "UPDATE Worklog..AllUsers SET Email = @Email WHERE NetworkID = @NetworkID";

            sqlCommand.CommandText = sSQL;

            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        //public void StartWorkingOnTask(int TaskID)
        //{
        //    if (this.CurrentTask != null)
        //    {
        //        this.CurrentTask.StopWork(this);
        //    }

        //    _CurrentTask = new Task(TaskID);

        //    this.CurrentTask.StartWork(this);

        //    string sSQL = "";
        //    SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
        //    SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);

        //    sqlCommand.Parameters.AddWithValue("NetworkID", _NetworkID);
        //    sqlCommand.Parameters.AddWithValue("CurrentTaskID", this.CurrentTask.ID);

        //    sSQL = "UPDATE Worklog..AllUsers SET CurrentTaskID = @CurrentTaskID WHERE NetworkID = @NetworkID";

        //    sqlCommand.CommandText = sSQL;

        //    sqlConnection.Open();
        //    sqlCommand.ExecuteNonQuery();
        //    sqlConnection.Close();
        //}

        //public void StopWorkingOnTask()
        //{
        //    if (_CurrentTask != null)
        //    {
        //        _CurrentTask.StopWork(this);

        //        string sSQL = "";
        //        SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
        //        SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);

        //        sqlCommand.Parameters.AddWithValue("NetworkID", _NetworkID);
        //        sqlCommand.Parameters.AddWithValue("CurrentTaskID", DBNull.Value);

        //        sSQL = "UPDATE Worklog..AllUsers SET CurrentTaskID = @CurrentTaskID WHERE NetworkID = @NetworkID";

        //        sqlCommand.CommandText = sSQL;

        //        sqlConnection.Open();
        //        sqlCommand.ExecuteNonQuery();
        //        sqlConnection.Close();

        //        _CurrentTask = null;

        //    }
        //}

        public static List<User> GetDepartmentMembers(User CurrentUser, bool blnActive, bool blnManager)
        {
            var userList = new List<User>();

            var sSql = "";
            var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            var sqlCommand = new SqlCommand(sSql, sqlConnection);

            if (blnActive == true && blnManager == true) {
                sSql = "SELECT NetworkID FROM Worklog..AllUsers WHERE Active = 1 and Manager = 1";
            }
            else if (blnActive == true) {
                sSql = "SELECT NetworkID FROM Worklog..AllUsers WHERE Active = 1";
            }
            else if (blnManager == true) {
                sSql = "SELECT NetworkID FROM Worklog..AllUsers WHERE Manager = 1";
            }
            else {
                sSql = "SELECT NetworkID FROM Worklog..AllUsers"; //As default, we ignore the case Active = 0 and Manager = 0
            }

            sqlCommand.CommandText = sSql;
            sqlConnection.Open();

            var networkIdList = new List<string>();

            using (var dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (dr["NetworkID"] != DBNull.Value)
                    {
                        networkIdList.Add((string)dr["NetworkID"]);
                    }
                }
            }
            sqlConnection.Close();

            foreach (var networkId in networkIdList)
            {
                userList.Add(new User(networkId, false));
            }

            return userList;
        }

        public static List<User> GetTeamMembers(User CurrentUser)
        {
            List<User> userList = new List<User>();

            string sSQL = "";
            SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);

            sSQL = "SELECT NetworkID FROM Worklog..AllUsers WHERE Active = 1 ORDER BY FirstName";

            sqlCommand.CommandText = sSQL;
            sqlConnection.Open();

            List<string> networkIDList = new List<string>();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (!(dr["NetworkID"] == DBNull.Value))
                    {
                        networkIDList.Add((string)dr["NetworkID"]);
                    }
                }
            }
            sqlConnection.Close();

            foreach (string networkID in networkIDList)
            {
                userList.Add(new User(networkID, false));
            }

            return userList;
        }

        public static List<string> FindUserIDs(string UserInfo, string SearchType)
        {
            List<string> networkIDList = new List<string>();

            string sSQL = "";
            SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(sSQL, sqlConnection);
            sqlCommand.Parameters.AddWithValue("UserInfo", UserInfo);

            sSQL = "SELECT NetworkID, UserName FROM Worklog..AllUsers WHERE ";

            if ("Begins With" == SearchType)
            {
                sSQL += "NetworkID LIKE @UserInfo + '%' ";
                sSQL += "OR FirstName LIKE @UserInfo + '%' ";
                sSQL += "OR LastName LIKE @UserInfo + '%' ";
                sSQL += "OR FirstName + ' ' + LastName LIKE @UserInfo + '%' ";
                sSQL += "OR Email LIKE @UserInfo + '%' ";
            }
            else
            {
                sSQL += "NetworkID LIKE '%' + @UserInfo + '%' ";
                sSQL += "OR FirstName LIKE '%' + @UserInfo + '%' ";
                sSQL += "OR LastName LIKE '%' + @UserInfo + '%' ";
                sSQL += "OR FirstName + ' ' + LastName LIKE '%' + @UserInfo + '%' ";
                sSQL += "OR Email LIKE '%' + @UserInfo + '%' ";
            }

            sqlCommand.CommandText = sSQL;
            sqlConnection.Open();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (!(dr["NetworkID"] == DBNull.Value))
                    {
                        networkIDList.Add((string)dr["NetworkID"]);
                    }
                    // Adding the usernames to this list due to support the legacy system based around username
                    if (!(dr["UserName"] == DBNull.Value))
                    {
                        networkIDList.Add((string)dr["UserName"]);
                    }
                }
            }
            sqlConnection.Close();

            return networkIDList;
        }

        //public static IEnumerable<User> GetUsers(User CurrentUser, bool blnActive, bool blnManager)
        //{
        //    IEnumerable<User> userList = new List<User>();

        //    using (var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString))
        //    {

        //        sqlConnection.Open();
        //        userList = sqlConnection.Query<User>("sp_GetUsers", new { IsActive = blnActive, IsManager = blnManager }, commandType: CommandType.StoredProcedure);

        //    }

        //    return userList;
        //}

        //public static dynamic GetTeamUsers(Team Team)
        //{
        //    dynamic userList = null;

        //    using (var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebWorklogConnectionString"].ConnectionString))
        //    {
        //        sqlConnection.Open();
        //        userList = sqlConnection.Query<dynamic>("sp_GetTeamUsers", new { TeamID = Team.ID }, commandType: CommandType.StoredProcedure);
        //    }

        //    return userList;
        //}
    }
}