﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Utilities
/// </summary>
/// 
namespace BPS.Common.Utilities
{
    public static class Cookies
    {
        public static string Get(string Key)
        {
            if (HttpContext.Current.Request.Cookies[Key] != null)
            {
                string value = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies[Key].Value).ToString();
                if (value != string.Empty) return value;
            }

            return null;
        }

        public static void Set(string Key, string Value)
        {
            if (HttpContext.Current.Request.Cookies[Key] != null)
                HttpContext.Current.Response.Cookies.Set(new HttpCookie(Key, Value));
            else
                HttpContext.Current.Response.Cookies.Add(new HttpCookie(Key, Value));
        }

        public static void Remove(string Key)
        {
            if (HttpContext.Current.Response.Cookies[Key] != null)
            {
                HttpCookie removedCookie = new HttpCookie(Key) { Expires = DateTime.Now.AddDays(-1) };
                HttpContext.Current.Response.Cookies.Add(removedCookie);
            }
        }
    }

    public static class Utils
    {
        public static string RowVersionToString(this byte[] rowVertion)
        {
            return Convert.ToBase64String(rowVertion);
        }

        public static string GenerateColumnQuery(string definedColumn, string selectedColumn, out string sresult)
        {
            var definedColumns = definedColumn.Split(',').ToList();
            var selectedColumns = selectedColumn.Split(',').ToList();
            var result = new List<string>();
            foreach (var item in selectedColumns)
            {
                var index = definedColumns.FindIndex(x => x.Trim().ToLower() == item.Trim().ToLower());
                if (index >= 0)
                {
                    result.Add(definedColumns[index]);
                }
            }
            sresult = string.Join(",", result).Trim();
            return sresult;
        }

        public static string CheckForSQLInjection(string userInput, out string sResult)
        {
            if (string.IsNullOrWhiteSpace(userInput))
            {
                sResult = string.Empty;
                return sResult;

            }
            // Don't use this function for the comments or text area fields.
            // Consider when using, not apply for all.
            sResult = userInput;
            string[] sqlCheckList = { "--",";--","/*","*/","@@","alter","create","cursor","declare","drop","exec","execute","fetch","kill","sysobjects","syscolumns" };
            string pattern = @"<[a-zA-Z/][\s\S]*>|(WHERE|OR|or|where)\s\S[a-zA-Z0-9]*=[A-Za-z0-9]*";
            string checkString = userInput.Replace("'", "''").Trim();

            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if (checkString.IndexOf(sqlCheckList[i],StringComparison.OrdinalIgnoreCase) > 0 || Regex.IsMatch(checkString, pattern))
                {
                    sResult = string.Empty;
                }
            }
            if (string.IsNullOrEmpty(sResult))
            {
                throw new System.Exception(checkString);
            }

            return sResult;
        }

        public static void SendEmail(string subject, string from, string to, string body, string attachments = "", string cc = "", string bcc = "")
        {
            try
            {
                var mailMsg = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress(from),
                    Body = body
                };

                foreach (var recipient in to.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(recipient))
                    {
                        break;
                    }
                    mailMsg.To.Add(recipient);
                }

                foreach (var recipient in cc.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(recipient))
                    {
                        break;
                    }
                    mailMsg.CC.Add(recipient);
                }

                foreach (var recipient in bcc.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(recipient))
                    {
                        break;
                    }
                    mailMsg.Bcc.Add(recipient);
                }

                foreach (var attachment in attachments.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(attachment))
                    {
                        break;
                    }
                    mailMsg.Attachments.Add(new Attachment(attachment));
                }

                mailMsg.IsBodyHtml = true;
                var client = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"])
                {
                    Credentials = CredentialCache.DefaultNetworkCredentials
                };

                client.Send(mailMsg);
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}