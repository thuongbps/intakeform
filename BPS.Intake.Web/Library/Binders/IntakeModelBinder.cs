﻿using System;
using System.Web.Mvc;
namespace BPS.Intake.Web.Library.Binders
{
    public class IntakeModelBinder : DefaultModelBinder, IModelBinder
    {
        private bool BindEnum<T>(ControllerContext controllerContext, ModelBindingContext bindingContext, System.ComponentModel.PropertyDescriptor propertyDescriptor) where T : struct
        {
            if (propertyDescriptor.PropertyType == typeof(T))
            {
                foreach (string fieldName in controllerContext.HttpContext.Request.Form.Keys)
                {
                    if (fieldName == propertyDescriptor.Name || fieldName.EndsWith("." + propertyDescriptor.Name))
                    {
                        var stringValue = controllerContext.HttpContext.Request.Form[fieldName];
                        T result;
                        if (Enum.TryParse(stringValue, out result))
                        {
                            propertyDescriptor.SetValue(bindingContext.Model, result);
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}