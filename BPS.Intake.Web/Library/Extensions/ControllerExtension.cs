﻿using System.IO;
using System.Web.Mvc;

namespace BPS.Intake.Web.Library.Extensions
{
    public static class ControllerExtension
    {
        public static string RenderViewToString(this ControllerBase controller, string viewName, object viewData)
        {
            var sw = new StringWriter();

            var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
            controller.ViewData.Model = viewData;

            var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

            viewResult.View.Render(viewContext, sw);

            var data = sw.GetStringBuilder().ToString();

            return data;
        }
    }
}