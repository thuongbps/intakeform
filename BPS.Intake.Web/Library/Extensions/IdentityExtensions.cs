﻿using System;
using System.Security.Principal;
using Microsoft.AspNet.Identity;
using BPS.Intake.Web.Library.Management;
using System.Web;
using HvN.Lib.IoC.Autofac;
using BPS.Intake.Domain.Service;
using System.Web.Mvc;
using BPS.Intake.Infrastructure.Types;
using System.Linq;
using BPS.Intake.Domain.Dto;

namespace BPS.Intake.Web.Library.Extensions
{
    public static class IdentityExtensions
    {
        public static Guid GetUserGuid(this IIdentity identity)
        {
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }

            var id = identity.GetUserId<string>();

            return Guid.Parse(id);
        }

        public static ApplicationUser GetUserInfo(this IPrincipal user)
        {
            const string CurrentUserInfoKey = "CurrentUser";
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                if (HttpContext.Current.Items.Contains(CurrentUserInfoKey))
                {
                    HttpContext.Current.Items.Remove(CurrentUserInfoKey);
                }

                return null;
            }            

            if (HttpContext.Current.Items.Contains(CurrentUserInfoKey))
            {
                return HttpContext.Current.Items[CurrentUserInfoKey] as ApplicationUser;
            }

            var userService = AutofacEngine.Instance.Resolve<IUserService>();

            var userInfo = userService.FindByUserName(user.Identity.Name).Result;

            if (userInfo == null)
            {
                return null;
            }

            var result = new ApplicationUser(userInfo);
            HttpContext.Current.Items.Add(CurrentUserInfoKey, result);

            return result;
        }

        public static ApplicationUser GetUserInfo(this Controller controller)
        {
            return GetUserInfo(controller.User);
        }

        public static bool IsInRoles(this UserDto user, RoleType role) 
        {
            return user.Roles != null && user.Roles.Where(r => (r.RoleType & role) == r.RoleType).Any();
        }

        public static bool IsInRoles(this ApplicationUser user, RoleType role)
        {
            return user.Roles != null && user.Roles.Where(r => (r.RoleType & role) == r.RoleType).Any();
        }
    }
}