﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Http.Routing;

namespace BPS.Intake.Web.Library.Helper.Common
{
    public static class CommonMethod
    {
        public static string MakeChildItemMenuSelected(string actionName, string controllerName, string areaName, string className = "active")
        {
            var area = "";
            var controller = "";
            var action = "";

            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;
            if (routeValues != null)
            {
                if (routeValues.ContainsKey("area"))
                {
                    area = routeValues["area"].ToString();
                }
                if (routeValues.ContainsKey("controller"))
                {
                    controller = routeValues["controller"].ToString();
                }
                if (routeValues.ContainsKey("action"))
                {
                    action = routeValues["action"].ToString();
                }
            }

            if ((!controller.Equals(controllerName, StringComparison.OrdinalIgnoreCase))
                || (!action.Equals(actionName, StringComparison.OrdinalIgnoreCase)
                || (!area.Equals(areaName, StringComparison.OrdinalIgnoreCase))
                ))
            {
                return "";
            }

            return className;
        }
        public static string GetRealMethodFromAsyncMethod(MethodBase asyncMethod)
        {
            var generatedType = asyncMethod.DeclaringType;
            if (generatedType == null) return "";
            var originalType = generatedType.DeclaringType;
            if (originalType == null) return "";
            var matchingMethods =
                from methodInfo in originalType.GetMethods()
                let attr = methodInfo.GetCustomAttribute<AsyncStateMachineAttribute>()
                where attr != null && attr.StateMachineType == generatedType
                select methodInfo;

            var foundMethod = matchingMethods.FirstOrDefault();
            return foundMethod != null ? foundMethod.Name : "";
        }

        public static string GetEnumDescription(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
    }
}