﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.IO;
//using System.Threading;
//using System.Runtime.InteropServices;
//using uno;
//using uno.util;
//using unoidl.com.sun.star.beans;
//using unoidl.com.sun.star.frame;
//using unoidl.com.sun.star.lang;
//using System.Diagnostics;
//using System.Configuration;


//namespace BPS.Intake.Web.Library.Helper.Common
//{
//    public class ConvertOfficeToPDF
//    {
//        private static ConvertOfficeToPDF _mInstance;
//        public static ConvertOfficeToPDF Instance
//        {
//            get { return _mInstance ?? (_mInstance = new ConvertOfficeToPDF()); }
//        }

//        public void ConvertToPdf(string inputFile, string outputFile)
//        {
//            XComponent xComponent = null;
//            try
//            {
//                if (ConvertExtensionToFilterType(Path.GetExtension(inputFile)) == null)
//                    throw new InvalidProgramException("Unknown file type for OpenOffice. File = " + inputFile);
//                StartOpenOffice();
//                //Get a ComponentContext
//                var xLocalContext = uno.util.Bootstrap.bootstrap();
//                //Get MultiServiceFactory
//                var xRemoteFactory = (XMultiServiceFactory)xLocalContext.getServiceManager();
//                //Get a CompontLoader
//                var aLoader = (XComponentLoader)xRemoteFactory.createInstance("com.sun.star.frame.Desktop");
//                //Load the sourcefile
//                xComponent = InitDocument(aLoader, PathConverter(inputFile), "_blank");
//                //Wait for loading
//                while (xComponent == null)
//                {
//                    Thread.Sleep(1000);
//                }
//                // save/export the document
//                SaveDocument(xComponent, inputFile, PathConverter(outputFile));
//            }
//            catch(Exception ex)
//            {

//            }
//            finally
//            {
//                if (xComponent != null) xComponent.dispose();
//            }
//        }


//        private void StartOpenOffice()
//        {
//            var ps = Process.GetProcessesByName("soffice.exe");
//            if (ps.Length != 0)
//                throw new InvalidProgramException("OpenOffice not found.  Is OpenOffice installed?");
//            if (ps.Length > 0)
//                return;
//            var p = new Process
//            {
//                StartInfo =
//                {
//                    Arguments = "-headless -nofirststartwizard",
//                    FileName = "soffice.exe",
//                    CreateNoWindow = true
//                }
//            };
//            var result = p.Start();

//            if (result == false)
//                throw new InvalidProgramException("OpenOffice failed to start.");
//        }


//        private XComponent InitDocument(XComponentLoader aLoader, string file, string target)
//        {
//            var openProps = new PropertyValue[1];
//            openProps[0] = new PropertyValue { Name = "Hidden", Value = new Any(true) };
//            var xComponent = aLoader.loadComponentFromURL(file, target, 0, openProps);
//            return xComponent;
//        }

//        private void SaveDocument(XComponent xComponent, string sourceFile, string destinationFile)
//        {
//            var propertyValues = new PropertyValue[2];
//            // Setting the flag for overwriting
//            propertyValues[1] = new PropertyValue { Name = "Overwrite", Value = new Any(true) };
//            //// Setting the filter name
//            propertyValues[0] = new PropertyValue
//            {
//                Name = "FilterName",
//                Value = new Any(ConvertExtensionToFilterType(Path.GetExtension(sourceFile)))
//            };
//            ((XStorable)xComponent).storeToURL(destinationFile, propertyValues);
//        }


//        private string PathConverter(string file)
//        {
//            string fileReturn = string.Empty;
//            if (string.IsNullOrEmpty(file))
//            {
//                throw new NullReferenceException("Null or empty path passed to OpenOffice");
//            }else
//            {
//                fileReturn = String.Format("file:///{0}", file.Replace(@"\", "/"));
//            }

//            return fileReturn;
//        }

//        public string ConvertExtensionToFilterType(string extension)
//        {
//            switch (extension)
//            {
//                case ".doc":
//                case ".docx":
//                case ".txt":
//                case ".rtf":
//                case ".html":
//                case ".htm":
//                case ".xml":
//                case ".odt":
//                case ".wps":
//                case ".wpd":
//                    return "writer_pdf_Export";
//                case ".xls":
//                case ".xlsb":
//                case ".xlsx":
//                case ".ods":
//                    return "calc_pdf_Export";
//                case ".ppt":
//                case ".pptx":
//                case ".odp":
//                    return "impress_pdf_Export";

//                default:
//                    return null;
//            }
//        }



//    }
//}