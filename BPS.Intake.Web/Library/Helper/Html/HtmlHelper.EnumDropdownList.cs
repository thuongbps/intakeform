﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace BPS.Intake.Web.Library.Helper.Html
{
    public static class HtmlHelperEnumDropdownList
    {
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            return EnumDropDownListFor(htmlHelper, expression, htmlAttributes, null, null, null);
        }

        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression,
            object htmlAttributes, IEnumerable<TEnum> includes, string optionLabel, IEnumerable<TEnum> excludes)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            var enumType = Nullable.GetUnderlyingType(typeof(TEnum)) ?? typeof(TEnum);

            IEnumerable<TEnum> enumValues = Enum.GetValues(enumType).Cast<TEnum>();

            if (excludes != null)
            {
                enumValues = enumValues.Except(excludes);
            }
            if (includes != null)
            {
                enumValues = enumValues.Where(includes.Contains);
            }

            var items = enumValues.Select(s => new SelectListItem
                                                   {
                                                       Text = s.GetDescription(),
                                                       Value = s.ToString(),
                                                       Selected = s.Equals(metaData.Model)
                                                   });

            if (optionLabel != null)
            {
                new[] { new SelectListItem { Text = optionLabel } }.Concat(items);
            }

            return htmlHelper.DropDownListFor(expression, items, optionLabel, htmlAttributes);
        }

        private static string GetDescription(this object obj)
        {
            try
            {
                var fieldInfo = obj.GetType().GetField(obj.ToString());

                var attr = fieldInfo.GetCustomAttribute<DescriptionAttribute>();

                return (attr != null && !string.IsNullOrWhiteSpace(attr.Description) ? attr.Description : obj.ToString()).ToFriendlyCase();
            }
            catch (NullReferenceException ex)
            {
                return string.Empty;
            }
        }

        public static MvcHtmlString CustomEnumDropDownListFor<TModel, TEnum>(
  this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
           
            var items =
                values.Select(
                   value =>
                   new SelectListItem
                   {
                       Text = GetEnumDescription(value),
                       Value = value.ToString(),
                       Selected = value.Equals(metadata.Model)
                   });
            
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            return htmlHelper.DropDownListFor(expression, items, attributes);
        }

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        private static string ToFriendlyCase(this string value, string removeText = "")
        {
            if (string.IsNullOrEmpty(value))
            {
                return "";
            }

            var friendlyCase = Regex.Replace(value, "(?!^)([A-Z])", " $1");

            return string.IsNullOrEmpty(removeText) ? friendlyCase : friendlyCase.Replace(removeText, "");
        }
    }
}