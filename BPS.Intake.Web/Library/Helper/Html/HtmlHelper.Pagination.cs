﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using BPS.Intake.Web.Models.Inc;

namespace BPS.Intake.Web.Library.Helper.Html
{
    public static class HtmlHelperPagination
    {
        public static MvcHtmlString PaginationFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression)
        {
            var model = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model as PaginationModel;

            if (model != null)
            {
                if (model.ShowPaging)
                {
                    model.CalculatePaging();
                }

                var htmlField = ExpressionHelper.GetExpressionText(expression);
                var htmlFieldId = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlField);

                model.PaginationId = htmlFieldId;

                return htmlHelper.EditorFor(expression, "_pagination");
            }

            return MvcHtmlString.Create(string.Empty);
        }

        public static MvcHtmlString Pagination(this HtmlHelper htmlHelper, string name, PaginationModel model)
        {
            if (model != null)
            {
                if (model.ShowPaging)
                {
                    model.CalculatePaging();
                }
                model.PaginationId = name;

                return new MvcHtmlString(HtmlHelperPartialToString.PartialToString("EditorTemplates/_pagination", model, htmlHelper.ViewContext));
            }

            return MvcHtmlString.Create(string.Empty);
        }
    }
}