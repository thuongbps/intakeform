﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;

namespace BPS.Intake.Web.Library.Helper.Html
{
    public static class HtmlHelperPartialToString
	{
        public static string PartialToString(string viewName, object model, ViewContext viewContext)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                throw new ArgumentException("viewName is empty");
            }

            var context = new ControllerContext(viewContext.RequestContext, viewContext.Controller);

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);

                var newViewContext = new ViewContext(context, viewResult.View, viewContext.ViewData,
                                                     viewContext.TempData, sw)
                {
                    ViewData = { Model = model }
                };

                viewResult.View.Render(newViewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static string RenderPartialToString(string viewName, object model, ControllerContext ControllerContext)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            ViewDataDictionary ViewData = new ViewDataDictionary();
            TempDataDictionary TempData = new TempDataDictionary();
            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }

        }
    }
}