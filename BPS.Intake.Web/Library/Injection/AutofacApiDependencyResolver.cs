﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using HvN.Lib.IoC.Autofac;

namespace BPS.Intake.Web.Library.Injection
{
    public class AutofacApiDependencyResolver : IDependencyResolver
    {
        public void Dispose()
        {

        }

        public object GetService(Type serviceType)
        {
            return AutofacEngine.Instance.ResolveOptional(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            var type = typeof(IEnumerable<>).MakeGenericType(serviceType);
            return (IEnumerable<object>)AutofacEngine.Instance.Resolve(type);
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }
    }
}