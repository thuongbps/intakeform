﻿using System.Web.Http.Controllers;
using System.Web.Mvc;
using HvN.Lib.IoC.Seedwork.IoC;
using HvN.Lib.Shared.Utils;

namespace BPS.Intake.Web.Library.Injection
{
    public static class AutofacContainerWebExtension
    {
        public static void RegisterControllers(this IContainerManager container, ITypeFinder finder)
        {
            var allControllers = finder.FindClassesOfType(typeof(IController));
            foreach (var controller in allControllers)
            {
                container.AddComponent(controller, controller);
            }
        }

        public static void RegisterApiControllers(this IContainerManager container, ITypeFinder finder)
        {
            var allControllers = finder.FindClassesOfType(typeof(IHttpController));
            foreach (var controller in allControllers)
            {
                container.AddComponent(controller, controller);
            }
        }
    }
}