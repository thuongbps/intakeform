﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using HvN.Lib.IoC.Autofac;

namespace BPS.Intake.Web.Library.Injection
{
    public class AutofacDependencyResolver : IDependencyResolver
    {
        public object GetService(Type serviceType)
        {
            return AutofacEngine.Instance.ResolveOptional(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            var type = typeof(IEnumerable<>).MakeGenericType(serviceType);
            return (IEnumerable<object>)AutofacEngine.Instance.Resolve(type);
        }
    }
}