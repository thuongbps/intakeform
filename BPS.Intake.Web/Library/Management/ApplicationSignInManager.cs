﻿using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Threading.Tasks;
using System.Web;

namespace BPS.Intake.Web.Library.Management
{
    public class ApplicationSignInManager : SignInManager<UserDto, Guid>
    {
        public LoginDto LoginDto { get; set; }
        public ApplicationSignInManager(ApplicationUserManager userManager)
            : base(userManager, HttpContext.Current.GetOwinContext().Authentication)
        {
            LoginDto = new LoginDto();
        }

        public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            var userInStore = await UserManager.FindByNameAsync(userName);
            if (userInStore == null)
            {
                var user = new UserDto
                {
                    UserName = LoginDto.OnQId,
                    FirstName = LoginDto.FirstName,
                    LastName = LoginDto.LastName,
                    OnQUserHotels = LoginDto.OnQUserHotels,
                    OnQUserType = LoginDto.OnQUserType,
                    Email = LoginDto.Email
                };

                user.CreatedDate = DateTimeOffset.Now;

                var result = await UserManager.CreateAsync(user);
                if (!result.Succeeded)
                {
                    throw new ApplicationException("Could not create user");
                }
            }

            return SignInStatus.Success;
        }

        public void SignOut()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }
    }
}