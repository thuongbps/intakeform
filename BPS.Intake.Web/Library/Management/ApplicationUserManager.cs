﻿using BPS.Intake.Domain.Dto;
using BPS.Intake.Domain.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BPS.Intake.Web.Library.Management
{
    public class ApplicationUserManager : UserManager<UserDto, Guid>
    {
        public ApplicationUserManager(IUserStore<UserDto, Guid> store)
            : base(store)
        {
        }

        public override async Task<bool> IsLockedOutAsync(Guid userId)
        {
            if (SupportsUserLockout)
            {
                return await base.IsLockedOutAsync(userId);
            }

            return false;
        }

        public override async Task<bool> CheckPasswordAsync(UserDto user, string password)
        {
            if (SupportsUserPassword)
            {
                return await base.CheckPasswordAsync(user, password);
            }

            return true; // already confirmed
        }

        public override async Task<IdentityResult> ResetAccessFailedCountAsync(Guid userId)
        {
            if (SupportsUserLockout)
            {
                return await base.ResetAccessFailedCountAsync(userId);
            }

            return IdentityResult.Success;
        }

        public override async Task<bool> GetTwoFactorEnabledAsync(Guid userId)
        {
            if (SupportsUserTwoFactor)
            {
                return await base.GetTwoFactorEnabledAsync(userId);
            }

            return false;
        }
    }

    public class ApplicationUser : IUser<Guid>
    {
        public ApplicationUser()
        {
        }

        public ApplicationUser(UserDto user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            UserName = user.UserName;
            Email = user.Email;
            Roles = user.Roles ?? new List<RoleDto>();
            Version = user.Version;
            CreatedDate = user.CreatedDate;
            ModifiedDate = user.UpdatedDate;
            //LineManger = user.LineManger == null ? string.Empty : user.LineManger.Name;
        }

        public string Email { get; set; }
        public byte[] Version { get; set; }
        public IList<RoleDto> Roles { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public string LineManger { get; set; }
    }
}