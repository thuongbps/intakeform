﻿using System;
using System.Collections.Generic;
using BPS.Intake.Web.Models.Inc;

namespace BPS.Intake.Web.Models
{
    [Serializable]
    public class BaseIndexModel<T>
    {
        public BaseIndexModel()
        {
            Results = new List<T>();

            Pagination = new PaginationModel();
        }

        public List<T> Results { get; set; }

        public PaginationModel Pagination { get; set; }
    }
}