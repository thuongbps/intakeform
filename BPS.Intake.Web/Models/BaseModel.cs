﻿using System;

namespace BPS.Intake.Web.Models
{
    public class BaseModel<TId>
    {
        public TId Id { get; set; }
    }

    public class BaseModel : BaseModel<Guid>
    {
    }
}