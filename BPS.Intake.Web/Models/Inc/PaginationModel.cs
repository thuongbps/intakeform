﻿using System;
using BPS.Intake.Infrastructure;

namespace BPS.Intake.Web.Models.Inc
{
    [Serializable]
    public class PaginationModel
    {
        public PaginationModel()
        {
            ShowPaging = true;
            PageSize = IntakeConstants.DefaultPageSize;
            CurrentPageIndex = 1;
        }

        public int TotalRecords { get; set; }

        public int PageSize { get; set; }

        public int CurrentPageIndex { get; set; }

        public int PageCount { get; private set; }

        public int DisplayedPageStartIndex { get; private set; }

        public int DisplayedPageEndIndex { get; private set; }

        public string PageClickUrl { private get; set; }

        public bool ShowPaging { get; set; }

        public string PaginationId { get; set; }

        /// <summary>
        /// Format xxxx({0}). PageClickFunction ~ xxx, param {0} is page index and it must be require.
        ///  </summary>
        public string PageClickFunction { get; set; }

        /// <summary>
        /// Format xxxx(0}). PageSizeClickFunction ~ xxx, param {0} is HTMLSelectElement.
        ///  </summary>
        public string PageSizeClickFunction { get; set; }

        public void CalculatePaging()
        {
            PageCount = (int)(Math.Ceiling(TotalRecords / (double)PageSize));
            CurrentPageIndex = CurrentPageIndex <= 0 ? 1 : (CurrentPageIndex > PageCount ? PageCount : CurrentPageIndex);

            DisplayedPageStartIndex = (CurrentPageIndex - IntakeConstants.DefaultPageCount / 2) < 1
                                        ? 1
                                        : CurrentPageIndex - IntakeConstants.DefaultPageCount / 2;

            DisplayedPageEndIndex = (CurrentPageIndex + IntakeConstants.DefaultPageCount / 2) > PageCount
                                        ? PageCount
                                        : CurrentPageIndex + IntakeConstants.DefaultPageCount / 2;
        }

        public string GetExecutePageClick(int pageIndex, bool alwaysSetEmptyFunction = false)
        {
            if (!alwaysSetEmptyFunction)
            {
                pageIndex = pageIndex <= 0 ? 1 : (pageIndex > PageCount ? PageCount : pageIndex);

                if (!string.IsNullOrEmpty(PageClickFunction))
                {
                    return string.Format("href=javascript:{0}", string.Format(PageClickFunction, pageIndex));
                }

                if (!string.IsNullOrEmpty(PageClickUrl))
                {
                    return string.Format("class=onlineTrainingLink href=javascript:void(0) link={0}",
                        String.Format("{0}{1}{2}={3}", PageClickUrl, (PageClickUrl.IndexOf("?") >= 0 ? "&" : "?"), IntakeConstants.DefaultPageQueryName, pageIndex));
                }
            }

            return "href=javascript:void(0)";
        }
    }
}