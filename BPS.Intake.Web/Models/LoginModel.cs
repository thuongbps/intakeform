﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; }
        public string LastName { get; set; }
        public string OnQId { get; set; }
        public string OnQUserHotels { get; set; }
        public string OnQUserType { get; set; }
        public SignInStatus IsSucess { get; set; }
    }
}