﻿using BPS.Intake.Infrastructure;

namespace BPS.Intake.Web.Models
{
    public class PagingViewModel
    {
        public PagingViewModel()
        {
            KeyWordSearch = string.Empty;
            PageIndex = 0;
            PageSize = IntakeConstants.PAGESIZE;
            TotalRecords = 0;
            CallBack = string.Empty;
            OrderBy = string.Empty;
        }
        public string KeyWordSearch { set; get; }
        public int PageIndex { set; get; }
        public int PageSize { set; get; }
        public long TotalRecords { set; get; }
        public string OrderBy { set; get; }
        public string CallBack { set; get; }
    }
}