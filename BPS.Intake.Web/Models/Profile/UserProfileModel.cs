﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BPS.Intake.Web.Models.Profile
{
    public class UserProfileModel : BaseModel
    {
        public UserProfileModel()
        {
            Roles = new List<string>();
        }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Given Name")]
        public string GivenName { get; set; }

        public string SurName { get; set; }

        public string Email { get; set; }

        public List<string> Roles { get; set; }
    }
}