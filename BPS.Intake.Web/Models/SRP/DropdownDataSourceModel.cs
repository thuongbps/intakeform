﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web.Models.SRP
{
    public class DropdownDataSourceModel
    {
        public int Id { get; set; }
        public int DropdownId { get; set; }
        public string OptionValue { get; set; }
    }
}