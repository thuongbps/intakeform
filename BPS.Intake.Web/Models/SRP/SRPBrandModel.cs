﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web.Models.SRP
{
    public class SRPBrandModel :BaseModel<int>
    {
        public virtual int BrandId { get; set; }
        public virtual int SRPId { get; set; }
        public virtual string Value { get; set; }
    }
}