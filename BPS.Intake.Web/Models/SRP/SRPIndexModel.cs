﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web.Models.SRP
{
    public class SRPIndexModel
    {
        public SRPIndexModel()
        {
            SRP = new SRPModel();
            SRPBrands = new List<SRPBrandModel>();
            SRPRegions = new List<SRPRegionModel>();
        }

        public SRPModel SRP { get; set; }
        public List<SRPBrandModel> SRPBrands { get; set; }
        public List<SRPRegionModel> SRPRegions { get; set; }
    }
}