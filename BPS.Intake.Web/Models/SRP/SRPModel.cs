﻿using BPS.Intake.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web.Models.SRP
{
    public class SRPModel : BaseModel<int>
    {
        public SRPModel()
        {
            Regions = new List<RegionModel>();
        }

        public string Name { get; set; }
        public Guid UserId { get; set; }
        public int SRPStatusId { get; set; }
        public string UserName { get; set; }
        public int SessionId { get; set; }
        public int HaveNextPage { get; set; }
        public int HavePreviousPage { get; set; }
        public List<RegionModel> Regions {get; set;}
    }
}