﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web.Models.SRP
{
    public class SRPRegionModel : BaseModel<int>
    {
        public virtual int RegionId { get; set; }
        public virtual int SRPId { get; set; }
        public virtual string Value { get; set; }
    }
}