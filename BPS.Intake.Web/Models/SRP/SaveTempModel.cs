﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPS.Intake.Web.Models.SRP
{
    public class SaveTempModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int SessionId { get; set; }
        public string Value { get; set; }
    }
}