﻿(function (intake, $) {

    var settings = {};

    intake.catetory = intake.catetory || {};

    intake.catetory.setOptions = function(options) {
        settings = $.extend(settings, options);
    };

    intake.catetory.init = function(options) {
        intake.catetory.setOptions(options);
        intake.catetory.addNew();
        intake.catetory.btnSearchClick();
        intake.catetory.cancel();
        intake.catetory.delete();
    };
    
    intake.catetory.searchForm = function () {
        $.ajaxLoading(true);
        $.post(settings.searchUrl, $('#form-category').serialize(), function (response) {
            $.ajaxLoading(false);
            $('#categoryList').html(response);
        });
    };

    intake.catetory.btnSearchClick = function () {
        $('#btnSearch').on('click', function () {
            intake.catetory.searchForm();
        });
    };
    
    intake.catetory.search = function (page) {
        $('#Pagination_CurrentPageIndex').val(page || 1);
        intake.catetory.searchForm();
    };

    intake.catetory.search_ChangePageSize = function (domPageSize) {
        $('#Pagination_PageSize').val(domPageSize.value);
        intake.catetory.search();
    };

    intake.catetory.addNew = function () {
        $('#addNewCategory').on('click', function() {
            location.href = settings.addUrl;
        });
    };

    intake.catetory.edit = function(id) {
        location.href = settings.editUrl + '/' + id;
    };
    
    intake.catetory.confirmDelete = function (id) {
        $('#currentId').val(id);
        $('#confirmPopup').modal();
    };

    intake.catetory.delete = function() {
        $('#confirmRequest').click(function() {
            $.get(settings.deleteUrl, { id: $('#currentId').val() }, function(response) {
                if(response.succeed) {
                    location.href = settings.indexUrl;
                }else {
                    alert(response.message);
                }
            });
        });
    };

    intake.catetory.cancel = function() {
        $('#cancelCategory').on('click', function () {
            location.href = settings.indexUrl;
        });
    };

}(window.intake = window.intake || {}, jQuery))

$('#form-category').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});
