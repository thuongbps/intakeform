﻿function Search() {
    var name = $('#searchName').val();
    var categoryId = $('select[name=CategorySelect]').val();
    var groupId = $('select[name=GroupSelect]').val();
    var type = $('select[name=TypeSelect]').val();
    var formId = $('#formId').val();

    var postData =
        {
            CategoryId: categoryId,
            GroupId: groupId,
            KeyWordSearch: name,
            Type: type,
            GroupType: formId
        }
    ;
   
    $.ajax({
        url: $("#UrlCallBack").val(),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(postData),
        beforeSend: function () {
            $.ajaxLoading(true);
        },
        success: function (data) {
            $.ajaxLoading(false);
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#ContentList').replaceWith(data);
           
        },
        error: function (err) {
        console.log(err.status + " - " + err.statusText);
    }
    });
}

function Reset() {
    $('#searchName').val('');
    $('select').prop('selectedIndex', 0);
    Search();
}


function PageIndex(element,event) {
    event.preventDefault();
    var isDisable = $(this).parent().hasClass("disabled");
    if (!isDisable) {
        var pageIndex = $(element).attr('data-page');
        CallBackPaging(pageIndex - 1);
    }
}

function CallBackPaging(pageIndex) {
    var name = $('#searchName').val();
    var categoryId = $('select[name=CategorySelect]').val();
    var groupId = $('select[name=GroupSelect]').val();
    var formId = $('#formId').val();
    var type = $('select[name=TypeSelect]').val();
    var postData =
    {
        CategoryId: categoryId,
        GroupId: groupId,
        KeyWordSearch: name,
        Type: type,
        GroupType: formId,
        PageIndex: pageIndex,
        PageSize: $('#PageSize').val()
    };

    $.ajax({
        url: $("#UrlCallBack").val(),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(postData),
        beforeSend: function () {
            $.ajaxLoading(true);
        },
        success: function (data) {
            $.ajaxLoading(false);
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#ContentList').replaceWith(data);
        }
    });

}
