﻿(function (intake, $) {

    var settings = {};

    intake.group = intake.group || {};

    intake.group.setOptions = function(options) {
        settings = $.extend(settings, options);
    };

    intake.group.init = function(options) {
        intake.group.setOptions(options);
        intake.group.addNew();
        intake.group.btnSearchClick();
        intake.group.cancel();
        intake.group.delete();
    };
    $('#form-group').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    intake.group.searchForm = function () {
        $.ajaxLoading(true);
        $.post(settings.searchUrl, $('#form-group').serialize(), function (response) {
            $.ajaxLoading(false);
            $('#groupList').html(response);
        });
    };

    intake.group.btnSearchClick = function () {
        $('#btnSearch').on('click', function () {
            intake.group.searchForm();
        });
    };
    
    intake.group.search = function (page) {
        $('#Pagination_CurrentPageIndex').val(page || 1);
        intake.group.searchForm();
    };

    intake.group.search_ChangePageSize = function (domPageSize) {
        $('#Pagination_PageSize').val(domPageSize.value);
        intake.group.search();
    };

    intake.group.addNew = function () {
        $('#addNewGroup').on('click', function () {
            location.href = settings.addUrl;
        });
    };

    intake.group.edit = function(id) {
        location.href = settings.editUrl + '/' + id;
    };
    
    intake.group.confirmDelete = function (id) {
        $('#currentId').val(id);
        $('#confirmPopup').modal();
    };

    intake.group.delete = function () {
        $('#confirmRequest').click(function() {
            $.get(settings.deleteUrl, { id: $('#currentId').val() }, function(response) {
                if(response.succeed) {
                    location.href = settings.indexUrl;
                }else {
                    alert(response.message);
                }
            });
        });
    };

    intake.group.cancel = function() {
        $('#cancelgroup').on('click', function () {
            location.href = settings.indexUrl;
        });
    };

}(window.intake = window.intake || {}, jQuery))