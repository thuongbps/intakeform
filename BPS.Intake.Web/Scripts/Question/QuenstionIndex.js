﻿var id = '';
var removeQuestion;
var courseName;
var domain = $("#hddomain").val();
jQuery(function ($) {

    $(".close").click(function () {
        location.reload(true);
    });
    $(document).on('click', '.normal-btn', function (e) {
        $('#divConfirmYesNo').modal('hide');
    });
    $(document).on('click', '.panel-heading span.clickable', function (e) {
        if ($(this).hasClass('panel-collapsed')) {
            // expand the panel
            $(this).parents('.panel').find('.panel-body').slideDown();
            $(this).removeClass('panel-collapsed');
            $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
        else {
            // collapse the panel
            $(this).parents('.panel').find('.panel-body').slideUp();
            $(this).addClass('panel-collapsed');
            $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        }
    });
});
$(document).ready(function () {
    $(document).on('click', '.confirm-delete', function (e) {
        id = $(this).closest('table tr').find('input:hidden');
        courseName = '';
        removeQuestion = $(this).closest('table tr');
        $("#hidCourseId").val(id.val());
        $("#bcourseName").html(courseName + "?");
        $('#divConfirmYesNo').modal();
    });

    $('.btn-delete-answer').click(function () {
        $.ajax({
            data: { id: id.val() },
            type: 'POST',
            dataType: "json",
            url: domain + '/TrainingTeam/ManageQuestion/DeleteQuestion',
            beforeSend: function () {
                waitingDialog.show('Progress Question');
            },
            success: function (data) {
            },
            fail: function (data) {

            },
            complete: function () {
                removeQuestion.remove();
                waitingDialog.hide();
            },
        })
    });
});