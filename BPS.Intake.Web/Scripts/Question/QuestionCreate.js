﻿$(document).ready(function () {
    $('#AddQuestion').click(function () {
        Answer('0');
        return false;
    });

    $('#Type').change(function () {        
        var selectedtype = $(this).val();        
        if (selectedtype == 'Essay')
        {
            $("#AnswerEssay").removeClass('hidden');
            $("#Answer").addClass('hidden');
        }
        else
        {
            $("#AnswerEssay").addClass('hidden');
            $("#Answer").removeClass('hidden');
        }
        return false;
    });

    $(document).on('click', '.remove', function (e) {
        $(this).closest("div").remove()
        Answer('1');
        return false;
    });
})

function Answer(type) {

    if (type == '0') {
        var clonediv = $('#ChildAddQuestion').clone()
        clonediv.appendTo(".AddQuestion");
    }
    $('.ChildAddQuestion').each(function (index, elements) {
        var contentQuestion = $(this).children('.form-control');
        var checkbox = $(this).children('.DictionaryCheck');
        var remove = $(this).children('.remove');
        var spanAlertValidate = $(this).children('.text-danger');
        var childSpanAlert = spanAlertValidate.children('span');
        contentQuestion.attr('name', 'dictionary[' + index + '].Key');
        contentQuestion.attr('id', 'dictionary_' + index + '__Key');
        contentQuestion.attr('aria-describedby', 'dictionary_'+index+'__Key-error');
        contentQuestion.attr('required', 'required');

        checkbox.attr('name', 'dictionary[' + index + '].Value');
        checkbox.attr('id', 'dictionary_' + index + '__Value');
        spanAlertValidate.attr('data-valmsg-for', 'dictionary[' + index + '].Key');
        childSpanAlert.attr('id', 'dictionary_'+index+'__Key-error');


        var hidden = $(this).find('input:hidden');
        hidden.attr('name', 'dictionary[' + index + '].Value');

        if (type == '1') {
            if (index == $('.ChildAddQuestion').length - 1) {
                contentQuestion.val('');
                checkbox.prop("checked", false);
            }
        }
    });    
}