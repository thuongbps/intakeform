﻿$(document).ready(function () {
    $("#Answer-text-danger").hide();
    var domain = $("#hddomain").val();
    $('#SaveQuestion').click(function () {
        var validated = ValidateEditQuestion();
        if(validated ==0)
        {
            var id = $('#Id').val();
            $.ajax({
                data: $('#form-editquestion').serialize(),
                type: 'POST',
                dataType: "json",
                url: domain + '/TrainingTeam/ManageQuestion/UpdateQuestion',
                beforeSend: function () {
                },
                success: function (data) {
                    $("#divResult").modal();
                },
                fail: function (data) {

                },
                complete: function () {
                    waitingDialog.hide();
                },
            })
            return false;
        }
       
    })
    $(document).on('click', '.btn-edit-answer', function (e) {
        var text = $(this).closest('table tr').find('input:text');
        var checkbox = $(this).closest('table tr').find('input:checkbox');
        var id = $(this).closest('table tr').find('input:hidden');
        if (text.val() != "")
        {
             $.ajax({
                    data: { id: id.val(), content: text.val(), isCorrect: checkbox.is(":checked") },
                    type: 'POST',
                    dataType: "json",
                    url: domain + '/TrainingTeam/ManageQuestion/UpdateAnswer',
                    beforeSend: function () {
               
                    },
                        success: function (data) {
                            $("#divResult").modal();
                            text.css("border", "1px solid white");
                    },
                        fail: function (data) {

                    },
                        complete: function () {
                            waitingDialog.hide();
                    },
            })
        }
        else
        {
            text.css("border", "1px solid red");
            text.attr("placeholder","Answer can not be empty");
        }
       
    })

    $(document).on('click', '.btn-delete-answer', function (e) {
        var id = $(this).closest('table tr').find('input:hidden');
        var removeAnswer = $(this).closest('table tr');
        $.ajax({
            data: { id: id.val() },
            type: 'POST',
            dataType: "json",
            url: domain + '/TrainingTeam/ManageQuestion/DeleteAnswer',
            beforeSend: function () {
                $.ajaxLoading(true);
            },
            success: function (data) {
                $.notify("Delete Question successfully!", { className: "success", position: "align top center" });
            },
            fail: function (data) {

            },
            complete: function () {
                removeAnswer.remove();
                $.ajaxLoading(false);
            },
        })
    });

    $('.panel-heading span.clickable').on("click", function (e) {
        if ($(this).hasClass('panel-collapsed')) {
            // expand the panel
            $(this).parents('.panel').find('.panel-body').slideDown();
            $(this).removeClass('panel-collapsed');
            $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
        else {
            // collapse the panel
            $(this).parents('.panel').find('.panel-body').slideUp();
            $(this).addClass('panel-collapsed');
            $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        }
    });

    $('#btn-add-answer').click(function () {
        $('#tbl-answer tr:first').after('<tr>\
                                             <td><input type="text" class="valid" aria-invalid="false"></td>\
                                             <td><input class="DictionaryCheck" id="dictionary_0__Value" name="dictionary[0].Value" type="checkbox" value="true"><input type="hidden" value=""></td>\
                                             <td><span class="glyphicon glyphicon-ok save" aria-hidden="true" style="cursor:pointer"></span>\
                                                  <span class="glyphicon glyphicon-remove remove" aria-hidden="true" style="cursor:pointer"></span></td>\
                                             </tr>');
        return false;
    });
    $(document).on('click', '.remove', function (e) {
        var removeAnswer = $(this).closest('table tr').remove();
    });

    $(document).on('click', '.save', function (e) {
        var table = $(this).closest('table tr td');
        var text = $(this).closest('table tr').find('input:text');
        var saveButton = $(this).closest('.save');
        var deleteButton = $(this).closest('tr').find('.remove');
        var checkbox = $(this).closest('table tr').find('input:checkbox');
        var hidden = $(this).closest('table tr').find('input:hidden');
        var idQuestion = $('#Id').val();

        if (text.val() != "")
        {
            $.ajax({
                data: { id: idQuestion, content: text.val(), isCorrect: checkbox.is(":checked") },
                type: 'POST',
                dataType: "json",
                url: domain + '/TrainingTeam/ManageQuestion/CreateAnswer',
                beforeSend: function () {
                    $.ajaxLoading(true);
                },
                success: function (data) {
                    hidden.val(data)
                    saveButton.remove();
                    deleteButton.remove();
                    table.append('<a class="label label-sm label-primary btn-edit-answer" href="#">Confirm edit</a>\
                                  <a href="#" class="label label-sm label-danger btn-delete-answer">Delete</a>');
                    text.css('border', 'none');
                    $("#Answer-text-danger").hide();
                    text.css("border", "1px solid white");
                },
                fail: function (data) {

                },
                complete: function () {
                    $.ajaxLoading(false);
                },
            })
        }
        else
        {
            text.css("border", "1px solid red");
            text.attr("placeholder", "Answer can not be empty");
        }

        
    });
});


function ValidateEditQuestion()
{
    var errorAlert = "<span>This field is required.</span>";
    var validatefail = 0;
    //======================================valid content question
    if ($("#txt_content").val()=="")
    {
        $("#Content-text-danger").attr("class", "text-danger field-validation-error").html(errorAlert);
        validatefail += 1;
    }else
    {
        $("#Content-text-danger").attr("class", "field-validation-valid text-danger").html("");
    }
    //======================================valid category
    if ($("#select_category").val() == "") {
        $("#CategoryId-text-danger").attr("class", "text-danger field-validation-error").html(errorAlert);
        validatefail += 1;
    } else {
        $("#CategoryId-text-danger").attr("class", "field-validation-valid text-danger").html("");
    }
    //====================================== valid level
    if ($("#dl_level").val() == "") {
        $("#Level-text-danger").attr("class", "text-danger field-validation-error").html(errorAlert);
        validatefail += 1;
    } else {
        $("#Level-text-danger").attr("class", "field-validation-valid text-danger").html("");
    }
    //====================================== valid type
    if ($("#dl_Type").val() == "") {
        $("#Type-text-danger").attr("class", "text-danger field-validation-error").html(errorAlert);
        validatefail += 1;
    } else {
        $("#Type-text-danger").attr("class", "field-validation-valid text-danger").html("");
    }
    //====================================== valid answer
    var rowCount = $('#tbl-answer >tbody >tr').length;
    if (rowCount == 0)
    {
        validatefail += 1;
        $("#Answer-text-danger").show();
    } else
    {
        $("#Answer-text-danger").hide();
    }
    return validatefail;
}