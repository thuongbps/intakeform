﻿;
(function (intake, $) {
    var settings = {};

    intake.srp = intake.srp || {};

    intake.srp.setOptions = function (options) {
        settings = $.extend(settings, options);
    };

    intake.srp.init = function (options) {
        intake.srp.setOptions(options);
    };

    intake.srp.loadGrid = function () {
        var cl_Name = function (data, type, full, meta) {
            return "<a class=\"modal-link\" name=\"partyLink\" href=\"" + full[2] + "\">" + full[1] + "</a>";
        }
        var oTable = $('#srpGrid').dataTable({
            "bServerSide": true,
            "sAjaxSource": settings.urlGetList,
            "bProcessing": false,
            "dom": '<"paging-padding"l>rt<"col-sm-4 zero-padding"i>p',
            aaSorting: [[0, 'asc']],
            "iDisplayLength": 20,
            "oLanguage": {
                "sEmptyTable": "There's no record.",
                "sLengthMenu": '<select class="form-control">' +
                  '<option value="5">5</option>' +
                  '<option value="10">10</option>' +
                  '<option value="20">20</option>' +
                  '<option value="50">50</option>' +
                  '<option value="100">100</option>' +
                  '</select> records per page'
            },
            "aoColumns": [
                            {
                                "sName": "Name",
                                "render": cl_Name
                            }
            ],
            "order": [[0, "asc"]]
        });
    }

}(window.intake = window.intake || {}, jQuery))