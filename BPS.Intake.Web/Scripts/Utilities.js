﻿$(document).ready(function () {

    $.ajaxLoading = function (isShow) {
        (isShow === true) ? $('#ajaxloading').show() : $('#ajaxloading').hide();
    };

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.scrolltop:hidden').stop(true, true).fadeIn();
        } else {
            $('.scrolltop').stop(true, true).fadeOut();
        }
    });

    $('.scrolltop').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
});

function truncateField(field) {
    field.change(function () {
        field.val(truncate(field.val()));
    });
}
function truncate(text) {
    var truncated = text.trim();
    return truncated;
}

var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}

function showFullSpace() {
    var listName = $('.show-full-space');
    for (var i = 0 ; i < listName.length ; i++) {
        listName[i].text = listName[i].text.replace(/\s/g, " ");
    }
}

function showNumberOfRecords(number) {
    var plural = '';

    if (number > 1)
        plural = 's';

    return number + ' record' + plural + ' found';
}

function checkEdited(saveButtonId) {
    $('.check-edited').on('input change', function () {
        $('#' + saveButtonId).removeAttr("disabled");
    });
}

function encode(text) {
    var encoded = encodeURIComponent(text);
    return encoded;
}

function _join(arrayToJoin, arraySplitted, index) {

    for (var i = 1 ; i < index; i++) {
        arrayToJoin += "/" + arraySplitted[i];
    }
    return arrayToJoin;
}
function showAlertMessage(title, message, height) {
    $('#divAlertMessageMasterPage').css('height', height + 'px');
    $('#popupTitle', '#divAlertMessageMasterPage').html(title);
    $('#popupMessage', '#divAlertMessageMasterPage').html(message);
    $('#divAlertMessageMasterPage').modal();
}
