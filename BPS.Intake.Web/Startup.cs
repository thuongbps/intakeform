﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BPS.Intake.Web.Startup))]
namespace BPS.Intake.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // config automapper
            Configure();
        }
    }
}
