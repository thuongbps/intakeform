# IntakeForm
This project is designed as DDD pattern idealy.
Technology used:
- .NET Framework 4.5
- ASPNET MVC 5
- Entity Framework 6 (code first migration)
- JQuery and some libraries opensource.
============================================

1. Set BPS.Intake.Web as start up project
2. Open and adjust connection string info base on your server (BPS.Intake.Web/Configs/ConnectionStrings.config)
3. Open Package Manager Console, choose Default Project as Backend/BPS.Intake.Framework
4. Type Update-Database -Verbose
5. Run script UserRoleData.sql (Backend/BPS.Intake.Framework/SQLSript) 
6. Take a look at AppSettings.config
 - There are 3 roles of user in this project (Admin, Traning Team and User)
 - The project is currently use dtran1 with role User (<add key="DefaulLocalUserLogin" value="dtran1"/>) 
5. Start website
